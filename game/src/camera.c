#include "camera.h"

Camera2D CameraInit()
{
	Camera2D l_new_camera;
	l_new_camera.target = party[0].position;
	l_new_camera.offset = (Vector2){screenWidth/2, screenHeight/2};
	l_new_camera.rotation = 0.0f;
	l_new_camera.zoom = CAM_DEF_ZOOM_FAC;
	return l_new_camera;
}

void CheckEdgeOfMap(Camera2D *p_camera, RenderTexture2D *p_bg_texture,
		    int p_edge)
{
	float target = (p_edge == 1) ? p_camera->target.x : p_camera->target.y;
	float *offset = (p_edge == 1) ? &p_camera->offset.x : &p_camera->offset.y;
	float tex_size = (p_edge == 1) ? p_bg_texture->texture.width :
		p_bg_texture->texture.height;
	int screen_size = (p_edge == 1) ? screenWidth : screenHeight;
	if(target * p_camera->zoom - (screen_size/2) <= 0){
		*offset = target * p_camera->zoom;
	}
	else if(target * p_camera->zoom + (screen_size/2) >=
		tex_size * p_camera->zoom){
			*offset = screen_size -
				(tex_size * p_camera->zoom -
				 target * p_camera->zoom);
	}
}

void CheckWidthEdgeOfMap(Camera2D *p_camera, RenderTexture2D *p_bg_texture)
{
	CheckEdgeOfMap(p_camera, p_bg_texture, 1);
}

void CheckHeightEdgeOfMap(Camera2D *p_camera, RenderTexture2D *p_bg_texture)
{
	CheckEdgeOfMap(p_camera, p_bg_texture, -1); 
}

void CheckBoundsOfMap(Camera2D *p_camera, RenderTexture2D *p_bg_texture)
{
	CheckWidthEdgeOfMap(p_camera, p_bg_texture);
	CheckHeightEdgeOfMap(p_camera, p_bg_texture);
}

void CameraUpdate(Camera2D *p_camera, RenderTexture2D *p_bg_texture)
{
	p_camera->target = party[0].position; 
	CheckBoundsOfMap(p_camera, p_bg_texture);
}

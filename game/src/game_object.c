#include "central_systems.h"

const static struct {
	enum goTypes val;
	const char* str;
} go_type_conv [] = {
	{COLLIDER, "Collider"},
	{NPC,"NPC"},
	{TELEPORT, "Teleport"},
	{SLOCATION, "Start Location"},
	{BUILDING, "Building"},
	{ENCOUNTER, "Random Encounters"},
	{NONE, ""}
};

GameObject game_object_npc_new(tmx_object* tmx_obj);
GameObject game_object_encounter_new(tmx_object* tmx_obj);
GameObject game_object_teleport_new(tmx_object* tmx_obj);
GameObject game_object_collider_new(tmx_object* tmx_obj);
GameObject game_object_slocation_new(tmx_object* tmx_obj);
GameObject game_object_building_new(tmx_object* tmx_obj);
GameObject game_object_none_new(tmx_object* tmx_obj);

GameObject game_object_new(tmx_object* tmx_obj, enum goTypes type){
	/* this will be a SWITCH statement function */
	/* if(!strcmp(type, "NPC")){ */
	/* 	return game_object_npc_new(tmx_obj); */
	/* } */
	switch(type){
	case NPC:
		return game_object_npc_new(tmx_obj);
		break;
	case COLLIDER:
		return game_object_collider_new(tmx_obj);
		break;
	case TELEPORT:
		return game_object_teleport_new(tmx_obj);
		break;
	case SLOCATION:
		return game_object_slocation_new(tmx_obj);
		break;
	case ENCOUNTER:
		return game_object_encounter_new(tmx_obj);
		break;
	case BUILDING:
		return game_object_building_new(tmx_obj);
	case NONE:
	default:
		return game_object_none_new(tmx_obj);
	}
}

int npc_default_draw(float *d_time, struct GameObject * self){
	/* if(is_talking){ */
	/* 	// draw dialogue box above npc, with correct position and dialogue line */
	/* } */
	DrawTextureRec(g_sprite_sheet,
		       ht_search(charSpriteSheet, "defaultNPC"),
		       self->position, WHITE);
	if(self->value.npc.can_talk && !self->value.npc.is_talking){
		//draw talkIcon at npc position
		DrawTextureRec(g_sprite_sheet,
			       ht_search(charSpriteSheet, "talkIcon"),
			       (Vector2){self->position.x,
					 self->position.y - 16.0f}, WHITE);
		/* DrawRectangle(self->position.x, self->position.y-16, 16, 16, GREEN); */
	}
	return 0;
}

int npc_default_update(float *d_time, struct GameObject * self, Scene* scene){
	//first step for default is movement, if polyline move along the points with speed of npc. Otherwise stand still
	//TODO rough implementation of lerp moving along polyline is done. Needs cleaning up later.
	//TODO need for the lerping to take into acount when vertical line in y axis, is suddenly very speed up. Because it checks on the delta x
	//later on maybe default behaviour for being interactable, dialogs etc.
	/* collision with player and npc has dialogue lines */
	if(CheckCollisionRecs(party[0].collider,
			      (Rectangle){self->position.x - 5.0f,
					  self->position.y  - 5.0f,
					  20.0f, 20.0f})){
		//set can_talk to true
		self->value.npc.can_talk = 1;
	}else{
		self->value.npc.can_talk = 0;
	}
	if(self->value.npc.can_talk && !self->value.npc.is_talking){
		/* do we want input here? */
		if(IsKeyReleased(KEY_X)){
			self->value.npc.is_talking = !self->value.npc.is_talking;
			SwitchState(&current_game_state, current_game_states.DIALOGUE);
			/* party[0].is_talking = !party[0].is_talking; */
			printf("INFO: is_talking value: %d\n", self->value.npc.is_talking);
		}
	}
	if(self->value.npc.route_length > 0){
		struct point_list* target_pos;
		if(self->value.npc.direction_route == NEXT)
			target_pos = self->value.npc.current_pos->next;
		else
			target_pos = self->value.npc.current_pos->previous;
		
		float d_x = fabs(target_pos->point.x - self->position.x);
		float d_y = fabs(target_pos->point.y - self->position.y);
		/* printf("info: delta before: %f,%f\n", d_x,d_y); */
		if(d_x <= 0.04f && d_y <= 0.04f){
			self->value.npc.current_pos = target_pos;
			if(self->value.npc.direction_route == NEXT){
				target_pos = self->value.npc.current_pos->next;
				if(target_pos == NULL){
					printf("WARNING: Got into null terrority and need to go to Previous/n");
					self->value.npc.direction_route = PREVIOUS;
					target_pos = self->value.npc.current_pos->previous;
				}
			}else{
				target_pos = self->value.npc.current_pos->previous;
				if(target_pos == NULL){
					printf("WARNING: Got into null terrority and need to go to Next/n");
					self->value.npc.direction_route = NEXT;
					target_pos = self->value.npc.current_pos->next;
				}
			}
		}
		if(d_x <= 0.04f && d_y >= 0.04f){
			d_x = (self->value.npc.speed * *d_time) /
			(target_pos->point.y - self->position.y);
			/* printf("INFO: using the position y: %f\n", d_x); */
		}else{
			d_x = (self->value.npc.speed * *d_time) /
				(target_pos->point.x - self->position.x);
		}
		d_x = fabs(d_x);
		/* printf("info: delta %f\n", d_x); */
		/* printf("info: point walkroute %f, %f\n",(target_pos->point.x * d_x * */
		/* 	 (float)self->value.npc.direction_route), */
		/*        target_pos->point.y); */
		self->position.x = self->position.x * (1 - d_x) +
			(target_pos->point.x * d_x);
		self->position.y = self->position.y * (1 - d_x) +
			(target_pos->point.y * d_x);
	}
	return 0;
}

struct point_list* npc_walking_route(double** points, int length, Vector2 offset){
	//TODO Make this filling of linked list more elegant. dry
	struct point_list* result = (struct point_list*)malloc(sizeof(struct point_list));
	result->point.x = (float)points[0][0] + offset.x;
	result->point.y = (float)points[0][1] + offset.y;
	result->previous = NULL;
	result->next = NULL;
	struct point_list* prev = result;
	for(int i = 1; i < length; i++){
		struct point_list* temp = (struct point_list*)malloc(sizeof(struct point_list));
		temp->point.x = (float)points[i][0] + offset.x;
		temp->point.y = (float)points[i][1] + offset.y;
		temp->previous = prev;
		prev->next = temp;
		temp->next = NULL;
		prev = temp;
	}
	return result;
}

GameObject game_object_npc_new(tmx_object* tmx_obj)
{
	GameObject result_go;
	if(tmx_obj->name == NULL){
		strncpy(result_go.name, "NONE", 32);
	}else{
		strncpy(result_go.name, tmx_obj->name, 32);
	}
	result_go.visible = tmx_obj->visible;
	result_go.position.x = tmx_obj->x;
	result_go.position.y = tmx_obj->y;
	result_go.type = NPC;
	if(tmx_get_property(tmx_obj->properties, "name") != NULL){
		result_go.value.npc.name =
			tmx_get_property(tmx_obj->properties,
					 "name")->value.string;
	}
       	if(tmx_get_property(tmx_obj->properties, "id") != NULL){
		result_go.value.npc.id =
			tmx_get_property(tmx_obj->properties,
					 "id")->value.integer;
	}
	result_go.value.npc.route_length = 0; //default init value
	if(tmx_obj->obj_type == OT_POLYLINE &&
	   tmx_obj->content.shape != NULL){
		result_go.value.npc.route_length =
			tmx_obj->content.shape->points_len;
		result_go.value.npc.direction_route = NEXT;
		result_go.value.npc.walk_route =
			npc_walking_route(tmx_obj->content.shape->points,
					  result_go.value.npc.route_length,
					  (Vector2){tmx_obj->x, tmx_obj->y});
		result_go.value.npc.current_pos = result_go.value.npc.walk_route;
	}
	result_go.CustomDraw = &npc_default_draw; //from xml database
	result_go.CustomUpdate = &npc_default_update; //from xml database
	result_go.value.npc.speed = 5; //from xml database
	result_go.value.npc.can_talk = 0; //from xml database
	result_go.value.npc.is_talking = 0; //does this need to be on the npc or on the character/player itself
	//TODO custom update and draw needs to be implemented
	return result_go;
}

int8_t game_object_equals(GameObject* first_gobj, GameObject* second_gobj)
{
	int8_t result = !strcmp(first_gobj->name, second_gobj->name) &&
		first_gobj->visible == second_gobj->visible &&
		first_gobj->position.x == second_gobj->position.x &&
		first_gobj->position.y == second_gobj->position.y &&
		first_gobj->type == second_gobj->type &&
		first_gobj->CustomDraw == second_gobj->CustomDraw &&
		first_gobj->CustomUpdate == second_gobj->CustomUpdate &&
		first_gobj->next == second_gobj->next;
	/* TODO check for equality on the value var needs to be added */
	return result; 
}

enum goTypes str_2_go_type(const char* type)
{
	for (int i = 0;  i < LENGTH(go_type_conv); i++){
		if (!strcmp (type, go_type_conv[i].str))
			return go_type_conv[i].val;    
	}
	return NONE;
}

GameObject game_object_encounter_new(tmx_object* tmx_obj){
	GameObject temp;
	if(tmx_obj->name == NULL){
		strncpy(temp.name, "NONE", 32);
	}else{
		strncpy(temp.name, tmx_obj->name, 32);
	}
	temp.visible = tmx_obj->visible;
	temp.position.x = tmx_obj->x;
	temp.position.y = tmx_obj->y;
	temp.type = ENCOUNTER;
	tmx_properties* props = tmx_obj->properties;
	tmx_property* prop_id = tmx_get_property(props, "id_0");
	/* TODO: this can be cleaner */
	if(prop_id != NULL){
		temp.value.ranEncounter.id0 = prop_id->value.integer;
	}
	prop_id = tmx_get_property(props, "id_1");
	if(prop_id != NULL){
		temp.value.ranEncounter.id1 = prop_id->value.integer;
	}
	prop_id = tmx_get_property(props, "id_2");
	if(prop_id != NULL){
		temp.value.ranEncounter.id2 = prop_id->value.integer;
	}
	prop_id = tmx_get_property(props, "id_3");
	if(prop_id != NULL){
		temp.value.ranEncounter.id3 = prop_id->value.integer;
	}
	prop_id = tmx_get_property(props, "id_4");
	if(prop_id != NULL){
		temp.value.ranEncounter.id4 = prop_id->value.integer;
	}
	temp.value.ranEncounter.area = (Rectangle) {temp.position.x,
						     temp.position.y,
						     tmx_obj->width,
						     tmx_obj->height};
	temp.CustomUpdate = &encounter_default_update;
	return temp;
}

GameObject game_object_teleport_new(tmx_object* tmx_obj){
	GameObject temp;
	if(tmx_obj->name == NULL){
		strncpy(temp.name, "NONE", 32);
	}else{
		strncpy(temp.name, tmx_obj->name, 32);
	}
	temp.visible = tmx_obj->visible;
	temp.position.x = tmx_obj->x;
	temp.position.y = tmx_obj->y;
	temp.type = TELEPORT;
	tmx_properties* props = tmx_obj->properties;
	tmx_property* prop_temp = tmx_get_property(props, "id");
	if(prop_temp == NULL){
		temp.value.teleport.id = 1;
	}
	else{
		temp.value.teleport.id = prop_temp->value.integer;
	}
	prop_temp = tmx_get_property(props, "target_id");
	if(prop_temp == NULL){
		temp.value.teleport.target_id = 0;
	}
	else{
		temp.value.teleport.target_id = prop_temp->value.integer;
	}
	//TODO: add default for empty targetmap
	strcpy(temp.value.teleport.target_map,
	       tmx_get_property(props, "target_map")->value.string);
	temp.CustomUpdate = &teleport_default_update;
	return temp;
}

GameObject game_object_collider_new(tmx_object* tmx_obj){
	GameObject temp;
	if(tmx_obj->name == NULL){
		strncpy(temp.name, "NONE", 32);
	}else{
		strncpy(temp.name, tmx_obj->name, 32);
	}
	temp.visible = tmx_obj->visible;
	temp.position.x = tmx_obj->x;
	temp.position.y = tmx_obj->y;
	temp.type = COLLIDER;
	tmx_properties* props = tmx_obj->properties;
	if(props == NULL){
		temp.value.collider.layer = 0;
	}else{
		tmx_property* layer_prop = tmx_get_property(props, "layer");
		temp.value.collider.layer = layer_prop->value.integer;
	}
	temp.value.collider.area = (Rectangle) {temp.position.x,
						temp.position.y,
						tmx_obj->width,
						tmx_obj->height};
	temp.CustomUpdate = &collider_default_update;
	return temp;
}

GameObject game_object_slocation_new(tmx_object* tmx_obj){
	GameObject temp;
	if(tmx_obj->name == NULL){
		strncpy(temp.name, "NONE", 32);
	}else{
		strncpy(temp.name, tmx_obj->name, 32);
	}
	temp.visible = tmx_obj->visible;
	temp.position.x = tmx_obj->x;
	temp.position.y = tmx_obj->y;
	temp.type = SLOCATION;
	temp.CustomUpdate = &slocation_default_update;
	return temp;
}

GameObject game_object_building_new(tmx_object* tmx_obj){
	GameObject temp;
	if(tmx_obj->name == NULL){
		strncpy(temp.name, "NONE", 32);
	}else{
		strncpy(temp.name, tmx_obj->name, 32);
	}
	temp.visible = tmx_obj->visible;
	temp.position.x = tmx_obj->x;
	temp.position.y = tmx_obj->y;
	temp.type = BUILDING;
	return temp;
}

GameObject game_object_none_new(tmx_object* tmx_obj){
	GameObject temp;
	if(tmx_obj->name == NULL){
		strncpy(temp.name, "NONE", 32);
	}else{
		strncpy(temp.name, tmx_obj->name, 32);
	}
	temp.visible = tmx_obj->visible;
	temp.position.x = tmx_obj->x;
	temp.position.y = tmx_obj->y;
	temp.type = NONE;
	return temp;
}

int slocation_default_update(float* d_time, struct GameObject* self, Scene* scene){
	if(scene->first_time == 1){
		party[0].position = self->position;
		scene->first_time = 0;
	}
	return 0;
}

int encounter_default_update(float* d_time, struct GameObject* self, Scene* scene){
	if(CheckCollisionRecs(party[0].collider, self->value.ranEncounter.area)){
		if(party[0].position.x != party[0].oldPosition.x ||
		   party[0].position.y != party[0].oldPosition.y){
			scene->danger -= (float)party[0].speed * *d_time;
			printf("random encounter number: %f\n", scene->danger);
			if(scene->danger  <= 0){
				int chooseEncounter = rand() % 5;
				switch(chooseEncounter){
				case 0:
					currentRandomEncounter = self->value.ranEncounter.id0;
					break;
				case 1:
					currentRandomEncounter = self->value.ranEncounter.id1;
					break;
				case 2:
					currentRandomEncounter = self->value.ranEncounter.id2;
					break;
				case 3:
					currentRandomEncounter = self->value.ranEncounter.id3;
					break;
				case 4:
					currentRandomEncounter = self->value.ranEncounter.id4;
					break;
				default :
					currentRandomEncounter = 0;
				}
				TraceLog(LOG_INFO, "Encounter id: %i", currentRandomEncounter);
				SwitchState(&current_game_state, current_game_states.COMBAT);
			}
		}
	}
	return 0;
}

int teleport_default_update(float* d_time, struct GameObject* self, Scene* scene){
	if(CheckCollisionRecs(party[0].collider, (Rectangle){self->position.x,
														 self->position.y,
														 16.0f, 16.0f})){
		if(strcmp(self->value.teleport.target_map, scene->name) == 0){
			GameObject *tempTel;
			for(tempTel = &scene->objects; tempTel != NULL;
				tempTel = tempTel->next){
				if(tempTel->type == TELEPORT && tempTel-> visible == 1 &&
				   tempTel->value.teleport.id == self->value.teleport.target_id){
					// teleport animation
					party[0].position.x = tempTel->position.x+8.0f;
					party[0].position.y = tempTel->position.y +24.0f;
					scene->camera.offset = (Vector2){screenWidth/2,
													 screenHeight/2};
					break;
				}
			}
		}else{
			strcpy(current_scene_name, self->value.teleport.target_map);
			UnloadRenderTexture(scene->mapFrameBuffer);
			//TODO: do we need a temp here; Seems like I lack some knowledge
			Scene* temp;
			LoadScene(&temp, current_scene_name);
			*scene = *temp;
			return 1;
			// Load scene target_map and put coordinates position of character as the same position as target
		}
	}
	return 0;
}

int collider_default_update(float* d_time, struct GameObject* self, Scene* scene){
	if(CheckCollisionRecs(party[0].collider, self->value.collider.area)){
		if(party[0].moveDirection == 0){
			party[0].position.y += (float)party[0].speed * *d_time;
		}else if(party[0].moveDirection == 1){
			party[0].position.y -= (float)party[0].speed * *d_time;
		}else if(party[0].moveDirection == 2){
			party[0].position.x -= (float)party[0].speed * *d_time;
		}else if(party[0].moveDirection == 3){
			party[0].position.x += (float)party[0].speed * *d_time;
		}
	}
	return 0;
}

#include "central_systems.h"
#include "camera.h"

void LoadScene(Scene**, char*);
void MapDrawSceneName(char* name, Font font, float time);

// TCCState *s;
Scene *currentScene;
char current_scene_name[32] = "test2";
float timeOnMap;

char *TestScript = "#include \"central_systems.h\" \n float x,y;\n void testUpdate(Vector2 *position){\nx= x+ 0.11f;\n y = y +0.2f;\n ;//position->x = x;\n //position->y = y;\n}\n void testDraw(void){\n //DrawRectangle(100, 100, 250, 250, WHITE);\n}";

void MapInit()
{
	textFont = LoadFont("../Data/Fonts/Golden-Sun.ttf");
	timeOnMap = 0.0f;
	LoadScene(&currentScene, current_scene_name); //needs to be scene from loadscene function
	currentScene->danger = rand() % 255;
	//need to be able to make a difference between completely new scene (as in party starts at startposition of map) or that it is not new (for example reload from save, menu or teleport)
    // strcpy(party[0].name, "Hero");
    // strcpy(party[1].name, "Hero2");
    // party[0].isPlayer = 1;
    // party[1].isPlayer = 1;
    party[0].graphic = LoadTexture("../Data/Art/Character_sheet.png");
    party[0].speed = 50.0f;
	// s = tcc_new();

	// if(!s)
	// {
	// 	printf("Can't create TCC context.\n");
	// }
	// tcc_set_output_type(s, TCC_OUTPUT_MEMORY);
	// if(tcc_add_include_path(s, "Data/Linux/include") < 0)
	// {
	// 	printf("Can't include path in TCC env.\n");
	// }
	// if(tcc_add_include_path(s, "Data/Linux/include/libxml2/") < 0)
	// {
	// 	printf("Can't include path in TCC env.\n");
	// }
	// if(tcc_add_library_path(s, "/Data/Linux/lib/") < 0)
	// {
	// 	printf("Can't add lib path in TCC env.\n");
	// }
	// if(tcc_add_library(s, "raylib") < 0)
	// {
	// 	printf("Can't add library in TCC env.\n");
	// }


	// if(tcc_compile_string(s, TestScript) < 0)
	// {
	// 	printf("Compilation error.\n");
	// }

	// tcc_relocate(s, TCC_RELOCATE_AUTO);

	// printf("size of tcc: %lu\n", sizeof(s));
	
}

void MapUpdate(float *dTime)
{
	timeOnMap += *dTime;
	// void (*Tempfunction)(Vector2 *) = tcc_get_symbol(s, "testUpdate");
	// if(Tempfunction == NULL)
	// {
	// 	printf("Symbol not found.\n");
	// }else{
	// 	Tempfunction(&camera.offset);
	// }
	for(GameObject* TempObjects = &currentScene->objects; TempObjects != NULL; TempObjects = TempObjects->next){
		if(TempObjects->visible){
			switch(TempObjects->type)
			{
				case BUILDING:
					//TODO: need to implement buildings
				break;
				
				case NONE:
				break;

				default:
					if(TempObjects->CustomUpdate(dTime, TempObjects, currentScene) == 1){
						printf("INFO: gameobject returned 1. %s, %d\n",
							   TempObjects->name, TempObjects->type);
						return;
					} 
				// TraceLog(LOG_INFO, "The object %s, has no type!", TempObjects->name);
			}
		}
	}
	CharacterUpdate(dTime);
	CameraUpdate(&currentScene->camera, &currentScene->mapFrameBuffer);
	/* camera.target = party[0].position; */
}

void MapDraw(float *dTime)
{
	BeginMode2D(currentScene->camera);
	DrawTextureRec(
                    currentScene->mapFrameBuffer.texture,                  
                    (Rectangle){0, 0, currentScene->mapFrameBuffer.texture.width,
								-currentScene->mapFrameBuffer.texture.height},
                    (Vector2){0.0, 0.0},
                    WHITE);
	// void (*Tempfunction)(void) = tcc_get_symbol(s, "testDraw");
	// if(Tempfunction == NULL)
	// {
	// 	printf("Symbol not found.\n");
	// }else{
	// 	Tempfunction();
	// }
	//collision test (but need scene structure in place?)
	CharacterDraw(dTime);
	for(GameObject* TempObjects = &currentScene->objects; TempObjects != NULL;
		TempObjects = TempObjects->next){
		if(TempObjects->visible){
			DrawRectangle(TempObjects->position.x,TempObjects->position.y,  5, 5, BLUE);
			if(TempObjects->type == NPC &&
			   TempObjects->CustomDraw != NULL){
				TempObjects->CustomDraw(dTime, TempObjects);
			}
		}
	}
	DrawRectangle(0,0,  5, 5, BLUE);
	EndMode2D();
	DrawFPS(5, 5);

	// Draw name of the area on screen, at the beginning
	if(timeOnMap <= 5.0f){
		MapDrawSceneName(currentScene->name, textFont, timeOnMap);
	}
}

void MapInput(float *dTime)
{
	if(IsKeyPressed(KEY_BACKSPACE))
	{
		SwitchState(&current_game_state, current_game_states.MAIN_MENU);
	}
	if(IsKeyReleased(KEY_HOME))
	{
	    party[0].position.x = 8.0f;
	    party[0].position.y = 16.0f;
	}
	if(IsKeyDown(KEY_PAGE_DOWN)){
		printf("INFO: offset %f\n", currentScene->camera.offset.y);
		currentScene->camera.offset.y += 1;		
	}
	if(IsKeyDown(KEY_PAGE_UP)){
		printf("INFO: offset %f\n", currentScene->camera.offset.y);
		currentScene->camera.offset.y -= 1;
	}
	CharacterInput(dTime);
}

void MapDeInit()
{
	// if(s != NULL)
	// {
	// 	printf("tcc delete\n");
	// 	tcc_delete(s);
	// }
	UnloadRenderTexture(currentScene->mapFrameBuffer);
	UnloadTexture(party[0].graphic);
	UnloadFont(textFont);
	/* free(currentScene->objects); */
	free(currentScene);
}

void LoadScene(Scene** curScene, char* name)
{
	char fileName[50];
	Scene* TempScene = (Scene*)malloc(sizeof(Scene));
	tmx_map *sceneMap = NULL; 
	tmx_layer *layer = NULL;
	tmx_object *head = NULL;
	GameObject *HeadGameObject = NULL;

	strcpy(fileName, "../Data/Areas/");
	strcat(fileName, name);
	strcat(fileName, ".tmx");
	
	sceneMap = tmx_load(fileName);
	layer = sceneMap->ly_head;
	TempScene->name = (char *)malloc(sizeof(strlen(name)+1));
	strncpy(TempScene->name, name, strlen(name)+1);
	TempScene->first_time = 1;
	timeOnMap = 0.0f;
	printf("filename of scene: %s\n Name of scene: %s\n",fileName, TempScene->name);

	while(layer)
	{
		if(layer->visible && layer->type == L_OBJGR)
		{
			head = layer->content.objgr->head;
			while(head)
			{
				GameObject *TempObj = (GameObject*)malloc(sizeof(GameObject));
				char* type = "";
				if(head->type == NULL){
					type = "";
				}else{
					type = head->type;
				}
				//TODO: custom update and draw needs to be implemented for the different objects
				*TempObj = game_object_new(head, str_2_go_type(type));
				TempObj->next = HeadGameObject;
				HeadGameObject = TempObj;

				head = head->next;
			}
		}
		layer = layer->next;
	}

	TempScene->objects = *HeadGameObject;
	// GameObject *print = &TempScene->objects;
	// printf("List of Objects:\n");
	// while(print != NULL)
	// {
	// 	printf("[name:%s\nvisible:%i\nposition:%f,%f\ntype:%i\n",print->name, print->visible, print->position.x,print->position.y, print->type);
	// 	if(print->type == COLLIDER)
	// 	{
	// 		printf("layer: %i\nArea: %f,%f,%f,%f",print->value.collider.layer, print->value.collider.area.x,print->value.collider.area.y,print->value.collider.area.width,print->value.collider.area.height);
	// 	}
	// 	printf("]\n");
	// 	print = print->next;
	// }
	RenderTmxMapToFramebuf(!strcmp(TempScene->name, "test_dungeon")?"../Data/Areas/test_dungeon.tmx":"../Data/Areas/test2.tmx", &TempScene->mapFrameBuffer);
	TempScene->camera = CameraInit();
	*curScene = TempScene;
	tmx_map_free(sceneMap);
}

void MapDrawSceneName(char* name, Font font, float time)
{
	if((time*3.0f/10.0f) <= 0.4f){
		DrawTextEx(font, name, (Vector2){screenWidth -
				   (time*3.0f/10.0f)*screenWidth,
				   0.8*screenHeight}, 72.0f, 1.0f,
			   (Color){0.0f,255.0f,255.0f,time*60.0f});
	}
	else{
		DrawTextEx(font, name, (Vector2){0.6f*screenWidth,
				   0.8f*screenHeight}, 72.0f, 1.0f,
			   (Color){0.0f,255.0f,255.0f,255.0f});
	}

}

#include "central_systems.h"


static const int HT_PRIME_1 = 199;
static const int HT_PRIME_2 = 151;
static const int HT_INITIAL_SIZE = 20;

static atSprite HT_DELETED_ITEM = {NULL, (Rectangle){0,0,0,0}};

static atSprite* ht_new_item(const char* key, Rectangle value) {
	atSprite* i = malloc(sizeof(atSprite));
	i->key = malloc(sizeof(key));
	strcpy(i->key, key);
	i->value = value;
	return i;
}

static atTable* ht_new_sized(const int base_size)
{
	atTable* ht = malloc(sizeof(atTable));

	ht->base_size = base_size;
	ht->size = next_prime(ht->base_size);
	ht->count = 0;
	ht->items = calloc((size_t)ht->size, sizeof(atSprite*));
	return ht;
}

atTable* ht_new() 
{
	return ht_new_sized(HT_INITIAL_SIZE);
}

static void ht_resize(atTable* ht, const int base_size)
{
	if(base_size < HT_INITIAL_SIZE)
	{
		return;
	}
	atTable* new_ht = ht_new_sized(base_size);
	for(int i = 0; i < ht->size; i++)
	{
		atSprite* item = ht->items[i];
		if(item != NULL && item != &HT_DELETED_ITEM)
		{
			ht_insert(new_ht, item->key, item->value);
		}
	}

	ht->base_size = new_ht->base_size;
	ht->count = new_ht->count;

	// giving swapping the items between new_ht and ht
	const int tmp_size = ht->size;
	ht->size = new_ht->size;
	new_ht->size = tmp_size;

	atSprite** tmp_items = ht->items;
	ht->items = new_ht->items;
	new_ht->items = tmp_items;

	ht_del_hash_table(new_ht);
}

static void ht_resize_up(atTable* ht)
{
	const int new_size = ht->base_size * 2;
	ht_resize(ht, new_size);
}

static void ht_resize_down(atTable* ht)
{
	const int new_size = ht->base_size / 2;
	ht_resize(ht, new_size);
}

static void ht_del_item(atSprite* i) {
	free(i->key);
	// free(i->value);
	free(i);
}

void ht_del_hash_table(atTable* ht) {
	for(int i = 0; i < ht->size; i++){
		atSprite* item = ht->items[i];
		if(item != NULL)
		{
    			ht_del_item(item);
		}
	}
	free(ht->items);
	free(ht);
}

static int ht_hash(const char* s, const int a, const int m) {
	long hash = 0;
	const int len_s = strlen(s);
	for(int i = 0; i < len_s; i++)
	{
    		hash += (long)pow(a, len_s - (i+1)) * s[i];
    		hash = hash % m;
	}
	return (int)hash;
}

static int ht_get_hash(const char* s, const int num_buckets, const int attempt){
	const int hash_a = ht_hash(s, HT_PRIME_1, num_buckets);
	const int hash_b = ht_hash(s, HT_PRIME_2, num_buckets);
	return (hash_a + (attempt * (hash_b + 1))) % num_buckets;
}

void ht_insert(atTable* ht, const char* key, Rectangle value){
	const int load = ht->count * 100 / ht->size;
	if(load > 70)
	{
		ht_resize_up(ht);
	}
	atSprite* item = ht_new_item(key, value);
	int index = ht_get_hash(item->key, ht->size, 0);
	atSprite* cur_item = ht->items[index];
	int i = 1;
	while(cur_item != NULL){
        	if(cur_item != &HT_DELETED_ITEM)
        	{
            		if(!strcmp(cur_item->key, key))
            		{
                		ht_del_item(cur_item);
                		ht->items[index] = item;
                		return;
            		}
        	}
	atSprite* item = ht_new_item(key, value);
		index = ht_get_hash(item->key, ht->size, i);
		cur_item = ht->items[index];
		i++;
	}
	ht->items[index] = item;
	ht->count++;
}

Rectangle ht_search(atTable* ht, const char* key){
	int index = ht_get_hash(key, ht->size, 0);
	atSprite* item = ht->items[index];
	int i = 1;
	while(item != NULL)
	{
    		if(item != &HT_DELETED_ITEM)
    		{
        		if(strcmp(item->key, key) == 0)
        		{
                		return item->value;
        		}
    		}
    		index = ht_get_hash(key, ht->size, i);
    		item = ht->items[index];
    		i++;
	}
	return (Rectangle){0.0f,0.0f,0.0f,0.0f};
}

void ht_delete(atTable* ht, const char* key)
{
	const int load = ht->count * 100 / ht->size;
	if(load < 10)
	{
		ht_resize_down(ht);
	}
    int index = ht_get_hash(key, ht->size, 0);
    atSprite* item = ht->items[index];
    int i = 1;
    while(item != NULL)
    {
        if(item != &HT_DELETED_ITEM)
        {
            if(!strcmp(item->key, key))
            {
                ht_del_item(item);
                ht->items[index] = &HT_DELETED_ITEM;
            }
        }
        index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];
        i++;
    }
    ht->count--;
}

#include "central_systems.h"
 
Character party[5]; //party[0] is the leader of the party
int currentRandomEncounter;
game_states current_game_states =
{
  .MAIN_MENU = (game_state){
	.init_phase = &MainMenuInit,
	.deinit_phase = &MainMenuDeInit,
	.update_phase = &MainMenuUpdate,
	.draw_phase = &MainMenuDraw,
	.input_phase = &MainMenuInput
  },
	.MENU = (game_state){
	  .init_phase = &MainMenuInit,
	  .deinit_phase = &MainMenuDeInit,
	  .update_phase = &MainMenuUpdate,
	  .draw_phase = &MainMenuDraw,
	  .input_phase = &MainMenuInput
	},
	.COMBAT = (game_state){
	  .init_phase = &CombatInit,
	  .deinit_phase = &CombatDeInit,
	  .update_phase = &CombatUpdate,
	  .draw_phase = &CombatDraw,
	  .input_phase = &CombatInput
	},
	.MAP = (game_state){
	  .init_phase = &MapInit,
	  .deinit_phase = &MapDeInit,
	  .update_phase = &MapUpdate,
	  .draw_phase = &MapDraw,
	  .input_phase = &MapInput
	},
	.DIALOGUE = (game_state){
	  .init_phase = &dialogue_init,
	  .deinit_phase = &dialogue_deinit,
	  .update_phase = &dialogue_update,
	  .draw_phase = &dialogue_draw,
	  .input_phase = &dialogue_input
	},
	.NONE = (game_state){
	  .init_phase   = NULL,
	  .deinit_phase = NULL,
	  .update_phase = NULL,
	  .draw_phase   = NULL,
	  .input_phase  = NULL
	}
};
game_state_stack current_game_state;
int screenWidth;
int screenHeight;
bool debugMode;
bool displayWindow;
Font textFont;
Texture2D g_sprite_sheet;
atTable* charSpriteSheet;

void update_phase (float* d_time, game_state_stack* cur_game_states){
  int i = LENGTH(cur_game_states->states)-1;
  for(i; cur_game_states->states[i].update_phase == NULL; i--){}
  printf("DEBUG: got into update here! %i\n", i);
  cur_game_states->states[i].update_phase(d_time);
}

void draw_phase (float* d_time, game_state_stack* cur_game_states){
  for(int i=0; cur_game_states->states[i].draw_phase != NULL; i++){
	  printf("DEBUG: got into drawphase here! %i\n", i);
	 fputs("DEBUG: got into drawphase here!\n", stderr);
	cur_game_states->states[i].draw_phase(d_time);
  }
}

void input_phase (float* d_time, game_state_stack* cur_game_states){
  int i = LENGTH(cur_game_states->states)-1;
  for(i; cur_game_states->states[i].input_phase == NULL; i--){}
  printf("DEBUG: got into input phase here! %i, %p\n", i, cur_game_states->states[i].input_phase);
  cur_game_states->states[i].input_phase(d_time);
}

void deinit_phases (game_state_stack* cur_game_states){
    for(int i = LENGTH(cur_game_states->states) - 1; i >= 0; i--){
	  if(cur_game_states->states[i].deinit_phase != NULL){
		printf("DEBUG: got into deinit debug phase phase here! %i, %p\n",
			   i, cur_game_states->states[i].deinit_phase);
		pop_state_from_stack(cur_game_states);
	  }
	}
}

void SwitchState (game_state_stack* gameState, game_state switchToState){
  fputs("DEBUG: switching state!\n", stderr);
  deinit_phases(gameState);
  fputs("DEBUG: newstate\n", stderr);
  gameState->states[0] = switchToState;
  gameState->states[0].init_phase();
}

void init_game_state_stack(game_state_stack* cur_game_states){
      for(int i = LENGTH(cur_game_states->states)-1; i >= 0; i--){
		cur_game_states->states[i] = current_game_states.NONE;
		printf("DEBUG: got into init stack phase here! %i, %p\n",
			   i, cur_game_states->states[i].input_phase);
	}
}

void add_state_to_stack (game_state_stack* cur_game_states,
						game_state added_state){
  //add to highest stack
  int i = 0;
  for(i; cur_game_states->states[i].init_phase != NULL; i++){}
  printf("DEBUG: got into add to state stack phase here! %i, %p\n",
		 i, cur_game_states->states[i].input_phase);
  cur_game_states->states[i] = added_state;
  cur_game_states->states[i].init_phase();
}

void pop_state_from_stack (game_state_stack* cur_game_states){
  // pop highest stack
  int i = LENGTH(cur_game_states->states) - 1;
  for(i; cur_game_states->states[i].init_phase == NULL; i--){}
  printf("DEBUG: got into pop state from stack phase here! %i, %p\n",
		 i, cur_game_states->states[i].deinit_phase);
  cur_game_states->states[i].deinit_phase();
  cur_game_states->states[i] = current_game_states.NONE;
}

#include "central_systems.h"

//maybe a stacked state
char* test[4];
int index_test;
void dialogue_init(){
	textFont = LoadFont("../Data/Fonts/Golden-Sun.ttf");
	index_test = 0;
	test[0] = "Welcome to our lovely fruit store! We got all your favourites, like, apples, bananas and man...";
	test[1] = "Ooh, sorry forgot that you aren't affected.";
	test[2] = "Just so many visitors came by that it has become a habit...";
	test[3] = "Hoi";
}

void dialogue_update(float* d_time){
	if(index_test > (LENGTH(test) - 1)){
	  pop_state_from_stack(&current_game_state);
	  /* SwitchState(&current_game_state, current_game_states.MAP); */
	}
	//update position, of the next icon
}

void dialogue_draw(float* d_time) {
  gui_dialog_box(test[index_test],
				 (Vector2){screenWidth/2 -128.0f,
						   screenHeight/2 - 20.0f},
				 16.0f, textFont, GREEN);
  
  //arrow, or other icon that shows there are more lines to go through
}

void dialogue_input(float* d_time){
  if(IsKeyReleased(KEY_X)){
	index_test++;
  }
}

void dialogue_deinit(){
	/* UnloadFont(textFont); */
}

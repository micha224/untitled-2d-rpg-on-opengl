#include "central_systems.h"

int selectItem(int length, int selectIndex, char* direction){
	if(strcmp(direction,"up")==0){
		if(selectIndex <= 0){
			selectIndex = length - 1;
		}else{
			selectIndex = selectIndex - 1;
		}
	}

	if(strcmp(direction,"down")==0){
		if(selectIndex >= length -1){
			selectIndex = 0;
		}else{
			selectIndex = selectIndex + 1;
		}
	}
	return selectIndex;
}

bool GuiButton(Rectangle rec, const char* text, int state, Font textFont){
	bool pressed = false;
	Color textColor = WHITE;
	/* SetTextureFilter(textFont.texture, TEXTURE_FILTER_POINT); */
	float fontSize = BUT_F_SIZE;
	Vector2  textSize = MeasureTextEx(textFont, text,
					  fontSize, 1.0f);
	if (textSize.x > rec.width || textSize.y > rec.height){
		fontSize = (rec.width/textSize.x) * BUT_F_SIZE * (rec.height/textSize.y);
		textSize = MeasureTextEx(textFont, text,
					  fontSize, 1.0f);
	}
	if (state == 1){
		textColor = RED;
		if(IsKeyPressed(KEY_X))
		{
			pressed = true;
		}
	}

	DrawRectangleRounded(rec, 1.0f, 9, YELLOW);
	DrawTextEx(textFont, text, (Vector2){rec.x+rec.width/2-textSize.x/2, rec.y + rec.height/2 - textSize.y/2}, fontSize, 1, textColor);
	return pressed;
}

bool IsFocused(int *cFocus, int* window, int number){
	return (cFocus == window && *window==number)?1:0;
}

bool gui_dialog_box(const char* text, Vector2 position, float font_size, Font font, Color text_colour) {
  /* dialog take precedence over everything else that is */
  Vector2 text_size = MeasureTextEx(font, text, font_size, 1.0f);
  int max_width = (screenWidth - (0.1f * screenWidth)) - position.x;
  int lines = (text_size.x / max_width) + 1; //TODO: not sure about this formula
  const NPatchInfo box_tex = {
	.source = ht_search(charSpriteSheet, "dialogueBox"),       
	.left = 16,
	.top = 16,           
	.right = 16,              
	.bottom = 16,             
	.layout = NPATCH_NINE_PATCH
  };
  Rectangle bg_rec = {
	.x      = position.x,
	.y      = position.y,
	.width  = max_width -
	((max_width - (text_size.x + text_size.x * 0.2f))
	 * (text_size.x < max_width)) + (box_tex.right + box_tex.left),
	.height = (text_size.y * (lines + lines)) + (box_tex.top + box_tex.bottom)
  };
  Rectangle text_rec = {
	.x      = bg_rec.x + box_tex.left,//bg_rec.width * 0.05f, 
	.y      = bg_rec.y + box_tex.top,//bg_rec.height * 0.1f, 
	.width  = bg_rec.width - (box_tex.right + box_tex.left),//bg_rec.width * 0.1f, 
	.height = bg_rec.height - (box_tex.top + box_tex.bottom)//bg_rec.height * 0.1f
  };
  DrawTextureNPatch(g_sprite_sheet, box_tex, bg_rec,
					(Vector2){0.0f, 0.0f}, 0.0f, WHITE);
  if(text == NULL){
	text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cras semper auctor neque vitae tempus quam pellentesque. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Magna ac placerat vestibulum lectus mauris. Urna porttitor rhoncus dolor purus non. Feugiat in ante metus dictum at tempor commodo ullamcorper a. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Accumsan in nisl nisi scelerisque eu ultrices. Libero id faucibus nisl tincidunt. A erat nam at lectus urna duis convallis convallis. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum.";
  }
  DrawTextRecEx(font, text, text_rec, font_size, 1.0f, true, text_colour,
				0, 0, WHITE, WHITE);
  return false;
}

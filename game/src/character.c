#include "central_systems.h"


int framesCounter = 0;
int framesSpeed = 3;
int currentFrame = 0;
enum characterState { DOWN = 0, LEFT = 36, RIGHT = 72, UP = 108, IDLE = 152};
enum characterState chState = DOWN;
Rectangle FrameRec[4] = {{192.0f, 96.0f, 32.0f, 32.0f},{224.0f, 96.0f, 32.0f, 32.0f},{256.0f, 96.0f, 32.0f, 32.0f},{224.0f, 96.0f, 32.0f, 32.0f}};

animController playerAnims;
animClip animations = {"test", 4, 0.3f};

void CharacterInput(float *dTime){
	party[0].oldPosition = party[0].position;
	if(IsKeyReleased(KEY_Q))
	{
		// Setup basic anim controller
		playerAnims.amount = 4;
		playerAnims.curAnim = NULL;
		
		// Fill in the animations
		strcpy(playerAnims.animations[0].name, "up");
		playerAnims.animations[0].frames = 4;
		playerAnims.animations[0].duration = 0.5f;
		playerAnims.animations[0].loop = 0;
		playerAnims.animations[0].playing = 0;
		playerAnims.animations[0].curFrame = 0;
		playerAnims.animations[0].curTime = 0.0f;
		playerAnims.animations[0].keyframes[0].source = ht_search(charSpriteSheet, "up0Rua");
		playerAnims.animations[0].keyframes[1].source = ht_search(charSpriteSheet, "upRua");
		playerAnims.animations[0].keyframes[2].source = ht_search(charSpriteSheet, "up1Rua");
		playerAnims.animations[0].keyframes[3].source = ht_search(charSpriteSheet, "upRua");
		playerAnims.animations[0].keyframes[0].time = 0.0f;
		playerAnims.animations[0].keyframes[1].time = 0.25f;
		playerAnims.animations[0].keyframes[2].time = 0.50f;
		playerAnims.animations[0].keyframes[3].time = 0.75f;
		strcpy(playerAnims.animations[1].name, "down");
		playerAnims.animations[1].frames = 4;
		playerAnims.animations[1].duration = 0.3f;
		playerAnims.animations[1].loop = 0;
		playerAnims.animations[1].playing = 0;
		playerAnims.animations[1].curFrame = 0;
		playerAnims.animations[1].curTime = 0.0f;
		playerAnims.animations[1].keyframes[0].source = ht_search(charSpriteSheet, "down0Rua");
		playerAnims.animations[1].keyframes[1].source = ht_search(charSpriteSheet, "downRua");
		playerAnims.animations[1].keyframes[2].source = ht_search(charSpriteSheet, "down1Rua");
		playerAnims.animations[1].keyframes[3].source = ht_search(charSpriteSheet, "downRua");
		playerAnims.animations[1].keyframes[0].time = 0.0f;
		playerAnims.animations[1].keyframes[1].time = 0.25f;
		playerAnims.animations[1].keyframes[2].time = 0.50f;
		playerAnims.animations[1].keyframes[3].time = 0.75f;
		strcpy(playerAnims.animations[2].name, "left");
		playerAnims.animations[2].frames = 4;
		playerAnims.animations[2].duration = 0.3f;
		playerAnims.animations[2].loop = 0;
		playerAnims.animations[2].playing = 0;
		playerAnims.animations[2].curFrame = 0;
		playerAnims.animations[2].curTime = 0.0f;
		playerAnims.animations[2].keyframes[0].source = ht_search(charSpriteSheet, "left0Rua");
		playerAnims.animations[2].keyframes[1].source = ht_search(charSpriteSheet, "leftRua");
		playerAnims.animations[2].keyframes[2].source = ht_search(charSpriteSheet, "left1Rua");
		playerAnims.animations[2].keyframes[3].source = ht_search(charSpriteSheet, "leftRua");
		playerAnims.animations[2].keyframes[0].time = 0.0f;
		playerAnims.animations[2].keyframes[1].time = 0.25f;
		playerAnims.animations[2].keyframes[2].time = 0.50f;
		playerAnims.animations[2].keyframes[3].time = 0.75f;
		strcpy(playerAnims.animations[3].name, "right");
		playerAnims.animations[3].frames = 4;
		playerAnims.animations[3].duration = 0.3f;
		playerAnims.animations[3].loop = 0;
		playerAnims.animations[3].playing = 0;
		playerAnims.animations[3].curFrame = 0;
		playerAnims.animations[3].curTime = 0.0f;
		playerAnims.animations[3].keyframes[0].source = ht_search(charSpriteSheet, "right0Rua");
		playerAnims.animations[3].keyframes[1].source = ht_search(charSpriteSheet, "rightRua");
		playerAnims.animations[3].keyframes[2].source = ht_search(charSpriteSheet, "right1Rua");
		playerAnims.animations[3].keyframes[3].source = ht_search(charSpriteSheet, "rightRua");
		playerAnims.animations[3].keyframes[0].time = 0.0f;
		playerAnims.animations[3].keyframes[1].time = 0.25f;
		playerAnims.animations[3].keyframes[2].time = 0.50f;
		playerAnims.animations[3].keyframes[3].time = 0.75f;
		

		// animations.frameSource[3] = FrameRec[3];
		// animations.frameSource[0] = FrameRec[0];
		// animations.frameSource[1] = FrameRec[1];
		// animations.frameSource[2] = FrameRec[2];

	}
	if(IsKeyDown(KEY_UP))
	{
		party[0].moveDirection = 0;
		party[0].position.y -= (float)party[0].speed * *dTime;
		chState = UP;
		if(!isAnimPlaying(&playerAnims, "up"))
		{
			playAnimation(&playerAnims, "up", 0);
		}
	}
	else if(IsKeyDown(KEY_DOWN))
	{
		party[0].moveDirection = 1;
		party[0].position.y += (float)party[0].speed * *dTime;
		chState = DOWN;
		if(!isAnimPlaying(&playerAnims,"down"))
		{
			playAnimation(&playerAnims, "down", 0);
		}
	}
	else if(IsKeyDown(KEY_RIGHT))
	{
		party[0].moveDirection = 2;
		party[0].position.x += (float)party[0].speed * *dTime;
		chState = RIGHT;
		if(!isAnimPlaying(&playerAnims, "right"))
		{
			playAnimation(&playerAnims, "right", 0);
		}
	}
	else if(IsKeyDown(KEY_LEFT))
	{
		party[0].moveDirection = 3;
		party[0].position.x -= (float)party[0].speed * *dTime;
		chState = LEFT;
		if(!isAnimPlaying(&playerAnims,"left"))
		{
			playAnimation(&playerAnims, "left", 0);
		}
	}
	else
	{
		chState = IDLE;
	}
}

void CharacterUpdate(float *dTime){
	updateAnimController(&playerAnims, dTime);
	party[0].collider = (Rectangle){party[0].position.x-8.0f,party[0].position.y-6.0f,15.0f,11.0f};
}

void CharacterDraw(float *dTime){
	if(debugMode){
		DrawRectangleRec(party[0].collider, RED);
	}
	DrawTextureRec(party[0].graphic, playerAnims.graphic,
		       (Vector2){party[0].position.x-19.0f,
			       party[0].position.y-32.0f},WHITE);
}

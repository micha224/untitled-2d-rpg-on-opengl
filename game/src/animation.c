#include "central_systems.h"

// here comes the functions and stuff for animation system.
// need to be able to play animation, check if entity is playing an animation already, stop playing an animation.
//anim controller, with an update function to update the animation
//anim controller holds, anim clips and the animclips hold reference to spritesheet var? 

void playAnimation(animController* controller, char* clip, int looping)
{
	stopAnimation(controller);
	for(int i = 0; i < controller->amount; i++){
		if(!strcmp(controller->animations[i].name, clip)){
			controller->animations[i].playing = 1;	
			controller->animations[i].loop = looping;	
			controller->curAnim = &controller->animations[i];
		}
	}
}

void updateAnimController(animController* controller, float* deltaTime)
{
	if(controller->curAnim == NULL){
		return;
	}

	float interval = controller->curAnim->duration * controller->curAnim->keyframes[controller->curAnim->curFrame+1].time;
	/* printf("interval: %f, %f, %i\n", interval, controller->curAnim->curTime, controller->curAnim->curFrame); */
	if(controller->curAnim->curTime >= interval){
		controller->curAnim->curFrame++;
		interval = controller->curAnim->duration * controller->curAnim->keyframes[controller->curAnim->curFrame+1].time;
	}
	if(controller->curAnim->curFrame > (controller->curAnim->frames - 1))
	{
		if(controller->curAnim->loop)
		{
			controller->curAnim->curTime = 0.0f;
			controller->curAnim->curFrame = 0;
		}
		else
		{
			// TraceLog(LOG_ERROR, "stopped the animation");
			stopAnimation(controller);
			return;
		}
	}

	controller->graphic = controller->curAnim->keyframes[controller->curAnim->curFrame].source;
	controller->curAnim->curTime += *deltaTime;
}

void stopAnimation(animController* controller)
{
	for(int i = 0; i < controller->amount; i++)
	{
		controller->animations[i].playing = 0;
		controller->animations[i].curFrame = 0;
		controller->animations[i].curTime = 0.0f;
	}
	controller->curAnim = NULL;
}

int isAnimPlaying(animController* controller, char* clip)
{
	for(int i = 0; i < controller->amount; i++)
	{
		if(!strcmp(controller->animations[i].name, clip))
		{
			if(controller->animations[i].playing)
			{
				return 1;
			}
		}
	}	
	return 0;
}

#include "central_systems.h"

char *mainMessage = "Welcome to combat phase";
bool goingFoward = 1;
Character *Entities[20];
Character Enemies[15];
int enemyLength = 0;
int n =0;
Vector2 EnemyLocation[15] = {	{400,50},{325,205},
								{350,380},{300,50},
								{225,205},{250,380},
								{200,50},{125,205},
								{150,380},{500,50},
								{425,205},{450,380},
								{300,450},{375,450},
								{200,450}
							};
Vector2 PlayerLocation[5] = { {650,75},{650,220},{550,185},{550,240},{550,295}};

int cuurrentItemLength;
int selectTarget = 0;
GuiItem playerOptions[4] = {"Attack", "Addiction", "Items", "Flee"};
int selectPlayerMenu = 0;
int unfocusedMenu = 0;
int currentActor = 0;
int turnEnd = 0;
int turnStart = 1;
int *cuurrentFocus = &unfocusedMenu;

int attack = 0;
int selectedTarget = 0;
int targetSelectState = 0;
char qAction[32];

typedef struct combatLog
{
	int show;
	int counter;
	Rectangle area;// {1000,500,200,200};
	char lengthDisplay[250];
	char *text;// = "This is the start combat\n";
} COMLOG;

COMLOG cLog;

Texture2D heroFace;
Texture2D backgroundImg;

// Win or Lose Condition
int Victory = 0; //-1 means defeat and 1 means win, maybe a enum for this?
int hpParty, hpEnemy = 0;
//TODO: how to handle tuns in the combat
//TODO: combat log that can be shown in battle
//TODO: that enemies perform their actions
//TODO: that character have their own specific action shown
//TODO: animation manager needed for animations
//TODO: Need to think if I am doing attack, special functions via tcc from the seperate files or if I precode the functions and call them by the ID.
int defaultTestAttack(Character *, Character *, float *);
void apCombatLog(char*);
void targetCreature();
void LoadEncounter();

void CombatInit()
{
	textFont = LoadFont("../Data/Fonts/Golden-Sun.ttf");
	//init victory
	Victory = 0;
	//init combatLog
	cLog.text = calloc(strlen("You've encountered\n"),strlen("You've encountered\n"));
	strcat(cLog.text, "You've encountered ");
	cLog.counter = 0;
	cLog.show = 0;
	cLog.area = (Rectangle){1000,500,200,200};
	//load encounter enemies
	enemyLength = 0;
	LoadEncounter();
	// strcpy(Enemies[0].name,"Goblin");
	// Enemies[0].level = 1;
	// Enemies[0].experience = 10;
	// Enemies[0].health = 150;
	// Enemies[0].maxHealth = 150;
	// Enemies[0].attack = 10;
	// Enemies[0].speed = 5;
	// strcpy(Enemies[1].name,"Goblin1");
	// Enemies[1].level = 1;
	// Enemies[1].experience = 10;
	// Enemies[1].health = 150;
	// Enemies[1].maxHealth = 150;
	// Enemies[1].attack = 10;
	// Enemies[1].speed = 1;
	// strcpy(Enemies[2].name,"Goblin2");
	// Enemies[2].level = 1;
	// Enemies[2].experience = 10;
	// Enemies[2].health = 150;
	// Enemies[2].maxHealth = 150;
	// Enemies[2].attack = 10;
	// Enemies[2].speed = 10;
	// Enemies[0].icon = Enemies[1].icon = Enemies[2].icon = (Rectangle){0.0f,48.0f,48.0f,48.0f};
	// Enemies[0].battleGraph = Enemies[1].battleGraph = Enemies[2].battleGraph = (Rectangle){128.0f,64.0f,32.0f,32.0f};

	//temporary load in of party members for testing
	strcpy(party[0].name, "Hero");
    strcpy(party[1].name, "Hero2");    
    party[0].health = 99;
    party[0].maxHealth = 100;
    party[1].health = 166;
    party[1].maxHealth = 300;
    party[0].isPlayer = 1;
    party[1].isPlayer = 1;
    party[0].speed = 50.0f;
    party[1].speed = 8.0f;
    party[0].attack = 100;
    party[1].attack = 75;
    party[0].icon = ht_search(charSpriteSheet, "iconRua");
    party[1].icon = ht_search(charSpriteSheet, "iconPing");
    party[0].battleGraph = ht_search(charSpriteSheet, "leftRua");
    party[1].battleGraph = ht_search(charSpriteSheet, "leftPing");


    Image heFace = LoadImage("../Data/Art/hero_face.png");
    ImageResizeNN(&heFace, 100, 100);
    heroFace = LoadTextureFromImage(heFace);
    UnloadImage(heFace);

    Image checkedBg = GenImageChecked(screenWidth, screenHeight, 32, 32, PINK, VIOLET);
    backgroundImg = LoadTextureFromImage(checkedBg);
    UnloadImage(checkedBg);

	//determine and store turn order
	int i = 0, j = 0, partyLength = 0;
	Character *temp = &Enemies[0];
	while(strcmp(party[i].name, ""))
	{
		partyLength++;
		i++;
	}
	n = enemyLength + partyLength;
	cuurrentItemLength = n;
	turnStart = 1;
	currentActor = 0;
	turnEnd = 0;
	// TraceLog(LOG_INFO,"n = %i,%i\n", partyLength,n);
	for(i = 0; i < n; i++)
	{
		if(i < enemyLength)
		{
			Entities[i] = &Enemies[i];
		}
		else
		{
			Entities[i] = &party[(i-enemyLength)];
		}
	}
	for(i = 0; i < n; i++)
	{
		for(j=i+1; j<n;j++)
		{
			if(Entities[i]->speed < Entities[j]->speed)
			{
				temp = Entities[i];
				Entities[i] = Entities[j];
				Entities[j] = temp;
			}
		}
	}
	// free(temp);
}

void CombatUpdate(float *dTime)
{
	if(!turnEnd && Victory == 0)
	{
		if(!Entities[currentActor]->isPlayer)
		{
			static int target;
			if(turnStart)
			{
				target = rand() % n;
				turnStart=0;
			}
			int att = defaultTestAttack(Entities[currentActor], Entities[target], dTime);
			//att==0?(turnEnd=1):(turnEnd=0);
		}
		if(Entities[currentActor]->isPlayer)
		{
			if(turnStart)
			{
				cuurrentFocus = &selectPlayerMenu;
				cuurrentItemLength = LENGTH(playerOptions);
				turnStart=0;
			}
		}
	}
	if(turnEnd)
	{
		// make it so that when health of entity is below zero, make it zero
		for(int h = 0; h < n; h++)
		{
			if(!Entities[h]->isPlayer)
			{
				hpEnemy += Entities[h]->health;
			}
			else
			{
				hpParty += Entities[h]->health;
			}
		}
		if(hpEnemy <= 0)
		{
			Victory = 1;
		}
		else if(hpParty <=0)
		{
			Victory = -1;
		}
		else
		{
			hpEnemy = hpParty = 0;
		}
		currentActor++;
		if(currentActor >= n)
		{
			currentActor = 0;
		}
		turnEnd = 0;
		turnStart = 1;
	}
}

void CombatDraw(float *dTime)
{
	// Draw brackground scene
	DrawTexture(backgroundImg, screenWidth/2- backgroundImg.width/2, screenHeight/2 - backgroundImg.height/2,WHITE);

	// Draw Turn order
	DrawTextEx(textFont, "TURN ORDER", (Vector2){0.0f, 0.0f}, 24.0f, 2.4f, GREEN);
	DrawTextEx(textFont, "Press \"C\" for combat Log!",
			   (Vector2){screenWidth-500.0f, screenHeight-30.0f}, 24.0f, 2.4f, GREEN);
	DrawTextEx(textFont, Entities[currentActor]->name, (Vector2){53.0f, 25.0f},
			   24.0f, 2.4f, RED);
	// DrawTextureEx(heroFace, (Vector2){0.0f,25.0f}, 0.0f, 0.5f, RED);
	DrawTexturePro(g_sprite_sheet, Entities[currentActor]->icon, (Rectangle){0.0f,25.0f, 50.0f, 50.0f}, (Vector2){0.0f,0.0f}, 0.0f, RED);
	float boxLength = heroFace.height/1.8f+25.0f;
	int next =1;
	while(boxLength <= 0.5*screenHeight)
	{
		if(currentActor+next >= n)
		{
			next = -currentActor;
		}
		DrawTextEx(textFont, Entities[currentActor+next]->name,
				   (Vector2){53.0f, boxLength}, 24.0f, 2.4f, WHITE);
		// DrawTextureEx(heroFace, (Vector2){0.0f,boxLength}, 0.0f, 0.5f, WHITE);
		DrawTexturePro(g_sprite_sheet, Entities[currentActor+next]->icon, (Rectangle){0.0f,boxLength, 50.0f, 50.0f}, (Vector2){0.0f,0.0f}, 0.0f, WHITE);
		boxLength += heroFace.height/1.8f;
		next++;
	}

	// Draw players and enemies
	int i, b = 0, c = 0;
	for(i = 0; i < n; i++)
	{
		char healthtext[32];
		snprintf(healthtext,32,"Health: %d/%d",Entities[i]->health,Entities[i]->maxHealth);
		if(Entities[i]->isPlayer)
		{
			//Draw the stats card of the players in bottom of screen
			float cHealth = ((float)Entities[i]->health/(float)Entities[i]->maxHealth);
			char hpIdentifier[5], addIdentifier[5];
			snprintf(hpIdentifier,5,"%d",Entities[i]->health);
			snprintf(addIdentifier,5,"%d",Entities[i]->addiction);
			// DrawCircle(0.1f*screenWidth+b*0.2f*screenWidth, 0.9f*screenHeight, 50, BROWN);

			// DrawTexture(heroFace,0.1f*screenWidth+b*0.15f*screenWidth-50, 0.9f*screenHeight-50, WHITE);
			DrawTexturePro(g_sprite_sheet, Entities[i]->icon,
						   (Rectangle){0.1f*screenWidth+b*0.15f*screenWidth-50,
									   0.9f*screenHeight-50, 100.0f, 100.0f},
						   (Vector2){0.0f,0.0f}, 0.0f, WHITE);
			DrawRectangle(0.13f*screenWidth+b*0.15f*screenWidth, 0.85f*screenHeight,
						  0.07f*screenWidth*cHealth, 20, GREEN);
			DrawTextEx(textFont, hpIdentifier,
					   (Vector2){0.13f*screenWidth+b*0.15f*screenWidth,
								 0.85f*screenHeight}, 24.0f, 2.4f, YELLOW);
			DrawRectangle(0.13f*screenWidth+b*0.15f*screenWidth,
						  0.9f*screenHeight, 0.07f*screenWidth, 20, PURPLE);
			DrawTextEx(textFont, addIdentifier,
					   (Vector2){0.13f*screenWidth+b*0.15f*screenWidth,
								 0.9f*screenHeight}, 24.0f, 2.4f, YELLOW);
			DrawTextEx(textFont, Entities[i]->name,
					   (Vector2){0.07f*screenWidth+b*0.15f*screenWidth,
								 0.89f*screenHeight}, 24.0f, 2.4f, RED);
			//Draw graphics for the combat scene of characters
			// DrawRectangleV(PlayerLocation[b],(Vector2){50,50},IsFocused(cuurrentFocus, &selectTarget,i)?RED:WHITE);
			DrawTexturePro(g_sprite_sheet, Entities[i]->battleGraph, (Rectangle){PlayerLocation[b].x,PlayerLocation[b].y,128.0f,128.0f},(Vector2){0.0f,0.0f},0.0f, IsFocused(cuurrentFocus, &selectTarget,i)?RED:WHITE);
			if(IsFocused(cuurrentFocus, &selectTarget,i)){
				DrawRectangleRoundedLines((Rectangle){PlayerLocation[b].x + 50.0f,PlayerLocation[b].y-20.0f,MeasureText(healthtext, 24)+1,50.0f}, 0.5f, 9, 1.0f, GRAY);
				DrawTextEx(textFont, Entities[i]->name,
						   (Vector2){PlayerLocation[b].x + 51.0f,
									 PlayerLocation[b].y-20.0f}, 24.0f, 2.4f, WHITE);
				DrawTextEx(textFont, healthtext,
						   (Vector2){PlayerLocation[b].x + 51.0f,
									 PlayerLocation[b].y}, 24.0f, 2.4f, WHITE);
			}
			b++;
		}
		else if(!Entities[i]->isPlayer)
		{
			// DrawRectangleV(EnemyLocation[c],(Vector2){100,100}, IsFocused(cuurrentFocus, &selectTarget,i)?RED:BLUE);
			DrawTexturePro(g_sprite_sheet, Entities[i]->battleGraph, (Rectangle){EnemyLocation[c].x,EnemyLocation[c].y,128.0f,128.0f},(Vector2){0.0f,0.0f},0.0f, IsFocused(cuurrentFocus, &selectTarget,i)?RED:WHITE);
			if(IsFocused(cuurrentFocus, &selectTarget,i))
			{
				DrawRectangleRoundedLines((Rectangle){EnemyLocation[c].x + 50.0f,EnemyLocation[c].y-20.0f,MeasureText(healthtext, 24)+1,50.0f}, 0.5f, 9, 1.0f, GRAY);
 				DrawTextEx(textFont, Entities[i]->name,
						   (Vector2){EnemyLocation[c].x + 51.0f,
									 EnemyLocation[c].y-20.0f}, 24.0f, 2.4f, WHITE);
				DrawTextEx(textFont, healthtext,
						   (Vector2){EnemyLocation[c].x + 51.0f,
									 EnemyLocation[c].y}, 24.0f, 2.4f, WHITE);
			}
			c++;
		}
	}

	// Draw the command menu when it is the turn of a playable character
	if(Entities[currentActor]->isPlayer)
	{
		if(GuiButton((Rectangle){0.8*screenWidth, 0.2*screenHeight,100,60}, playerOptions[0].name, IsFocused(cuurrentFocus, &selectPlayerMenu, 0), GetFontDefault()))
		{
			targetCreature();
			strcpy(qAction, "default");
		}
		if(GuiButton((Rectangle){0.8*screenWidth, 0.25*screenHeight,100,60}, playerOptions[1].name, IsFocused(cuurrentFocus, &selectPlayerMenu, 1), textFont))
		{

		}
		if(GuiButton((Rectangle){0.8*screenWidth, 0.3*screenHeight,100,60}, playerOptions[2].name, IsFocused(cuurrentFocus, &selectPlayerMenu, 2), textFont))
		{

		}
		if(GuiButton((Rectangle){0.8*screenWidth, 0.35*screenHeight,100,60}, playerOptions[3].name, IsFocused(cuurrentFocus, &selectPlayerMenu, 3), textFont))
		{
			SwitchState(&current_game_state, current_game_states.MAP);
		}
	}

	// Draw the commmand log when toggled
	if(cLog.show)
	{
		strncpy(cLog.lengthDisplay, cLog.text+cLog.counter, 250);
		DrawRectangleRounded(cLog.area, 0.1f, 9, (Color){GRAY.r,GRAY.g,GRAY.b, 125});
		DrawRectangleRoundedLines(cLog.area, 0.1f, 9, 3, GRAY);
		DrawTextRec(textFont, cLog.lengthDisplay, cLog.area, 12.0f, 1.0f, 1, WHITE);
	}

	// Draw the victory or defeat screen
	if(Victory == 1)
	{
		DrawText("You Are Vicotorious!!!", screenWidth/2, screenHeight/2, 48, RED);
	}
	else if(Victory == -1)
	{
		DrawText("You Are Defeated!!!", screenWidth/2, screenHeight/2, 48, RED);
	}

	DrawFPS(0.01*screenWidth, 0.95*screenHeight);
}

void CombatInput(float *dTime)
{
	if(IsKeyPressed(KEY_BACKSPACE))
	{
	  SwitchState(&current_game_state, current_game_states.MAIN_MENU);
	}
	if(attack == 1)
	{
		attack = defaultTestAttack(Entities[currentActor], Entities[selectedTarget], dTime);
	}
	if(targetSelectState == 1)
	{
		if(IsKeyPressed(KEY_X))
		{
			selectedTarget = *cuurrentFocus;
			if(strcmp(qAction, "default") == 0)
			{
				attack = 1;
				targetSelectState = 0;
				cuurrentFocus = &unfocusedMenu;
			}
		}
	}
	if(IsKeyReleased(KEY_UP))
	{
		if(cLog.show)
		{
			cLog.counter <=0?(cLog.counter = 0):(cLog.counter -= 20);
		}
		else
		{
			*cuurrentFocus = selectItem(cuurrentItemLength, *cuurrentFocus, "up");
		}
	} else if(IsKeyReleased(KEY_DOWN))
	{
		if(cLog.show)
		{
			if(cLog.counter+250 <= strlen(cLog.text))
			{
				cLog.counter += 20;
			}
		}
		else
		{
			*cuurrentFocus = selectItem(cuurrentItemLength, *cuurrentFocus, "down");
		}
	}
	if(IsKeyReleased(KEY_C))
	{
		cLog.show = !cLog.show;
	}
}

void CombatDeInit()
{
	free(cLog.text);
	UnloadTexture(heroFace);
	UnloadFont(textFont);
}

int defaultTestAttack(Character *caster, Character *target, float *dTime)
{
	static float deltaTime = 0.0f;
	char AttackMessage[100];
	snprintf(AttackMessage,100,"%s attacked %s for %d damage.\n",caster->name, target->name, caster->attack);
	if(deltaTime == 0.0f)
	{
		target->health -= caster->attack;
	}
	if(deltaTime <= 1.5f)
	{
		DrawText(AttackMessage, screenWidth/2-105.0f, screenHeight/2,34,WHITE);
		deltaTime += *dTime;
	}
	else
	{
		deltaTime = 0.0f;
		turnEnd = 1;
		apCombatLog(AttackMessage);
		return 0;
	}
	return 1;
}

void targetCreature()
{
	cuurrentFocus = &selectTarget;
	cuurrentItemLength = n;
	targetSelectState = 1;
}

void apCombatLog(char* newS)
{
	TraceLog(LOG_INFO, "sizeof combat log:%i",strlen(cLog.text));
	if((cLog.text = realloc(cLog.text,strlen(cLog.text)+strlen(newS)+1)) != NULL)
	{
		strcat(cLog.text, newS);
	}
}

void LoadEncounter()
{
	xmlDoc *db = xmlReadFile("../Data/Databases/Encounters.xml", NULL, 0);
	xmlDoc *enDb = xmlReadFile("../Data/Databases/enemydb.xml", NULL, 0);

	if(db != NULL)
	{
		xmlNode *rootElement = xmlDocGetRootElement(db);
		xmlNodePtr node = rootElement->children;
		// TraceLog(LOG_ERROR, "%s", rootElement->children->name);
		for(int i= 0; i < xmlChildElementCount(rootElement); i++)
		{
			int temp = atoi((char *)xmlGetProp(node,(xmlChar *)"id"));
			if( temp == currentRandomEncounter)
			{
				int EnemyTypes = xmlChildElementCount(node) - 1;
				node = node->children;
				char nameEncounter[32];
				snprintf(nameEncounter,32,"%s\n",(char *)xmlNodeGetContent(node));
				apCombatLog(nameEncounter);
				node = node->next;
				//cycle through enemy nodes
				for(int b = 0; b < EnemyTypes; b++)
				{
					//load the enemies from enemy db and into enemy array
					if(enDb == NULL)
					{
						TraceLog(LOG_ERROR,"Enemy database couldn't be loaded!");
					}
					xmlNode *enRoot = xmlDocGetRootElement(enDb);
					xmlNodePtr enNode, enName, enStats;
					for(enNode = enRoot->children; enNode != NULL; enNode = enNode->next)
					{
						if(!strcmp((char *)xmlNodeGetContent(node->children), (char *)xmlGetProp(enNode, (xmlChar *)"id")))
						{
							enName = enNode->children;
							enStats = enName->next->next->next->next;
							// TraceLog(LOG_ERROR,"Enemy type id requested: %s, Enemy id in enemy database: %s", (char *)xmlNodeGetContent(node->children), enNode->children->next->next->next->next->name);
							break;
						}
					}
					
					int count = atoi((char *)xmlNodeGetContent(node->children->next));
					for(int e = 0; e < count; e++)
					{
						strcpy(Enemies[enemyLength].name, (char *)xmlNodeGetContent(enName));
						Enemies[enemyLength].icon = ht_search(charSpriteSheet, (char *)xmlNodeGetContent(enName->next->children));
						Enemies[enemyLength].battleGraph = ht_search(charSpriteSheet, (char *)xmlNodeGetContent(enName->next->children->next));
						Enemies[enemyLength].level = atoi((char *)xmlNodeGetContent(enStats->children));
						Enemies[enemyLength].experience = atoi((char *)xmlNodeGetContent(enStats->children->next));
						Enemies[enemyLength].health = atoi((char *)xmlNodeGetContent(enStats->children->next->next));
						Enemies[enemyLength].maxHealth = atoi((char *)xmlNodeGetContent(enStats->children->next->next->next));
						Enemies[enemyLength].addiction = atoi((char *)xmlNodeGetContent(enStats->children->next->next->next->next));
						Enemies[enemyLength].attack = atoi((char *)xmlNodeGetContent(enStats->children->next->next->next->next->next));
						Enemies[enemyLength].accuracy = atoi((char *)xmlNodeGetContent(enStats->children->next->next->next->next->next->next));
						Enemies[enemyLength].speed = atoi((char *)xmlNodeGetContent(enStats->children->next->next->next->next->next->next->next));
						Enemies[enemyLength].defence = atoi((char *)xmlNodeGetContent(enStats->children->next->next->next->next->next->next->next->next));
						Enemies[enemyLength].spDefence = atoi((char *)xmlNodeGetContent(enStats->children->next->next->next->next->next->next->next->next->next));
						Enemies[enemyLength].attraction = 0;
						Enemies[enemyLength].isPlayer = 0;
						// TraceLog(LOG_ERROR, "Encounter name: %i", count);
						enemyLength++;
					}
					node = node->next;
				}

				break;
			}
			node = node->next;
		}

	}
	xmlFreeDoc(enDb);
	xmlFreeDoc(db);
}

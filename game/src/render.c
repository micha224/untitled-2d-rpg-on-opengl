/*******************************************************************************************
*
*   raylib [texture] example - Import and display of Tiled map editor map
*
*   This example has been created using raylib 2.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2017 Ramon Santamaria (@raysan5)
*
********************************************************************************************/
// #include <stdlib.h>

// #include <raylib.h>
// #include <tmx.h>


// // Helper function for the TMX lib to load a texture from a file
// Texture2D *LoadMapTexture(const char *fileName);

// // Helper function for the TMX lib to unload a texture that was previously loaded
// void UnloadMapTexture(Texture2D *tex);

// // Read a Tile map editor TMX map file and render the map into RenderTexture2D. 
// // This is the main part of this example.
// // This must be called after InitWindow().
// void RenderTmxMapToFramebuf(const char *mapFileName, RenderTexture2D *buf);

// // Frame buffer into which the map is rendered
// RenderTexture2D mapFrameBuffer;

// int main()
// {
//     // Initialization
//     //--------------------------------------------------------------------------------------
//     int screenWidth = 800;
//     int screenHeight = 640;
//     Vector2 player;     // This player doesn't have a box or a sprite. It's just a point.
//     Camera2D camera;

//     InitWindow(screenWidth, screenHeight, "raylib [core] example - 2d camera");

//     // Load Tiled TMX map and render it to the frame buffer
//     RenderTmxMapToFramebuf("/media/micha/2TB HDD/shared with linux/micha/Documents/Programming projects/Untitled RPG/untitled-2d-rpg-on-opengl/Resources/test2.tmx", &mapFrameBuffer);

//     player.x = screenWidth / 2;
//     player.y = screenHeight / 2;
//     camera.target = (Vector2){ player.x, player.y };
//     camera.offset = (Vector2){ 0, 0 };
//     camera.rotation = 0.0;
//     camera.zoom = 1.0;
//     SetWindowPosition(1920,GetMonitorHeight(1)/3);
//     SetTargetFPS(60);
//     //--------------------------------------------------------------------------------------

//     // Main game loop
//     while (!WindowShouldClose())    // Detect window close button or ESC key
//     {
//         // Update
//         //----------------------------------------------------------------------------------
//         if (IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_D))
//         {
//             player.x += 4;              // Player movement
//             camera.offset.x -= 4;       // Camera displacement with player movement
//         }
//         else if (IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_A))
//         {
//             player.x -= 4;              // Player movement
//             camera.offset.x += 4;       // Camera displacement with player movement
//         }
//         else if (IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_S))
//         {
//             player.y += 4;              // Player movement
//             camera.offset.y -= 4;       // Camera displacement with player movement
//         }
//         else if (IsKeyDown(KEY_UP) || IsKeyDown(KEY_W))
//         {
//             player.y -= 4;              // Player movement
//             camera.offset.y += 4;       // Camera displacement with player movement
//         }
//         // Camera target follows player
//         camera.target = (Vector2){ player.x, player.y };
//         //----------------------------------------------------------------------------------

//         // Draw
//         //----------------------------------------------------------------------------------
//         BeginDrawing();
//             ClearBackground(RAYWHITE);

//             BeginMode2D(camera);
//                 // Flip along the y axis because OpenGL origin is at bottom left corner while Raylib is top left
//                 DrawTextureRec(
//                     mapFrameBuffer.texture,                  
//                     (Rectangle){0, 0, mapFrameBuffer.texture.width, -mapFrameBuffer.texture.height},
//                     (Vector2){0.0, 0.0},
//                     WHITE);
//             EndMode2D();
//         EndDrawing();
//         //----------------------------------------------------------------------------------
//     }

//     // De-Initialization
//     //--------------------------------------------------------------------------------------   
//     UnloadRenderTexture(mapFrameBuffer);
//     CloseWindow();        // Close window and OpenGL context
//     //--------------------------------------------------------------------------------------

//     return 0;
// }

#include "central_systems.h"

Texture2D *LoadMapTexture(const char *fileName)
{
    Texture2D *tex = (Texture2D *)malloc(sizeof(Texture2D));
    if (tex != NULL) 
    {
        *tex = LoadTexture(fileName);
        TraceLog(LOG_INFO, "TMX texture loaded from %s", fileName);
        return tex;
    }
    return NULL;
}

void UnloadMapTexture(Texture2D *tex)
{
    if (tex != NULL) 
    {
        UnloadTexture(*tex);
        free(tex);
    }
}

void DrawTmxLayer(tmx_map *map, tmx_layer *layer)
{
    unsigned long row, col;
    unsigned int gid;
    unsigned int flip;
    tmx_tile *tile;
    unsigned int tile_width;
    unsigned int tile_height;
    Rectangle sourceRect;
    Rectangle destRect;
    Texture2D *tsTexture; // tileset texture
    float rotation = 0.0;
    Vector2 origin = {0.0, 0.0};

    for (row = 0; row < map->height; row++)
    {
        for (col = 0; col < map->width; col++)
        {
            gid = layer->content.gids[(row * map->width) + col];
            flip = gid & ~TMX_FLIP_BITS_REMOVAL;    // get flip operations from GID
            gid = gid & TMX_FLIP_BITS_REMOVAL;      // remove flip operations from GID to get tile ID
            tile = map->tiles[gid];
            if (tile != NULL)
            {
                // Get tile's texture out of the tileset texture
                if (tile->image != NULL)
                {
                    tsTexture = (Texture2D *)tile->image->resource_image;
                    tile_width = tile->image->width;
                    tile_height = tile->image->height;
                }
                else
                {
                    tsTexture = (Texture2D *)tile->tileset->image->resource_image;
                    tile_width = tile->tileset->tile_width;
                    tile_height = tile->tileset->tile_height;
                }

                sourceRect.x = tile->ul_x;
                sourceRect.y = tile->ul_y;
                sourceRect.width = destRect.width = tile_width;
                sourceRect.height = destRect.height = tile_height;
                destRect.x = col * tile_width;
                destRect.y = row * tile_height;

                // Deal with flips
                origin.x = 0.0;
                origin.y = 0.0;
                rotation = 0.0;
                switch(flip)
                {
                    case TMX_FLIPPED_DIAGONALLY:
                    {
                        sourceRect.height = -1 * sourceRect.height;
                        rotation = 90.0;
                    } break;
                    case TMX_FLIPPED_VERTICALLY:
                    {
                        sourceRect.height = -1 * sourceRect.height;
                    } break;
                    case TMX_FLIPPED_DIAGONALLY + TMX_FLIPPED_VERTICALLY:
                    {
                        rotation = -90.0;
                    } break;
                    case TMX_FLIPPED_HORIZONTALLY:
                    {
                        sourceRect.width = -1 * sourceRect.width;
                    } break;
                    case  TMX_FLIPPED_DIAGONALLY + TMX_FLIPPED_HORIZONTALLY:
                    {
                        rotation = 90.0;
                    } break;
                    case TMX_FLIPPED_HORIZONTALLY + TMX_FLIPPED_VERTICALLY:
                    {
                        rotation = 180.0;
                    } break;
                    case TMX_FLIPPED_DIAGONALLY + TMX_FLIPPED_HORIZONTALLY + TMX_FLIPPED_VERTICALLY:
                    {
                        sourceRect.width = -1 * sourceRect.width;
                        rotation = 90.0;
                    } break;
                    default:
                    {
                        origin.x = 0.0;
                        origin.y = 0.0;
                        rotation = 0.0;
                    } break;
                }

                // Adjust origin to rotate around the center of the tile, 
                // which means destination recangle's origin must be adjusted.
                if (rotation != 0.0)
                {
                    origin.x = tile_width / 2;
                    origin.y = tile_height / 2;
                    destRect.x += tile_width / 2;
                    destRect.y += tile_height / 2;
                }

                // TODO: Take layer opacity into account
                DrawTexturePro(*tsTexture, sourceRect, destRect, origin, rotation, WHITE);
            }
        }
    }
}

void DrawTmxObjectLayerDebug(tmx_map *map, tmx_layer *layer, struct ObjectTypes *defaultOT)
{
    tmx_object *currentObject;
    unsigned int drawOrder = layer->content.objgr->draworder; 
    unsigned int objectType;
    int visible;
    float rotation;
    Font textFont = GetFontDefault();
    //reorder the object group based on draw order (default is index)
    switch(drawOrder)
    {
        case G_NONE:
        {
            TraceLog(LOG_ERROR, "The Object Layer does not have a draworder.");
        } break;
        case G_INDEX:
        {
            TraceLog(LOG_INFO, "The Object Layer has a draworder that uses the indices of object array.");
        } break;
        case G_TOPDOWN:
        {
            TraceLog(LOG_INFO, "The Object Layer has a draw order that uses y coordinate (top down).");
        } break;
    }

    
    for(currentObject = layer->content.objgr->head; currentObject != NULL; currentObject = currentObject->next)
    {
        objectType = currentObject->obj_type;
        visible = currentObject->visible;
        Color colorObject;

        struct ObjectTypes *temp = (struct ObjectTypes *)malloc(sizeof(struct ObjectTypes));
        for(temp = defaultOT; temp != NULL; temp = temp->next)
        {
            if(currentObject->type == NULL)
            {
                colorObject.r = 255;
                colorObject.g = 255;
                colorObject.b = 255;
                colorObject.a = 255;
                continue;
            }

            if(strcmp(currentObject->type, temp->name) == 0)
            {
                ARGBtoSingleValues(&temp->color, &colorObject);
            }
            struct Properties *tempP = (struct Properties *)malloc(sizeof(struct Properties));
            for(tempP = &temp->firstProp; tempP != NULL; tempP = tempP->next)
            {
                switch(tempP->property.type)
                {
                    case PT_NONE:
                        printf("No property type:\n");
                    break;

                    case PT_INT:
                        printf("name of property: %s, type of property: INT, default val: %i\n", tempP->property.name, tempP->property.value.integer);
                    break;

                    case PT_FLOAT:
                        printf("name of property: %s, type of property: FLOAT, default val: %f\n", tempP->property.name, tempP->property.value.decimal);
                    break;

                    case PT_BOOL:
                        printf("name of property: %s, type of property: BOOL, default val: %i\n", tempP->property.name, tempP->property.value.boolean);
                    break;

                    case PT_STRING:
                        printf("name of property: %s, type of property: STRING, default val: %s\n", tempP->property.name, tempP->property.value.string);
                    break;

                    case PT_COLOR:
                        printf("name of property: %s, type of property: COLOR, default val: %u\n", tempP->property.name, tempP->property.value.color);
                    break;

                    case PT_FILE:
                        printf("name of property: %s, type of property: FILE, default val: %s\n", tempP->property.name, tempP->property.value.file);
                    break;                    
                }
            }
        }
        free(temp);
        // printf("Object type: %i\n", objectType);
        //Only continue with this object if it is visible
        if(!visible) {continue;}

        switch(objectType)
        {
            case OT_NONE:
            {
                //in case of error
                TraceLog(LOG_ERROR, "Object drawing error.");
            } break;
            case OT_SQUARE:
            {
                //use x,y,width,height
                    rotation = currentObject->rotation;
                    DrawRectanglePro((Rectangle){currentObject->x, currentObject->y, currentObject->width, currentObject->height}, (Vector2){0,0}, rotation, (Color){colorObject.r,colorObject.g,colorObject.b,127});
                    DrawRectangleLines(currentObject->x, currentObject->y, currentObject->width, currentObject->height,colorObject);
                    DrawTextRec(textFont,currentObject->name,(Rectangle){currentObject->x+2, currentObject->y, currentObject->width, currentObject->height},textFont.baseSize,1.0f,1,BLACK);
            } break;
            case OT_POLYGON:
            {
                //Use content.shape
                Vector2 sPoint,pPoint,xPoint;
                Vector2 *pointArray = (Vector2 *)malloc(sizeof(Vector2)*currentObject->content.shape->points_len);

                //Draw Lines between the points of the shape
                for(int i = 0; (i+1)  < currentObject->content.shape->points_len; i++)
                {
                    if(i == 0)
                    {
                        sPoint.x = currentObject->content.shape->points[i][0]+currentObject->x;
                        sPoint.y = currentObject->content.shape->points[i][1]+currentObject->y;
                    }
                    pPoint.x = currentObject->content.shape->points[i][0]+currentObject->x;
                    pPoint.y = currentObject->content.shape->points[i][1]+currentObject->y;
                    xPoint.x = currentObject->content.shape->points[i+1][0]+currentObject->x;
                    xPoint.y = currentObject->content.shape->points[i+1][1]+currentObject->y;
                
                    // DrawLineEx(pPoint, xPoint, 1.0f, GREEN);
                }
               
                for(int c = 0; (c)  < currentObject->content.shape->points_len; c++)
                {
                    pointArray[c].x = currentObject->content.shape->points[c][0]+currentObject->x;
                    pointArray[c].y = currentObject->content.shape->points[c][1]+currentObject->y;
                    // pointArray++;
                }

                //Draw line from last point to first point
                /* DrawPolygon(pointArray, currentObject->content.shape->points_len, (Color){colorObject.r,colorObject.g,colorObject.b,127}); */
                DrawLineStrip(pointArray, currentObject->content.shape->points_len, colorObject);
                DrawLineEx(xPoint, sPoint, 1.0f, colorObject);
                
                //Draw the object name
                DrawTextRec(textFont,currentObject->name,(Rectangle){currentObject->x+2, currentObject->y, currentObject->x+200, currentObject->y+50},textFont.baseSize,1.0f,1,BLACK);
            } break;
            case OT_POLYLINE:
            {
                //Use content.shape
                Vector2 sPoint, ePoint;

                //Draw Lines between the points of the shape
                for(int c = 0; (c+1)  < currentObject->content.shape->points_len; c++)
                {
                    sPoint.x = currentObject->content.shape->points[c][0]+currentObject->x;
                    sPoint.y = currentObject->content.shape->points[c][1]+currentObject->y;
                    ePoint.x = currentObject->content.shape->points[c+1][0]+currentObject->x;
                    ePoint.y = currentObject->content.shape->points[c+1][1]+currentObject->y;
                    DrawLineEx(sPoint, ePoint, 1.0f, colorObject);
                }
                //Draw the object name
                DrawTextRec(textFont,currentObject->name,(Rectangle){currentObject->x+2, currentObject->y, currentObject->x+200, currentObject->y+50},textFont.baseSize,1.0f,1,BLACK);
            } break;
            case OT_ELLIPSE:
            {
                //Use x,y,width(h radius), height (v radius)
                
                // DrawEllipseSector((Vector2){currentObject->x+0.5*currentObject->width, currentObject->y+0.5*currentObject->height}, currentObject->width, currentObject->height, 0, 360, 36, (Color){colorObject.r,colorObject.g,colorObject.b,127});
                DrawEllipse(currentObject->x+0.5*currentObject->width, currentObject->y+0.5*currentObject->height, 0.5*currentObject->width, 0.5*currentObject->height, (Color){colorObject.r,colorObject.g,colorObject.b,127});
                DrawEllipseLines(currentObject->x+0.5*currentObject->width, currentObject->y+0.5*currentObject->height, 0.5*currentObject->width, 0.5*currentObject->height, colorObject);
                // DrawEllipseSectorLines((Vector2){currentObject->x+0.5*currentObject->width, currentObject->y+0.5*currentObject->height}, currentObject->width, currentObject->height, 0, 360, 36, colorObject);
                DrawTextRec(textFont,currentObject->name,(Rectangle){currentObject->x+2, currentObject->y, currentObject->width, currentObject->height},textFont.baseSize,1.0f,1,BLACK);
                // DrawCircleSector((Vector2){currentObject->x+0.5*currentObject->width, currentObject->y+0.5*currentObject->height}, currentObject->height*0.5f,0,360,360,BLUE);

            } break;
            case OT_TILE:
            {
                //use content.gid
                unsigned int flip, gid;
                tmx_tile *tile;
                unsigned int tile_width;
                unsigned int tile_height;
                Rectangle sourceRect;
                Rectangle destRect;
                Texture2D *tsTexture; // tileset texture
                Vector2 origin = {0.0, 0.0};

                destRect.x = currentObject->x;
                destRect.y = currentObject->y - currentObject->height;
                destRect.width = currentObject->width;
                destRect.height = currentObject->height;
                gid = currentObject->content.gid;
                flip = gid & ~TMX_FLIP_BITS_REMOVAL;    // get flip operations from GID
                gid = gid & TMX_FLIP_BITS_REMOVAL;      // remove flip operations from GID to get tile ID
                tile = map->tiles[gid];
                if (tile != NULL)
                {
                    // Get tile's texture out of the tileset texture
                    if (tile->image != NULL)
                    {
                        tsTexture = (Texture2D *)tile->image->resource_image;
                        tile_width = tile->image->width;
                        tile_height = tile->image->height;
                    }
                    else
                    {
                        tsTexture = (Texture2D *)tile->tileset->image->resource_image;
                        tile_width = tile->tileset->tile_width;
                        tile_height = tile->tileset->tile_height;
                    }

                    sourceRect.x = tile->ul_x;
                    sourceRect.y = tile->ul_y;
                    sourceRect.width = tile_width;
                    sourceRect.height = tile_height;

                    // Deal with flips
                    origin.x = 0.0;
                    origin.y = 0.0;
                    rotation = 0.0;
                    switch(flip)
                    {
                        case TMX_FLIPPED_DIAGONALLY:
                        {
                            sourceRect.height = -1 * sourceRect.height;
                            rotation = 90.0;
                        } break;
                        case TMX_FLIPPED_VERTICALLY:
                        {
                            sourceRect.height = -1 * sourceRect.height;
                        } break;
                        case TMX_FLIPPED_DIAGONALLY + TMX_FLIPPED_VERTICALLY:
                        {
                            rotation = -90.0;
                        } break;
                        case TMX_FLIPPED_HORIZONTALLY:
                        {
                            sourceRect.width = -1 * sourceRect.width;
                        } break;
                        case  TMX_FLIPPED_DIAGONALLY + TMX_FLIPPED_HORIZONTALLY:
                        {
                            rotation = 90.0;
                        } break;
                        case TMX_FLIPPED_HORIZONTALLY + TMX_FLIPPED_VERTICALLY:
                        {
                            rotation = 180.0;
                        } break;
                        case TMX_FLIPPED_DIAGONALLY + TMX_FLIPPED_HORIZONTALLY + TMX_FLIPPED_VERTICALLY:
                        {
                            sourceRect.width = -1 * sourceRect.width;
                            rotation = 90.0;
                        } break;
                        default:
                        {
                            origin.x = 0.0;
                            origin.y = 0.0;
                            rotation = 0.0;
                        } break;
                    }

                    // Adjust origin to rotate around the center of the tile, 
                    // which means destination recangle's origin must be adjusted.
                    if (rotation != 0.0)
                    {
                        origin.x = tile_width / 2;
                        origin.y = tile_height / 2;
                        destRect.x += tile_width / 2;
                        destRect.y += tile_height / 2;
                    }

                    // TODO: Take layer opacity into account
                    DrawTexturePro(*tsTexture, sourceRect, destRect, origin, rotation, colorObject);
                    DrawTextRec(textFont,currentObject->name,(Rectangle){currentObject->x+2, currentObject->y, currentObject->width, currentObject->height},textFont.baseSize,1.0f,1,BLACK);
                }

            } break;
            case OT_TEXT:
            {
                //Use content.text
                DrawTextRec(textFont, currentObject->content.text->text, (Rectangle){currentObject->x, currentObject->y, currentObject->width, currentObject->height}, currentObject->content.text->pixelsize, 1.0f, currentObject->content.text->wrap, WHITE);
                //TODO: get color of tmx and set to color var
            } break;
            case OT_POINT:
            {
                //Use x,y
                DrawRectangle(currentObject->x,currentObject->y,  5, 5, colorObject);
                DrawTextRec(textFont,currentObject->name,(Rectangle){currentObject->x+2, currentObject->y, currentObject->width, currentObject->height},textFont.baseSize,1.0f,1,BLACK);
            }
        }
    }
}

void DrawTmxImageLayer(tmx_map *map, tmx_layer *layer)
{
    Texture2D *imageTexture;

    if(layer->content.image != NULL)
    {
        imageTexture = (Texture2D *)layer->content.image->resource_image;
        DrawTexture(*imageTexture, 0+layer->offsetx, 0+layer->offsety, WHITE);
    }
    else
    {
        TraceLog(LOG_WARNING, "The image layer: \"%s\" doesn't have a image.", layer->name);
    }
}

void DrawTmxGroupLayer(tmx_map *map, tmx_layer *layer, struct ObjectTypes *defaultOT)
{
    tmx_layer *currentLayer;
    for(currentLayer = layer->content.group_head; currentLayer != NULL; currentLayer = currentLayer->next)
    {
        if (currentLayer->visible)
        {
            switch(currentLayer->type)
            {
                case L_LAYER:
                {
                    TraceLog(LOG_INFO, "Render TMX layer \"%s\"", currentLayer->name);
                    DrawTmxLayer(map, currentLayer);
                } break;

                case L_GROUP:   
                {
                    TraceLog(LOG_INFO, "Render Group TMX layer: %s", currentLayer->name);
                    DrawTmxGroupLayer(map, currentLayer, defaultOT);
                } break;

                case L_OBJGR:
                {
                    if(debugMode)
                    {
                        TraceLog(LOG_INFO, "Render Object TMX layer: %s", currentLayer->name);
                        DrawTmxObjectLayerDebug(map, currentLayer, defaultOT);
                    }
                } break;

                case L_IMAGE:
                {
                    TraceLog(LOG_INFO, "Render Image TMX layer: %s", currentLayer->name);
                    DrawTmxImageLayer(map, currentLayer);
                } break;
                case L_NONE:
                {
                    TraceLog(LOG_INFO, "Layer draw error: %s", currentLayer->name);
                }
                default:
                    break;
            }
        }
    }
}

void RenderTmxMapToFramebuf(const char *mapFileName, RenderTexture2D *buf)
{
    tmx_layer *layer = NULL;

    // Setting these two function pointers allows TMX lib to load the tileset graphics and
    // set each tile's resource_image property properly.
    tmx_img_load_func = (void *(*)(const char *))LoadMapTexture;
    tmx_img_free_func = (void (*)(void *))UnloadMapTexture;
    tmx_map *mapTmx = tmx_load(mapFileName);
    if (mapTmx == NULL) {
        tmx_perror("tmx_load");
        return;
    }

    Color BgColor;
    unsigned int backgrColor = mapTmx->backgroundcolor;
    struct ObjectTypes testO;
    GetDefaultObjectTypes(&testO, "../Data/Config/objecttypes.xml"); 
    //Import the default object properties here

    // Create a frame buffer
    // TODO: I don't life loading the RenderTexture here and unloading it in main(), but map info is needed to 
    // allocate the buffer of the right size, so either load it here, or separate tmx_load part of the code into
    // a separate function to allow the application code to call LoadRenderTexture between tmx_load and actual
    // drawing of the map.

    ARGBtoSingleValues(&backgrColor, &BgColor);
    *buf = LoadRenderTexture(mapTmx->width * mapTmx->tile_width, mapTmx->height * mapTmx->tile_height);

    BeginTextureMode(*buf); // start rendering into the buffer
        ClearBackground(BgColor);
        // Iterate through TMX layers rendering them into buf
        layer = mapTmx->ly_head;
        while(layer)
        {
            if (layer->visible)
            {
                switch(layer->type)
                {
                    case L_LAYER:
                    {
                        // TraceLog(LOG_INFO, "Render TMX layer \"%s\"", layer->name);
                        DrawTmxLayer(mapTmx, layer);
                    } break;

                    case L_GROUP:   
                    {
                        // TraceLog(LOG_INFO, "Render Group TMX layer: %s", layer->name);
                        DrawTmxGroupLayer(mapTmx, layer, &testO);
                    } break;

                    case L_OBJGR:
                    {
                        // TraceLog(LOG_INFO, "Render Object TMX layer: %s", layer->name);
                        if(debugMode)
                        {
                            DrawTmxObjectLayerDebug(mapTmx, layer, &testO);
                        }
                    } break;

                    case L_IMAGE:
                    {
                        // TraceLog(LOG_INFO, "Render Image TMX layer: %s", layer->name);
                        DrawTmxImageLayer(mapTmx, layer);
                    } break;
                    case L_NONE:
                    {
                        TraceLog(LOG_INFO, "Layer draw error: %s", layer->name);
                    }
                    default:
                        break;
                }
            }
            layer = layer->next;
        }
    EndTextureMode();   // stop rendering into the buffer

    tmx_map_free(mapTmx);
}

// Draw a piece of a Ellipse
/* void DrawEllipseSector(Vector2 center, float radiusH,float radiusV, int startAngle, int endAngle, int segments, Color color) */
/* { */
/*     if (radiusH <= 0.0f) radiusH = 0.1f;  // Avoid div by zero */

/*     // Function expects (endAngle > startAngle) */
/*     if (endAngle < startAngle) */
/*     { */
/*         // Swap values */
/*         int tmp = startAngle; */
/*         startAngle = endAngle; */
/*         endAngle = tmp; */
/*     } */

/*     if (segments < 4) */
/*     { */
/*         // Calculate how many segments we need to draw a smooth circle, taken from https://stackoverflow.com/a/2244088 */
/*         #define CIRCLE_ERROR_RATE  0.5f */

/*         // Calculate the maximum angle between segments based on the error rate. */
/*         float th = acosf(2*powf(1 - CIRCLE_ERROR_RATE/radiusH, 2) - 1); */
/*         segments = (endAngle - startAngle)*ceilf(2*PI/th)/360; */

/*         if (segments <= 0) segments = 4; */
/*     } */

/*     radiusH = radiusH *0.5f; */
/*     radiusV = radiusV *0.5f; */

/*     float stepLength = (float)(endAngle - startAngle)/(float)segments; */
/*     float angle = startAngle; */

/* #if defined(SUPPORT_QUADS_DRAW_MODE) */
/*     if (rlCheckBufferLimit(4*segments/2)) rlglDraw(); */

/*     rlEnableTexture(GetShapesTexture().id); */

/*     rlBegin(RL_QUADS); */
/*         // NOTE: Every QUAD actually represents two segments */
/*         for (int i = 0; i < segments/2; i++) */
/*         { */
/*             rlColor4ub(color.r, color.g, color.b, color.a); */

/*             rlTexCoord2f(recTexShapes.x/texShapes.width, recTexShapes.y/texShapes.height); */
/*             rlVertex2f(center.x, center.y); */

/*             rlTexCoord2f(recTexShapes.x/texShapes.width, (recTexShapes.y + recTexShapes.height)/texShapes.height); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*angle)*radiusH, center.y + cosf(DEG2RAD*angle)*radiusV); */

/*             rlTexCoord2f((recTexShapes.x + recTexShapes.width)/texShapes.width, (recTexShapes.y + recTexShapes.height)/texShapes.height); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*(angle + stepLength))*radiusH, center.y + cosf(DEG2RAD*(angle + stepLength))*radiusV); */

/*             rlTexCoord2f((recTexShapes.x + recTexShapes.width)/texShapes.width, recTexShapes.y/texShapes.height); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*(angle + stepLength*2))*radiusH, center.y + cosf(DEG2RAD*(angle + stepLength*2))*radiusV); */

/*             angle += (stepLength*2); */
/*         } */

/*         // NOTE: In case number of segments is odd, we add one last piece to the cake */
/*         if (segments%2) */
/*         { */
/*             rlColor4ub(color.r, color.g, color.b, color.a); */

/*             rlTexCoord2f(recTexShapes.x/texShapes.width, recTexShapes.y/texShapes.height); */
/*             rlVertex2f(center.x, center.y); */

/*             rlTexCoord2f(recTexShapes.x/texShapes.width, (recTexShapes.y + recTexShapes.height)/texShapes.height); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*angle)*radiusH, center.y + cosf(DEG2RAD*angle)*radiusV); */

/*             rlTexCoord2f((recTexShapes.x + recTexShapes.width)/texShapes.width, (recTexShapes.y + recTexShapes.height)/texShapes.height); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*(angle + stepLength))*radiusH, center.y + cosf(DEG2RAD*(angle + stepLength))*radiusV); */

/*             rlTexCoord2f((recTexShapes.x + recTexShapes.width)/texShapes.width, recTexShapes.y/texShapes.height); */
/*             rlVertex2f(center.x, center.y); */
/*         } */
/*     rlEnd(); */

/*     rlDisableTexture(); */
/* #else */
/*     if (rlCheckBufferLimit(3*segments)) rlglDraw(); */

/*     rlBegin(RL_TRIANGLES); */
/*         for (int i = 0; i < segments; i++) */
/*         { */
/*             rlColor4ub(color.r, color.g, color.b, color.a); */

/*             rlVertex2f(center.x, center.y); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*angle)*radiusH, center.y + cosf(DEG2RAD*angle)*radiusV); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*(angle + stepLength))*radiusH, center.y + cosf(DEG2RAD*(angle + stepLength))*radiusV); */

/*             angle += stepLength; */
/*         } */
/*     rlEnd(); */
/* #endif */
/* } */

/* void DrawEllipseSectorLines(Vector2 center, float radiusH, float radiusV, int startAngle, int endAngle, int segments, Color color) */
/* { */
/*     if (radiusH <= 0.0f) radiusH = 0.1f;  // Avoid div by zero issue */

/*     // Function expects (endAngle > startAngle) */
/*     if (endAngle < startAngle) */
/*     { */
/*         // Swap values */
/*         int tmp = startAngle; */
/*         startAngle = endAngle; */
/*         endAngle = tmp; */
/*     } */

/*     if (segments < 4) */
/*     { */
/*         // Calculate how many segments we need to draw a smooth circle, taken from https://stackoverflow.com/a/2244088 */
/*         #ifndef CIRCLE_ERROR_RATE */
/*         #define CIRCLE_ERROR_RATE  0.5f */
/*         #endif */

/*         // Calculate the maximum angle between segments based on the error rate. */
/*         float th = acosf(2*powf(1 - CIRCLE_ERROR_RATE/radiusH, 2) - 1); */
/*         segments = (endAngle - startAngle)*ceilf(2*PI/th)/360; */

/*         if (segments <= 0) segments = 4; */
/*     } */
/*     radiusH = radiusH *0.5f; */
/*     radiusV = radiusV *0.5f; */

/*     float stepLength = (float)(endAngle - startAngle)/(float)segments; */
/*     float angle = startAngle; */

/*     // Hide the cap lines when the circle is full */
/*     bool showCapLines = true; */
/*     int limit = 2*(segments + 2); */
/*     if ((endAngle - startAngle)%360 == 0) { limit = 2*segments; showCapLines = false; } */

/*     if (rlCheckBufferLimit(limit)) rlglDraw(); */

/*     rlBegin(RL_LINES); */
/*         if (showCapLines) */
/*         { */
/*             rlColor4ub(color.r, color.g, color.b, color.a); */
/*             rlVertex2f(center.x, center.y); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*angle)*radiusH, center.y + cosf(DEG2RAD*angle)*radiusV); */
/*         } */

/*         for (int i = 0; i < segments; i++) */
/*         { */
/*             rlColor4ub(color.r, color.g, color.b, color.a); */

/*             rlVertex2f(center.x + sinf(DEG2RAD*angle)*radiusH, center.y + cosf(DEG2RAD*angle)*radiusV); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*(angle + stepLength))*radiusH, center.y + cosf(DEG2RAD*(angle + stepLength))*radiusV); */

/*             angle += stepLength; */
/*         } */

/*         if (showCapLines) */
/*         { */
/*             rlColor4ub(color.r, color.g, color.b, color.a); */
/*             rlVertex2f(center.x, center.y); */
/*             rlVertex2f(center.x + sinf(DEG2RAD*angle)*radiusH, center.y + cosf(DEG2RAD*angle)*radiusV); */
/*         } */
/*     rlEnd(); */
/* } */

/* void DrawPolygon(Vector2 *points, int pointsCount, Color color) */
/* { */
/*     if (rlCheckBufferLimit((pointsCount))) rlglDraw(); */
/*         rlBegin(RL_TRIANGLES); */

/*             rlColor4ub(color.r, color.g, color.b, color.a); */

/*             for (int i = 2; i < pointsCount; i+=2) */
/*             { */
/*                 rlVertex2f(points[i].x, points[i].y); */
/*                 rlVertex2f(points[i-1].x, points[i-1].y); */
/*                 rlVertex2f(points[i-2].x, points[i-2].y); */
/*                 rlColor4ub(200, 20, 255, color.a);   */
/*             } */
/*             rlVertex2f(points[0].x, points[0].y); */
/*             rlVertex2f(points[pointsCount-1].x, points[pointsCount-1].y); */
/*             rlVertex2f(points[pointsCount-2].x, points[pointsCount-2].y); */
/*         rlEnd(); */
/* } */
int get_color_rgb_f(const char *c) {
  if (*c == '#') c++;
  return (int)strtol(c, NULL, 16);
}


void ARGBtoSingleValues(unsigned int *argb, Color *single)
{
    unsigned char a = (*argb >> 24) & 0xFF;
    unsigned char r = (*argb >> 16) & 0xFF;
    unsigned char g = (*argb >> 8) & 0xFF;
    unsigned char b = (*argb) & 0xFF;
    single->r = r;
    single->g = g;
    single->b = b;
    if(a == 0)
    {
        single->a = 255;
    }else
    {
        single->a = a;
    }
    // TraceLog(LOG_INFO, "background color: a: %u, r: %u, g: %u, b: %u\n", a, r, g, b);
}

void GetDefaultObjectTypes(struct ObjectTypes *defaultObjects, char *fileName)
{
    if(defaultObjects == NULL)
    {
        TraceLog(LOG_ERROR, "The pointer to the default object types file is NULL.");
    }
    struct ObjectTypes head;
    struct ObjectTypes *tail = NULL;
    struct Properties headProp;
    struct Properties *tailProp = NULL;
    xmlDoc *db = NULL;
    xmlNode *rootElement = NULL;
    int itemCount = 0;
    int propertyCount = 0;

    db = xmlReadFile(fileName, NULL, 0);
    rootElement = xmlDocGetRootElement(db);
    xmlNodePtr node = rootElement->children;
    xmlNodePtr nodeProp = node->next->children;
    if(node == NULL) {  TraceLog(LOG_ERROR, "The Object types xml document is empty."); }   
    itemCount = xmlChildElementCount(rootElement);
    for(int i =0; i < itemCount; i++)
    {
        struct ObjectTypes *temp = (struct ObjectTypes *)malloc(sizeof(struct ObjectTypes));
        strcpy(temp->name,(const char *)xmlGetProp(node->next, (xmlChar *)"name"));
        // TraceLog(LOG_INFO, "name object: %s", temp->name);
        temp->color = get_color_rgb_f((const char *)xmlGetProp(node->next, (xmlChar *)"color"));
        // temp stuff
        propertyCount = xmlChildElementCount(node->next);
        nodeProp = node->next->children;
        for(int p = 0; p < propertyCount; p++)
        {
            struct Properties *tempProp =  (struct Properties *)malloc(sizeof(struct Properties));
            // printf("name of property: %s, type: %s, defualt value: %s\n", (const char *)xmlGetProp(nodeProp->next, (xmlChar *)"name"),(const char *)xmlGetProp(nodeProp->next, (xmlChar *)"type"),(const char *)xmlGetProp(nodeProp->next, (xmlChar *)"default"));
            tempProp->property.name = (char *)xmlGetProp(nodeProp->next, (xmlChar *)"name");
            if(strcmp((const char *)xmlGetProp(nodeProp->next, (xmlChar *)"type"), "int") == 0)
            {
                tempProp->property.type = PT_INT;    
                tempProp->property.value.integer = atoi((char*)xmlGetProp(nodeProp->next, (xmlChar *)"default"));
            }
            else if(strcmp((const char *)xmlGetProp(nodeProp->next, (xmlChar *)"type"), "bool") == 0)
            {
                tempProp->property.type = PT_BOOL;
                if(strcmp((char *)xmlGetProp(nodeProp->next, (xmlChar *)"default"), "false") == 0)
                {
                    tempProp->property.value.boolean = 0;
                } 
                else
                {
                    tempProp->property.value.boolean = 1;
                }
            }
            else if(strcmp((const char *)xmlGetProp(nodeProp->next, (xmlChar *)"type"), "file") == 0)
            {
                tempProp->property.type = PT_FILE;
                tempProp->property.value.file = (char *)xmlGetProp(nodeProp->next, (xmlChar *)"default");
            } else if(strcmp((const char *)xmlGetProp(nodeProp->next, (xmlChar *)"type"), "color") == 0)
            {
                tempProp->property.type = PT_COLOR;
                tempProp->property.value.color = get_color_rgb_f((const char *)xmlGetProp(nodeProp->next, (xmlChar *)"default"));
            } else if(strcmp((const char *)xmlGetProp(nodeProp->next, (xmlChar *)"type"), "string") == 0)
            {
                tempProp->property.type = PT_STRING;
                tempProp->property.value.string = (char *)xmlGetProp(nodeProp->next, (xmlChar *)"default");
            } else if(strcmp((const char *)xmlGetProp(nodeProp->next, (xmlChar *)"type"), "float") == 0)
            {
                tempProp->property.type = PT_FLOAT;
                tempProp->property.value.decimal = (float)atof((char*)xmlGetProp(nodeProp->next, (xmlChar *)"default"));
            }
            else 
            {
                //error no or unknown type
                tempProp->property.type = PT_NONE;
            }
            
            if(tailProp == NULL)
            {
                headProp = *tempProp;
                tailProp = tempProp;
                headProp.next = tempProp;
                nodeProp = nodeProp->next->next;
            }
            else
            {
               tailProp->next = tempProp;
               tailProp = tailProp->next;
               nodeProp = nodeProp->next->next;
            } 
        }
        temp->firstProp = *headProp.next;
        temp->next = NULL;

    if(tail == NULL)
    {
      head = *temp;
      tail = temp;
      head.next = temp;
      // TraceLog(LOG_INFO, "asd asdasd /");
      node = node->next->next;
    }
    else
    {
       tail->next = temp;
       tail = tail->next;
       node = node->next->next;
    } 
   // free(temp);
  }
  *defaultObjects = *head.next;
  free(tail);
    // free(head);
    xmlFreeDoc(db);
}

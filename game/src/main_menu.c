#include "central_systems.h"

GuiItem menuItems[4] = {"New Game", "Continue", "Quit", "Combat"};
GuiItem continueItems[5] = {"A","B","C","D", "E"};
int SelectedMenuItem = 0;
int continueWindow = 0;
int *currentFocus = &SelectedMenuItem;
int *lastFocus;
int currentItemLength = LENGTH(menuItems);
int lastItemLength;
float fontSize = 34.0f;
Font textFont2;

void MainMenuInit()
{
	textFont = LoadFont("../Data/Fonts/Golden-Sun.ttf");
	/* textFont = LoadFont("./Data/Fonts/Redaction-mL8P5.otf"); */
	textFont2 = LoadFont("../Data/Fonts/final fantasy vi snesb.ttf");
	GenTextureMipmaps(&textFont.texture);
	GenTextureMipmaps(&textFont2.texture);
	SetTextureFilter(textFont.texture, RL_TEXTURE_MIN_FILTER);
	SetTextureFilter(textFont.texture, RL_TEXTURE_MAG_FILTER);
	SetTextureFilter(textFont.texture, TEXTURE_FILTER_POINT);
	SetTextureFilter(textFont2.texture, TEXTURE_FILTER_POINT);
}

void MainMenuUpdate(float *dTime)
{
}

void MainMenuDraw(float *dTime)
{
	ClearBackground(BLACK);
	DrawFPS(5, 5);
	DrawTextEx(textFont,"2D (J)RPG", (Vector2){screenWidth/2-80, 200}, fontSize,
		   0.0f, WHITE);
	DrawTextEx(textFont2,"2D (J)RPG", (Vector2){screenWidth/2-200, 200}, fontSize,
		   0.0f, WHITE);
	/* gui_dialog_box((Rectangle){screenWidth/2 - 0.15*screenWidth, */
				 /* 0.5*screenHeight, 0.3*screenWidth, */
				 /* 0.3*screenHeight}, 15.0f, textFont, GREEN); */
	/* return; */
	if(GuiButton((Rectangle){screenWidth/2- 0.12*screenWidth,0.55*screenHeight,
				 0.23*screenWidth, 0.05*screenHeight}, menuItems[0].name,IsFocused(currentFocus, &SelectedMenuItem, 0), textFont))
	{
		SwitchState(&current_game_state, current_game_states.MAP);
	}
	if(GuiButton((Rectangle){screenWidth/2- 0.12*screenWidth,0.62*screenHeight,
				 0.23*screenWidth, 0.05*screenHeight}, menuItems[1].name, IsFocused(currentFocus, &SelectedMenuItem, 1), textFont))
	{
		lastFocus = currentFocus;
		currentFocus = &continueWindow;
		lastItemLength = currentItemLength;
		currentItemLength = LENGTH(continueItems);

	}
	if(GuiButton((Rectangle){screenWidth/2- 0.12*screenWidth,0.69*screenHeight,
				 0.23*screenWidth, 0.05*screenHeight}, menuItems[2].name, IsFocused(currentFocus, &SelectedMenuItem, 2), textFont))
	{
		displayWindow = !displayWindow;
	}
	if(GuiButton((Rectangle){screenWidth/2- 0.12*screenWidth,0.8*screenHeight,
				 0.23*screenWidth, 0.05*screenHeight}, menuItems[3].name,IsFocused(currentFocus, &SelectedMenuItem, 3), textFont))
	{
		SwitchState(&current_game_state, current_game_states.COMBAT);
	}
	DrawRectangleLines(screenWidth/2 - 0.15*screenWidth, 0.5*screenHeight, 0.3*screenWidth, 0.3*screenHeight, WHITE);

	if(currentFocus == &continueWindow)
	{
		DrawRectangleLines(screenWidth/2 + 0.16*screenWidth, 0.5*screenHeight, 0.3*screenWidth, 0.3*screenHeight, WHITE);
		
		GuiButton((Rectangle){screenWidth/2 + 0.17*screenWidth, 0.51*screenHeight, 0.1*screenWidth, 0.05*screenHeight}, continueItems[0].name, IsFocused(currentFocus, &continueWindow, 0), textFont);
		GuiButton((Rectangle){screenWidth/2 + 0.17*screenWidth, 0.57*screenHeight, 0.1*screenWidth, 0.05*screenHeight}, continueItems[1].name, IsFocused(currentFocus, &continueWindow, 1), textFont);
		GuiButton((Rectangle){screenWidth/2 + 0.17*screenWidth, 0.63*screenHeight, 0.1*screenWidth, 0.05*screenHeight}, continueItems[2].name, IsFocused(currentFocus, &continueWindow, 2), textFont);
		GuiButton((Rectangle){screenWidth/2 + 0.17*screenWidth, 0.69*screenHeight, 0.1*screenWidth, 0.05*screenHeight}, continueItems[3].name, IsFocused(currentFocus, &continueWindow, 3), textFont);
		GuiButton((Rectangle){screenWidth/2 + 0.17*screenWidth, 0.75*screenHeight, 0.1*screenWidth, 0.05*screenHeight}, continueItems[4].name, IsFocused(currentFocus, &continueWindow, 4), textFont);
	}
	//DrawTextEx(textFont,"(N)ew Game",(Vector2){screenWidth/2 - 0.07*screenWidth,0.55*screenHeight},34,0,WHITE);
	// DrawTextEx(textFont,"(C)ontinue",(Vector2){screenWidth/2 - 0.07*screenWidth,0.63*screenHeight},34,0,WHITE);
	// DrawTextEx(textFont,"(Q)uit",(Vector2){screenWidth/2 - 0.07*screenWidth,0.7*screenHeight},34,0,WHITE);
	// DrawTextEx(textFont,"C(o)mbat",(Vector2){screenWidth/2 - 0.07*screenWidth,0.8*screenHeight},34,0,WHITE);
}

void MainMenuInput(float *dTime)
{
	if(IsKeyPressed(KEY_EQUAL))
	{
		fontSize++;
		screenHeight += 10;
	}
	if(IsKeyPressed(KEY_MINUS))
	{
		fontSize--;
		screenHeight -= 10;
	}
	if(IsKeyPressed(KEY_O))
	{
		SwitchState(&current_game_state, current_game_states.COMBAT);
	}
	if(IsKeyPressed(KEY_N))
	{
		SwitchState(&current_game_state, current_game_states.MAP);
	}
	if(IsKeyPressed(KEY_Q))
	{
		displayWindow = !displayWindow;
	}
	if(IsKeyPressed(KEY_UP))
	{
		printf("selectedItem: %i\n", SelectedMenuItem);
		*currentFocus = selectItem(currentItemLength, *currentFocus, "up");
	}
	if(IsKeyPressed(KEY_DOWN))
	{
		printf("selectedItem: %i\n", SelectedMenuItem);
		*currentFocus = selectItem(currentItemLength, *currentFocus, "down");
	}
	if(IsKeyPressed(KEY_Z))
	{
		if(currentFocus != &SelectedMenuItem)
		{
			int *temp = currentFocus;
			int tempLength = currentItemLength;

			currentFocus = lastFocus;
			lastFocus = temp;
			currentItemLength = lastItemLength;
			lastItemLength = currentItemLength;
		}
	}
}

void MainMenuDeInit()
{
	UnloadFont(textFont);
	UnloadFont(textFont2);
}

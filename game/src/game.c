//main game loop file
//include the State machine of the game (combat, pause, etc.)
//include the game loop
#include "central_systems.h"

void InitializeWindow();

int main() {
  /* Initialize the starting vars and window context */
  bool showExitMessageBox = 0;
  int frameCount = 0;
  float deltaTime = 0.0f;
	
	InitializeWindow();
	displayWindow = 0;
	// ToggleFullscreen();
	g_sprite_sheet = LoadTexture("../Data/Art/Character_sheet.png");
	charSpriteSheet = ht_new();
	Rectangle test = (Rectangle){0.0f,0.0f,48.0f,48.0f};
	Rectangle test2 = (Rectangle){48.0f,0.0f,48.0f,48.0f};
	ht_insert(charSpriteSheet, "iconRua", test);
	ht_insert(charSpriteSheet, "iconPing", test2);
	ht_insert(charSpriteSheet, "downRua", (Rectangle){224.0f,0.0f,32.0f,32.0f});
	ht_insert(charSpriteSheet, "down0Rua", (Rectangle){192.0f, 0.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "down1Rua", (Rectangle){256.0f, 0.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "leftRua", (Rectangle){224.0f,32.0f,32.0f,32.0f});
	ht_insert(charSpriteSheet, "left0Rua", (Rectangle){192.0f, 32.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "left1Rua", (Rectangle){256.0f, 32.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "rightRua", (Rectangle){224.0f,64.0f,32.0f,32.0f});
	ht_insert(charSpriteSheet, "right0Rua", (Rectangle){192.0f, 64.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "right1Rua", (Rectangle){256.0f, 64.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "up0Rua", (Rectangle){192.0f, 96.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "upRua", (Rectangle){224.0f, 96.0f, 32.0f, 32.0f});
	ht_insert(charSpriteSheet, "up1Rua", (Rectangle){256.0f, 96.0f, 32.0f, 32.0f});	
	ht_insert(charSpriteSheet, "leftPing", (Rectangle){320.0f,32.0f,32.0f,32.0f});
	ht_insert(charSpriteSheet, "iconGoblin", (Rectangle){0.0f,48.0f,48.0f,48.0f});
	ht_insert(charSpriteSheet, "rightGoblin", (Rectangle){128.0f,64.0f,32.0f,32.0f});
	ht_insert(charSpriteSheet, "defaultNPC", (Rectangle){0.0f, 96.0f, 16.0f, 16.0f});
	ht_insert(charSpriteSheet, "talkIcon", (Rectangle){16.0f, 96.0f, 16.0f, 16.0f});
	ht_insert(charSpriteSheet, "dialogueBox", (Rectangle){32.0f, 96.0f, 48.0f, 48.0f});

	init_game_state_stack(&current_game_state);
	fputs("DEBUG: before setting first phase!\n",stderr);
	current_game_state.states[0] = current_game_states.MAP;
	current_game_state.states[0].init_phase();
	fputs("DEBUG: got after setting first phase!\n",stderr);
	/* add_state_to_stack(&current_game_state, current_game_states.COMBAT); */
	add_state_to_stack(&current_game_state, current_game_states.DIALOGUE);
	fputs("DEBUG: got after switching to dialogue!\n",stderr);
	/* Main game loop */
	while(!displayWindow) {
		displayWindow = WindowShouldClose();
		deltaTime = GetFrameTime();
		
		if (IsKeyPressed(KEY_ESCAPE)) {
			showExitMessageBox = !showExitMessageBox;
			displayWindow = 0;
		}
		if(IsKeyPressed(KEY_GRAVE)) {
			debugMode = !debugMode;
			/* if(current_game_state.states[0] == current_game_states.MAP) { */
			/* 	SwitchState(&current_game_state, current_game_states.MAP); */
			/* } */
		}		
		if(IsKeyDown(KEY_LEFT_ALT) || IsKeyDown(KEY_RIGHT_ALT)) {
			if(IsKeyReleased(KEY_ENTER)) {
				ToggleFullscreen();
			}
		}
		fputs("DEBUG: before input!\n", stderr);
		input_phase(&deltaTime, &current_game_state); 
		fputs("DEBUG: before draw!\n", stderr);
		update_phase(&deltaTime, &current_game_state);
		
		BeginDrawing();
		ClearBackground(WHITE);
		draw_phase(&deltaTime, &current_game_state);
		EndDrawing();
		
		frameCount++;
	}
	
	/* De-Initialize */
	deinit_phases(&current_game_state);
	ht_del_hash_table(charSpriteSheet);
	UnloadTexture(g_sprite_sheet);
	CloseWindow();

	return 0;
}

void InitializeWindow()
{
	#ifdef DEBUG
		SetTraceLogLevel(LOG_ALL);
		screenWidth = 1280;
		screenHeight = 720;
		/* screen gba 240x160 */
		/* screenWidth = 240; */
		/* screenHeight = 160; */
		/* SetConfigFlags(FLAG_WINDOW_RESIZABLE); */
		InitWindow(screenWidth, screenHeight, "2D TBRPG DEBUG");
		/* SetWindowPosition(0,GetMonitorHeight(0)/2); */
	#else
		SetTraceLogLevel(LOG_NONE);
		InitWindow(800, 600, "2D TBRPG");
		Vector2 l_window_pos = GetMonitorPosition(0);
		SetWindowPosition(l_window_pos.x, l_window_pos.y);
		screenWidth = GetMonitorWidth(0);
		screenHeight = GetMonitorHeight(0);
		SetWindowState(FLAG_FULLSCREEN_MODE);
		SetWindowSize(screenWidth, screenHeight);
	#endif

	SetTargetFPS(80);
}

#include <stdio.h>
#include "central_systems.h"
#include "minunit.h"
#include "tmx_utils.h"

int tests_run = 0;

static char* test_npc_new(){
	tmx_object* tmx_obj = (tmx_object*)malloc(sizeof(tmx_object));
	tmx_obj->id = 1;
	tmx_obj->obj_type = OT_NONE;
	tmx_obj->x = 10.0;
	tmx_obj->y = 10.0;
	tmx_obj->visible = 1;
	tmx_obj->name = "test1";
	tmx_obj->type = "NPC";
	tmx_property* prop_name = (tmx_property*)malloc(sizeof(tmx_property));
	prop_name->name = "Name";
	prop_name->type = PT_STRING;
	prop_name->value.string = "grunt";
	tmx_property* prop_id = (tmx_property*)malloc(sizeof(tmx_property));
	prop_id->name = "id";
	prop_id->type = PT_INT;
	prop_id->value.integer = 22;
	tmx_obj->properties = (tmx_properties*)mk_hashtable(5);
	hashtable_set((void*)tmx_obj->properties, "name", (void*)prop_name, NULL);
	hashtable_set((void*)tmx_obj->properties, "id", (void*)prop_id, NULL);
	GameObject res_npc = game_object_new(tmx_obj, NPC);
	mu_assert("test_npc_new: gameobject name is not correctly set",
		  !strcmp(tmx_obj->name, res_npc.name));
	mu_assert("test_npc_new: npc name is not correctly set",
		  !strcmp("grunt", res_npc.value.npc.name));
	mu_assert("test_npc_new: npc id is not correctly set",
		   res_npc.value.npc.id == 22);
	free(tmx_obj);
	free(prop_name);
	free(prop_id);
	return 0;
}

static char* test_npc_polyline(){
	tmx_object* tmx_obj = (tmx_object*)malloc(sizeof(tmx_object));
	tmx_obj->id = 1;
	tmx_obj->obj_type = OT_POLYLINE;
	tmx_obj->x = 10.0f;
	tmx_obj->y = 10.0f;
	tmx_obj->visible = 1;
	tmx_obj->name = "test1";
	tmx_obj->type = "NPC";
	tmx_obj->content.shape = (tmx_shape*)malloc(sizeof(tmx_shape));
	tmx_obj->content.shape->points = (double**)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[0] =  (double*)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[1] =  (double*)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[0][0] = 1.0;
	tmx_obj->content.shape->points[0][1] = 1.0;
	tmx_obj->content.shape->points[1][0] = 5.0;
	tmx_obj->content.shape->points[1][1] = 6.0;
	tmx_obj->content.shape->points_len = 2;
	GameObject res_npc = game_object_npc_new(tmx_obj);
	mu_assert("test_npc_polyline: points length isnt the same!",
		  res_npc.value.npc.route_length == tmx_obj->content.shape->points_len);
	mu_assert("test_npc_polyline: point 1 (1,1) isnt the same!",
		  res_npc.value.npc.walk_route->point.x == tmx_obj->content.shape->points[0][0] + tmx_obj->x);
	mu_assert("test_npc_polyline: point 1 (1,1) isnt the same!",
		  res_npc.value.npc.walk_route->point.y == tmx_obj->content.shape->points[0][1] +  tmx_obj->y);
	mu_assert("test_npc_polyline: point 2 (5,6) isnt the same!",
		  res_npc.value.npc.walk_route->next->point.x == tmx_obj->content.shape->points[1][0] + tmx_obj->x);
	mu_assert("test_npc_polyline: point 2 (5,6) isnt the same!",
		  res_npc.value.npc.walk_route->next->point.y == tmx_obj->content.shape->points[1][1] + tmx_obj->y);
	free(res_npc.value.npc.walk_route);
       	free(tmx_obj->content.shape->points[1]);
	free(tmx_obj->content.shape->points[0]);
	free(tmx_obj->content.shape->points);
	free(tmx_obj->content.shape);
	free(tmx_obj);
	return 0;
}

static char* test_npc_def_update_polyline_one_point(){
	tmx_object* tmx_obj = (tmx_object*)malloc(sizeof(tmx_object));
	tmx_obj->id = 1;
	tmx_obj->obj_type = OT_POLYLINE;
	tmx_obj->x = 0.0f;
	tmx_obj->y = 0.0f;
	tmx_obj->visible = 1;
	tmx_obj->name = "test1";
	tmx_obj->type = "NPC";
	tmx_obj->content.shape = (tmx_shape*)malloc(sizeof(tmx_shape));
	tmx_obj->content.shape->points = (double**)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[0] =  (double*)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[1] =  (double*)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[0][0] = 0.0f;
	tmx_obj->content.shape->points[0][1] = 0.0f;
	tmx_obj->content.shape->points[1][0] = 10.0f;
	tmx_obj->content.shape->points[1][1] = 1.0f;
	tmx_obj->content.shape->points_len = 2;
	GameObject res_npc = game_object_new(tmx_obj, NPC);
	res_npc.value.npc.speed = 2;
	float d_time = 2.50f;
	printf("INFO: position: %f, %f\n", res_npc.position.x, res_npc.position.y);
	npc_default_update(&d_time, &res_npc);
	printf("INFO: position: %f, %f\n", res_npc.position.x, res_npc.position.y);
	mu_assert("test_npc_def_update_polyline_onepoint: position after 2.50 isnt correct",
		  res_npc.position.x == 5.0f && res_npc.position.y == 0.50f);
	npc_default_update(&d_time, &res_npc);
	printf("INFO: position: %f, %f\n", res_npc.position.x, res_npc.position.y);
	mu_assert("test_npc_def_update_polyline_onepoint: position after 5.0 isnt correct",
		  res_npc.position.x == 10.0f && res_npc.position.y == 1.0f);
	free(res_npc.value.npc.walk_route);
	free(tmx_obj->content.shape->points[1]);
	free(tmx_obj->content.shape->points[0]);
	free(tmx_obj->content.shape->points);
	free(tmx_obj->content.shape);
	free(tmx_obj);
	return 0;
}

static char* test_npc_def_update_polyline_multiple_point(){
	tmx_object* tmx_obj = (tmx_object*)malloc(sizeof(tmx_object));
	tmx_obj->id = 1;
	tmx_obj->obj_type = OT_POLYLINE;
	tmx_obj->x = 0.0f;
	tmx_obj->y = 0.0f;
	tmx_obj->visible = 1;
	tmx_obj->name = "test1";
	tmx_obj->type = "NPC";
	tmx_obj->content.shape = (tmx_shape*)malloc(sizeof(tmx_shape));
	tmx_obj->content.shape->points = (double**)malloc(sizeof(double) * 6);
	tmx_obj->content.shape->points[0] =  (double*)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[1] =  (double*)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[2] =  (double*)malloc(sizeof(double) * 4);
	tmx_obj->content.shape->points[0][0] = 0.0f;
	tmx_obj->content.shape->points[0][1] = 0.0f;
	tmx_obj->content.shape->points[1][0] = 10.0f;
	tmx_obj->content.shape->points[1][1] = 1.0f;
	tmx_obj->content.shape->points[2][0] = 5.0f;
	tmx_obj->content.shape->points[2][1] = 5.0f; 
	tmx_obj->content.shape->points_len = 3;
	GameObject res_npc = game_object_new(tmx_obj, NPC);
	res_npc.value.npc.speed = 2;
	float d_time = 2.50f;
	printf("INFO: position: %f, %f\n", res_npc.position.x, res_npc.position.y);
	npc_default_update(&d_time, &res_npc);
	printf("INFO: position: %f, %f\n", res_npc.position.x, res_npc.position.y);
	mu_assert("test_npc_def_update_polyline_multiple: position after 2.50 isnt correct",
		  res_npc.position.x == 5.0f && res_npc.position.y == 0.50f);
	npc_default_update(&d_time, &res_npc);
	printf("INFO: position: %f, %f\n", res_npc.position.x, res_npc.position.y);
	mu_assert("test_npc_def_update_polyline_multiple: position after 5.0 isnt correct",
		  res_npc.position.x == 10.0f && res_npc.position.y == 1.0f);
	npc_default_update(&d_time, &res_npc);
	printf("INFO: position: %f, %f\n", res_npc.position.x, res_npc.position.y);
	mu_assert("test_npc_def_update_polyline_mutliple: position after 7.50 isnt correct",
		  res_npc.position.x == 5.0f && res_npc.position.y == 5.0f);
	free(res_npc.value.npc.walk_route);
	free(tmx_obj->content.shape->points[2]);
	free(tmx_obj->content.shape->points[1]);
	free(tmx_obj->content.shape->points[0]);
	free(tmx_obj->content.shape->points);
	free(tmx_obj->content.shape);
	free(tmx_obj);
	return 0;
}

static char* test_npc_def_update_stand_still(){
	tmx_object* tmx_obj = (tmx_object*)malloc(sizeof(tmx_object));
	tmx_obj->id = 1;
	tmx_obj->obj_type = OT_NONE;
	tmx_obj->x = 5.0f;
	tmx_obj->y = 10.0f;
	tmx_obj->visible = 1;
	tmx_obj->name = "test1";
	tmx_obj->type = "NPC";
	GameObject res_npc = game_object_new(tmx_obj, NPC);
	float d_time = 0.16f;
	mu_assert("test_npc_def_update_stand_still: init postion is not correct",
		  (res_npc.position.x == 5.0f && res_npc.position.y == 10));
	npc_default_update(&d_time, &res_npc);
	mu_assert("test_npc_def_update_stand_still: position after update " 
		  "is not correct", (res_npc.position.x == 5.0f &&
				     res_npc.position.y == 10.0f));
	free(tmx_obj);
	return 0;
}

static char* test_game_object_equals(){
/* 	t GameObject { */
/* 	char name[32]; */
/* 	bool visible; */
/* 	Vector2 position; */
/* 	enum goTypes type; */
/* 	void (*CustomUpdate) (float *); */
/* 	void (*CustomDraw) (float *); */
/* 	union goData value; */
/* 	struct GameObject* next; */
/* } GameObject; */
	GameObject go_1;
	strncpy(go_1.name, "test", 5);
	go_1.visible = 1;
	go_1.position = (Vector2){0.0f, 0.0f};
	go_1.type = NONE;
	go_1.CustomDraw = NULL;
	go_1.CustomUpdate = NULL;
	go_1.next = NULL;
	GameObject go_2;
	strncpy(go_2.name, "test", 5);
	go_2.visible = 1;
	go_2.position = (Vector2){0.0f, 0.0f};
	go_2.type = NONE;
	go_2.CustomDraw = NULL;
	go_2.CustomUpdate = NULL;
	go_2.next = NULL;
	mu_assert("test_game_object_equals: gameobjects are not equal!",
		  game_object_equals(&go_1, &go_2));
	go_2.visible = 0;
	mu_assert("test_game_object_equals: gameobjects are not equal!",
		  !game_object_equals(&go_1, &go_2));
	return 0;
}

static char* test_str_2_go_type(){
	mu_assert("test_str_2_go_type: go Type isn't NONE", str_2_go_type("") == NONE);
	mu_assert("test_str_2_go_type: go Type isn't COLLIDER",
		  str_2_go_type("Collider") == COLLIDER);
	mu_assert("test_str_2_go_type: go Type isn't ENCOUNTER",
		  str_2_go_type("Random Encounters") == ENCOUNTER);
	return 0;
}

static char* test_slocation_default_update(){
/* 	t GameObject { */
/* 	char name[32]; */
/* 	bool visible; */
/* 	Vector2 position; */
/* 	enum goTypes type; */
/* 	void (*CustomUpdate) (float *); */
/* 	void (*CustomDraw) (float *); */
/* 	union goData value; */
/* 	struct GameObject* next; */
/* } GameObject; */
	/* GameObject go_1; */
	mu_assert("test_slocation_default_update: position is not updated!",
		  1==0);
	return 0;
}

static char * all_tests() {
	mu_run_test(test_str_2_go_type);
	mu_run_test(test_game_object_equals);
	mu_run_test(test_npc_new);
	mu_run_test(test_npc_polyline);
	mu_run_test(test_npc_def_update_stand_still);
	mu_run_test(test_npc_def_update_polyline_one_point);
	mu_run_test(test_npc_def_update_polyline_multiple_point);
	mu_run_test(test_slocation_default_update);
	return 0;
}
 
int main(int argc, char **argv) {
	char *result = all_tests();
	if (result != 0) {
		printf("%s\n", result);
	}
	else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);
 
	return result != 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "raylib.h"
#include "tmx.h"
#include "prime.h"
#include <math.h>
#include <rlgl.h>
#include <libxml/parser.h>
// #include <libtcc.h>

#define LENGTH(x) (sizeof(x)/sizeof((x)[0]))

/*-- Game Play --*/
/* Variables and structs for gameplay */
typedef struct Character {
	char name[32];
	int level;
	int experience;
	int health;
	int maxHealth;
	int addiction;
	int attack;
	int accuracy;
	int speed;
	int defence;
	int spDefence;
	int attraction;
	//Description for monsters?
	//special powers
	//skills
	//equipment slots
	Texture2D graphic; //Test graphic for now
	Rectangle icon; //Test for now
	Rectangle battleGraph; //Test for now
	Rectangle collider;
	Vector2 position;
	Vector2 oldPosition;
	int moveDirection;
	int isPlayer;
} Character;

extern Character party[5]; //party[0] is the leader of the party
extern int currentRandomEncounter;
/*-- Game Functions --*/
void CharacterInput(float *);
void CharacterUpdate(float *);
void CharacterDraw(float *);

/*-- Game engine --*/
/* General variables and structs */
enum GameState{MAIN_MENU, MENU, COMBAT, MAP};
extern enum GameState gameState;
extern int screenWidth;
extern int screenHeight;
extern bool debugMode;
extern bool displayWindow;
extern Font textFont;

struct Properties{
	tmx_property property;
	struct Properties *next;
};

struct ObjectTypes{
	char name[32];
	unsigned int color;
	struct Properties firstProp;
	struct ObjectTypes *next;
};

typedef struct GuiItem{
	char* name;
} GuiItem;
//gameobject struct with a visible, type, union with correct value (collider, npc, teleport, event, etc.)
enum goTypes{COLLIDER, NPC, TELEPORT, SLOCATION, BUILDING, ENCOUNTER, NONE};

struct Collider {
	int layer;
	Rectangle area;
};
//structure npc based on the npc data from xml database?
struct NPC {
	char* name;
	int id;
};

struct Rencounter {
	int id0;
	int id1;
	int id2;
	int id3;
	int id4;
	Rectangle area;
};

struct Teleport {
	int id;
	int targetId;
	char targetMap[32];
};

struct Building {
	int id;

};

union goData {
	struct Collider collider;
	Vector2 startLocation; //This is basically the same as the position of the gameobject
	struct NPC npc;
	struct Rencounter ranEncounter;
	struct Teleport teleport;
	struct Building building;
};

typedef struct GameObject {
	char name[32];
	bool visible;
	Vector2 position;
	enum goTypes type;
	void (*CustomUpdate) (float *);
	void (*CustomDraw) (float *);
	union goData value;
	struct GameObject* next;
} GameObject;

typedef struct Scene {
	char * name;
	GameObject objects;
} Scene;

void SwitchState(enum GameState*, enum GameState);

/* TMX stuff and functions */
Texture2D *LoadMapTexture(const char *);
void UnloadMapTexture(Texture2D *); // Helper function for the TMX lib to unload a texture that was previously loaded
void RenderTmxMapToFramebuf(const char *, RenderTexture2D *); // Read a Tile map editor TMX map file and render the map into RenderTexture2D. 
void DrawTmxLayer(tmx_map *, tmx_layer *);
void DrawTmxObjectLayerDebug(tmx_map *, tmx_layer *, struct ObjectTypes *); //Draw the object layer debug
void DrawTmxImageLayer(tmx_map *, tmx_layer *); //Draw Image layer
void DrawTmxGroupLayer(tmx_map *, tmx_layer *, struct ObjectTypes *); //Draw the TmxGroupLayer

/* Draw functions */
void DrawEllipseSector(Vector2 , float ,float , int , int , int , Color);
void DrawEllipseSectorLines(Vector2, float, float, int, int, int, Color);
void ARGBtoSingleValues(unsigned int *, Color *);
void DrawPolygon(Vector2 *, int , Color );

/* XML/Database import functions */
void GetDefaultObjectTypes(struct ObjectTypes *, char *);
int get_color_rgb_f(const char *);

/* Hash-table struct and functions for texture atlases */
typedef struct{
    char* key;
    Rectangle value;
} atSprite;

typedef struct {
	int base_size;
    int size;
int count;
    atSprite** items;
} atTable;

atTable* ht_new();
void ht_del_hash_table(atTable* ht);
void ht_insert(atTable* ht, const char* key, Rectangle value);
Rectangle ht_search(atTable* ht, const char* key);
void ht_delete(atTable* h, const char* key);

extern atTable* charSpriteSheet;

/* General Functions */
//load scene function
//switch scene function
// other scene functions?

/* Animation system */
typedef struct Keyframe {
	float time;
	float rotation;
	Vector2 position; //not used in the animation functions or something like that yet
	Rectangle source;
} animKeyframe;

typedef struct AnimationClip {
	char name[16];
	int frames;
	float duration;
	int curFrame;
	float curTime;
	int loop;
	int playing;
	animKeyframe keyframes[32];
} animClip;

typedef struct AnimationController {
	//reference to graphics here
	//state like is playing or something? dont know if needed
	int amount;
	Rectangle graphic;
	animClip animations[32];
	animClip* curAnim;
	char* graphicFile;
} animController;

void playAnimation(animController*, char*, int); //sets the specific animation to start playing and stops currently playing animation if there is a animation playing. And if the animation should loop.
void updateAnimController(animController*, float*); //updates the animcontroller
void stopAnimation(animController*); //stops all animations
int isAnimPlaying(animController*, char*); //checks if animation is playing returns 1 if true, 0 if false


/* Main loop pointer functions */
extern void (*UpdatePhase) (float *);
extern void (*DrawPhase) (float *);
extern void (*InputPhase) (float *);

/* Main menu controls */
void MainMenuInit();
void MainMenuUpdate(float *);
void MainMenuDraw(float *);
void MainMenuInput(float *);
void MainMenuDeInit();

/* Combat Functions */
void CombatInit();
void CombatUpdate(float *);
void CombatDraw(float *);
void CombatInput(float *);
void CombatDeInit();

/* Area Map functions */
void MapInit();
void MapUpdate(float *);
void MapDraw(float *);
void MapInput(float *);
void MapDeInit();

/* (Pause) menu functions */

/* GUI functions */
int selectItem(int, int, char*);
bool IsFocused(int *, int* , int);
bool GuiButton(Rectangle, char*, int, Font);

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "raylib.h"
#include "tmx.h"
#include "prime.h"
#include <math.h>
#include "rlgl.h"
#include "libxml/parser.h"
// #include <libtcc.h>
#ifndef CENTRAL_SYSTEMS_H
#define CENTRAL_SYSTEMS_H

#define LENGTH(x) (sizeof(x)/sizeof((x)[0]))

/*-- Game Play --*/
/* Variables and structs for gameplay */
typedef struct Character {
	char name[32];
	int level;
	int experience;
	int health;
	int maxHealth;
	int addiction;
	int attack;
	int accuracy;
	int speed;
	int defence;
	int spDefence;
	int attraction;
	//Description for monsters?
	//special powers
	//skills
	//equipment slots
	Texture2D graphic; //Test graphic for now
	Rectangle icon; //Test for now
	Rectangle battleGraph; //Test for now
	Rectangle collider;
	Vector2 position;
	Vector2 oldPosition;
	int moveDirection;
	int isPlayer;
} Character;

extern Character party[5]; //party[0] is the leader of the party
extern int currentRandomEncounter;
extern char current_scene_name[32];
/*-- Game Functions --*/
void CharacterInput(float *);
void CharacterUpdate(float *);
void CharacterDraw(float *);

/*-- Game engine --*/
/* General variables and structs */
typedef struct game_state {
  void (*init_phase) ();
  void (*deinit_phase) ();
  void (*update_phase) (float *);
  void (*draw_phase) (float *);
  void (*input_phase) (float *);
} game_state;

typedef struct game_states {
  game_state MAIN_MENU;
  game_state MENU;
  game_state COMBAT;
  game_state MAP;
  game_state DIALOGUE;
  game_state NONE;
} game_states;

typedef struct game_state_stack {
  game_state states[5];
} game_state_stack;

extern game_states current_game_states;
extern game_state_stack current_game_state;
extern int screenHeight;
extern int screenWidth;
extern bool debugMode;
extern bool displayWindow;
extern Font textFont;
extern Texture2D g_sprite_sheet;
struct Properties{
	tmx_property property;
	struct Properties *next;
};

struct ObjectTypes{
	char name[32];
	unsigned int color;
	struct Properties firstProp;
	struct ObjectTypes *next;
};

typedef struct GuiItem{
	char* name;
} GuiItem;

//gameobject struct with a visible, type, union with correct value (collider, npc, teleport, event, etc.)
enum goTypes{COLLIDER, NPC, TELEPORT, SLOCATION, BUILDING, ENCOUNTER, NONE};
typedef struct Scene Scene;

struct Collider {
	int layer;
	Rectangle area;
};

//structure npc based on the npc data from xml database? yes structure from the xml!
struct point_list {
	Vector2 point;
	struct point_list* previous;
	struct point_list* next;
};

struct NPC {
	int id;
        int speed;
	int can_talk;
	int is_talking;
	int route_length;
	enum direction_route{PREVIOUS=-1, NEXT=1} direction_route;
	char* name;
	struct point_list* current_pos;
	struct point_list* walk_route;
};

struct Rencounter {
	int id0;
	int id1;
	int id2;
	int id3;
	int id4;
	Rectangle area;
};

struct Teleport {
	int id;
	int target_id;
	char target_map[32];
};

struct Building {
	int id;
};

union goData {
	struct Collider collider;
	Vector2 startLocation; //This is basically the same as the position of the gameobject
	struct NPC npc;
	struct Rencounter ranEncounter;
	struct Teleport teleport;
	struct Building building;
};

typedef struct GameObject {
	char name[32];
	bool visible;
	Vector2 position;
	enum goTypes type;
	int (*CustomUpdate) (float *, struct GameObject *, Scene*);
	int (*CustomDraw) (float *, struct GameObject *);
	union goData value;
	struct GameObject* next;
} GameObject;

typedef struct Scene {
	char * name;
	int first_time;
	float danger;
	RenderTexture2D mapFrameBuffer;
	Camera2D camera;
	GameObject objects;
} Scene;

/* game object functions */
GameObject game_object_new(tmx_object* tmx_obj, enum goTypes type);
GameObject game_object_npc_new(tmx_object* tmx_obj);
int npc_default_update(float *d_time, struct GameObject * self, Scene* scene);
int slocation_default_update(float* d_time, struct GameObject* self, Scene* scene);
int encounter_default_update(float* d_time, struct GameObject* self, Scene* scene);
int teleport_default_update(float* d_time, struct GameObject* self, Scene* scene);
int collider_default_update(float* d_time, struct GameObject* self, Scene* scene);
int8_t game_object_equals(GameObject* first_gobj, GameObject* second_gobj);
enum goTypes str_2_go_type(const char* type);

/* scene functions  */
void LoadScene(Scene** curScene, char* name);

void SwitchState(game_state_stack*, game_state);
void add_state_to_stack(game_state_stack*, game_state);
void init_game_state_stack(game_state_stack*);
void pop_state_from_stack (game_state_stack*);

/* TMX stuff and functions */
Texture2D *LoadMapTexture(const char *);
void UnloadMapTexture(Texture2D *); // Helper function for the TMX lib to unload a texture that was previously loaded
void RenderTmxMapToFramebuf(const char *, RenderTexture2D *); // Read a Tile map editor TMX map file and render the map into RenderTexture2D. 
void DrawTmxLayer(tmx_map *, tmx_layer *);
void DrawTmxObjectLayerDebug(tmx_map *, tmx_layer *, struct ObjectTypes *); //Draw the object layer debug
void DrawTmxImageLayer(tmx_map *, tmx_layer *); //Draw Image layer
void DrawTmxGroupLayer(tmx_map *, tmx_layer *, struct ObjectTypes *); //Draw the TmxGroupLayer

/* Draw functions */
void DrawEllipseSector(Vector2 , float ,float , int , int , int , Color);
void DrawEllipseSectorLines(Vector2, float, float, int, int, int, Color);
void ARGBtoSingleValues(unsigned int *, Color *);
void DrawPolygon(Vector2 *, int , Color );

/* XML/Database import functions */
void GetDefaultObjectTypes(struct ObjectTypes *, char *);
int get_color_rgb_f(const char *);

/* Hash-table struct and functions for texture atlases */
typedef struct{
	char* key;
	Rectangle value;
} atSprite;

typedef struct {
	int base_size;
	int size;
	int count;
	atSprite** items;
} atTable;

/* maybe making use of Generic for this (sheets, animations) */
atTable* ht_new();
void ht_del_hash_table(atTable* ht);
void ht_insert(atTable* ht, const char* key, Rectangle value);
Rectangle ht_search(atTable* ht, const char* key);
void ht_delete(atTable* h, const char* key);

extern atTable* charSpriteSheet;

/* General Functions */
//load scene function
//switch scene function
// other scene functions?

/* Animation system */
typedef struct Keyframe {
	float time;
	float rotation;
	Vector2 position; //not used in the animation functions or something like that yet
	Rectangle source;
} animKeyframe;

typedef struct AnimationClip {
	char name[16];
	int frames;
	float duration;
	int curFrame;
	float curTime;
	int loop;
	int playing;
	animKeyframe keyframes[32];
} animClip;

typedef struct AnimationController {
	//reference to graphics here
	//state like is playing or something? dont know if needed
	int amount;
	Rectangle graphic;
	animClip animations[32];
	animClip* curAnim;
	char* graphicFile;
} animController;

void playAnimation(animController*, char*, int); //sets the specific animation to start playing and stops currently playing animation if there is a animation playing. And if the animation should loop.
void updateAnimController(animController*, float*); //updates the animcontroller
void stopAnimation(animController*); //stops all animations
int isAnimPlaying(animController*, char*); //checks if animation is playing returns 1 if true, 0 if false

/* Main loop pointer functions */
void update_phase (float *, game_state_stack*);
void draw_phase   (float *, game_state_stack*);
void input_phase  (float *, game_state_stack*);
void deinit_phases (game_state_stack*);

/* Main menu controls */
void MainMenuInit();
void MainMenuUpdate(float *);
void MainMenuDraw(float *);
void MainMenuInput(float *);
void MainMenuDeInit();

/* Combat Functions */
void CombatInit();
void CombatUpdate(float *);
void CombatDraw(float *);
void CombatInput(float *);
void CombatDeInit();

/* Area Map functions */
void MapInit();
void MapUpdate(float *);
void MapDraw(float *);
void MapInput(float *);
void MapDeInit();

/* Dialogue default functions */
void dialogue_init();
void dialogue_update(float *);
void dialogue_draw(float *);
void dialogue_input(float *);
void dialogue_deinit();

/* (Pause) menu functions */

/* GUI functions */
#define RES_SCALE_FACTOR (0.00625 * screenHeight)
#define BUT_F_SIZE ((8.0f) * (RES_SCALE_FACTOR))
int selectItem(int, int, char*);
bool IsFocused(int *, int* , int);
bool GuiButton(Rectangle, const char*, int, Font);
bool gui_dialog_box(const char*, Vector2, float, Font, Color);
#endif

#include "central_systems.h"

#ifndef CAMERA_RPG_H
#define CAMERA_RPG_H

#define CAM_DEF_ZOOM_FAC (0.00625 * screenHeight)

Camera2D CameraInit();
void CameraUpdate(Camera2D *p_camera, RenderTexture2D *p_bg_texture);

#endif

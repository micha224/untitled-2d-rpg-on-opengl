#include "editor.h"

typedef struct keyframe {
	float time;
	float rotation;
	int position[2]; //not used in the animation functions or something like that yet
	char sourceTag[32];
	struct keyframe* next;
} anim_keyframe;

typedef struct animation_clip {
	char id[8];
	char name[16];
	char file[32];
	int frames;
	float duration;
	int loop;
	anim_keyframe* keyframes;
} animation;

typedef struct animation_db {
	int16_t size;
	animation* animations;
} animation_db;


//logic
void swap_keyframes(anim_keyframe* p_keyframe_dest, anim_keyframe* p_keyframe_source);
void sort_keyframes(anim_keyframe* p_keyframes);
void delete_keyframe(animation* p_animation, anim_keyframe* p_del_frame);
void parse_keyframe(xmlNodePtr p_node, anim_keyframe* p_keyframe);
void parse_keyframes(xmlNodePtr p_nodes, anim_keyframe* p_keyframes);
void parse_animation(xmlNodePtr p_animation_node, animation* p_anim_db,
		     int p_index);
int8_t animation_db_equals(animation_db* p_db, animation_db* p_db_ref);
animation_db* load_animation_database();
animation_db* animation_load_db_from_doc(xmlDocPtr p_doc);
int8_t animation_database_save(animation_db* p_anim_db, int16_t p_db_size,
			       char* p_file_path);
xmlDocPtr animation_create_database_doc(animation_db* p_anim_db);
animation_db* animation_add(char* p_animation_name, animation_db* p_anim_database);
animation_db* animation_delete(char* p_anim_id, animation_db* p_anim_database);
void animation_cur_anim_add_frame(animation* p_animation,
					 anim_keyframe* p_keyframe,
					 gpointer p_index);
int8_t animation_keyframe_equals(anim_keyframe* p_anim_keyframe,
				 anim_keyframe* p_anim_keyframe_ref);
int8_t animation_equals(animation* p_anim, animation* p_anim_ref);


//gui
GtkWidget* get_widget_child_by_name(GtkWidget* p_container, gchar* p_name);

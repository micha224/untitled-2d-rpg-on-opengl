#include <stdio.h>
#include "minunit.h"
#include "animation.h"

int tests_run = 0;

static char* test_get_widget_by_name_in_container(){
	GtkWidget* l_container = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	GtkWidget* l_child = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	char * l_name = "a_child";
	gtk_widget_set_name(l_child, l_name);
	gtk_box_pack_start(GTK_BOX(l_container), l_child, 0, 0, 0);
	
	GtkWidget* l_result = get_widget_child_by_name(l_container, l_name);	
	mu_assert("Error, widget is NULL", l_result != NULL);
	mu_assert("Error, name is not a_container",
		  !strcmp(l_name, gtk_widget_get_name(l_result)));


	char * l_name_2 = "a_child_2";
	l_child = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	gtk_widget_set_name(l_child, l_name_2);
	gtk_box_pack_start(GTK_BOX(l_container), l_child, 0, 0, 0);

	l_result = get_widget_child_by_name(l_container, l_name_2);	
	mu_assert("Error, widget is NULL", l_result != NULL);
	mu_assert("Error, name is not a_container",
		  !strcmp(l_name_2, gtk_widget_get_name(l_result)));
	gtk_widget_destroy(l_container);
	return 0;
}

static char* test_get_widget_by_name_two_levels_deep(){
	GtkWidget* l_container = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	GtkWidget* l_child = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	gtk_box_pack_start(GTK_BOX(l_container), l_child, 0, 0, 0);
	
	GtkWidget* l_child_2 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	char * l_name = "a_child";
	gtk_widget_set_name(l_child_2, l_name);
	gtk_box_pack_start(GTK_BOX(l_child), l_child_2, 0, 0, 0);

	GtkWidget* l_result = get_widget_child_by_name(l_container, l_name);	
	mu_assert("Error, widget is NULL", l_result != NULL);
	mu_assert("Error, name is not a_container",
		  !strcmp(l_name, gtk_widget_get_name(l_result)));
	gtk_widget_destroy(l_container);
	return 0;
}

static char* test_create_anim_db_doc(){
        animation_db* l_anim_db;
	xmlDocPtr l_db_doc = animation_create_database_doc(l_anim_db);
	mu_assert("Error, db_doc is NULL", l_db_doc != NULL);
	mu_assert("Error, db_doc is empty",
		  xmlDocGetRootElement(l_db_doc) != NULL);
	xmlFreeDoc(l_db_doc);
	return 0;
}

static char* test_xml_free_doc_null(){
	xmlFreeDoc(NULL);
	return 0;
}

static char* test_create_anim_database_animation_root_element(){
	animation_db* l_anim_db;
	xmlDocPtr l_db_doc = animation_create_database_doc(l_anim_db);
	xmlNodePtr l_root = xmlDocGetRootElement(l_db_doc);
	mu_assert("Error, name of root element isn't AnimationDatabase",
		  !strcmp(l_root->name, "AnimationDatabase"));
	xmlFreeDoc(l_db_doc);
	return 0;
}

static char* test_animation_keyframe_equals(){
	anim_keyframe k_frame;
	k_frame.rotation = 9.90f;
	k_frame.position[1] = 10;
	strcpy(k_frame.sourceTag, "test");
	anim_keyframe k_frame_ne;
	k_frame_ne.rotation = 9.90f;
	k_frame_ne.position[1] = 10;
	k_frame_ne.position[0] = 5;
	mu_assert("Error, different keyframes are equal",
		  !animation_keyframe_equals(&k_frame, &k_frame_ne));
	anim_keyframe k_frame_eq;
	k_frame_eq.rotation = 9.90f;
	strcpy(k_frame_eq.sourceTag, "test");
	k_frame_eq.position[1] = 10;
	k_frame_eq.position[0] = 0;
	mu_assert("Error, keyframes are not equal",
		  animation_keyframe_equals(&k_frame, &k_frame_eq));
	return 0;
}

static char* test_animation_equals(){
	animation anim1;
	animation anim2;
	/* create animations  */
	mu_assert("Error, animations are not equal",
		  animation_equals(&anim1, &anim2));
	return 0;
}

static char* test_animation_db_equals(){
	return 0;
}

static char* test_created_doc_equals_anim_database_object(){
	animation_db* l_anim_db;
	xmlDocPtr l_db_doc = animation_create_database_doc(l_anim_db);
	animation_db* l_created_db = animation_load_db_from_doc(l_db_doc);
	mu_assert("Error, animation objects aren't equal",
		  animation_db_equals(l_anim_db, l_created_db));
	return 0;
}

/* static char* test_create_anim_database_check_dtd(){ */
/* 	create anim database */
/* xmlValidateDtd */
/* 				  return 0; */
/* } */

static char * all_tests() {
	mu_run_test(test_get_widget_by_name_in_container);
	mu_run_test(test_get_widget_by_name_two_levels_deep);
	mu_run_test(test_create_anim_db_doc);
	mu_run_test(test_create_anim_database_animation_root_element);
	/* mu_run_test(test_created_doc_equals_anim_database_object); */
	mu_run_test(test_animation_keyframe_equals);
	mu_run_test(test_xml_free_doc_null);
	mu_run_test(test_animation_equals);
     return 0;
}
 
int main(int argc, char **argv) {
	GtkApplication *l_app;
	int l_status;

	l_app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
	l_status = g_application_run(G_APPLICATION(l_app),argc,argv);
	g_object_unref(l_app);
	char *result = all_tests();
	if (result != 0) {
		printf("%s\n", result);
	}
	else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);
 
	return result != 0;
}

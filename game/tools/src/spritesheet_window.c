#include "editor.h"


void sprite_sheet_window(GtkWidget* p_widget, gpointer p_data)
{
	GtkWidget *l_grid, *l_label, *l_window;

	l_grid = gtk_grid_get_child_at(p_data, 0, 1);
	gtk_widget_destroy(l_grid);
	l_grid = gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(l_grid), 20);
	gtk_grid_set_row_spacing(GTK_GRID(l_grid), 5);
	l_window = gtk_widget_get_toplevel(p_data);

	gtk_grid_attach(GTK_GRID(p_data), l_grid, 0, 1, 30, 10);
  
	//ANIMATIONS LIST
	l_label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(l_label), "<big><b>SpriteSheet window</b></big>");
	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 1, 1, 1);

	gtk_widget_show_all(l_window);
}

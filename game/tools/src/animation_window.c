#include "animation.h"

void animation_timeline_sub_window(GtkWidget*);
void animation_select_container(GtkWidget*);

sprite_sheet g_animation_sprite_sheet;
// char* animationSheetFilePath;

animation_db* g_animation_database;
animation* g_cur_animation;

GtkWidget* get_widget_child_by_name(GtkWidget* p_container, gchar* p_name)
{
	GList *l_children = gtk_container_get_children(GTK_CONTAINER(p_container));
	for (; l_children != NULL; l_children = l_children->next){
		if(!strcmp(gtk_widget_get_name(l_children->data),
				   p_name))
			return l_children->data;
		GtkWidget *l_child = get_widget_child_by_name(l_children->data,
							      p_name);
		if(l_child != NULL){
			return l_child;
		}
			
	}
	return NULL;
}

void animation_gui_save_dialog(GtkWidget* p_widget)
{
	GtkWidget *l_dialog =
		gtk_dialog_new_with_buttons("Save animations",
					    GTK_WINDOW(gtk_widget_get_toplevel(p_widget)),
					    GTK_DIALOG_DESTROY_WITH_PARENT,
					    "Close",
					    GTK_RESPONSE_OK,
					    NULL);

	GtkWidget * l_content_area = gtk_dialog_get_content_area(GTK_DIALOG(l_dialog));
	GtkWidget *l_grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(l_content_area), l_grid);
	
	GtkWidget *l_label = gtk_label_new("It is saving the animations...");
	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 0, 1, 1);
	
	GtkWidget *l_prog_bar = gtk_progress_bar_new();
	int l_timeout_tag = g_timeout_add (20, (GSourceFunc)gtk_progress_bar_pulse,
		       GTK_PROGRESS_BAR(l_prog_bar));
	gtk_grid_attach(GTK_GRID(l_grid), l_prog_bar, 0, 1, 1, 1);
	gtk_widget_show_all(l_dialog);
	int8_t l_saved = animation_database_save(g_animation_database,
						 g_animation_database->size, ANIM_DB_FILE_PATH);

	if(l_saved) {
		gtk_label_set_text(GTK_LABEL(l_label), "Animations saved!!!");
		g_source_remove(l_timeout_tag);
		gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(l_prog_bar),
					      1.0);
	}
	int l_result = gtk_dialog_run(GTK_DIALOG(l_dialog));
	if(l_result == GTK_RESPONSE_OK){
	}
	gtk_widget_destroy(l_dialog);
}

void animation_add_dialog_open(GtkWidget* p_widget)
{
	GtkWidget *l_dialog =
		gtk_dialog_new_with_buttons("Add Animation",
					    GTK_WINDOW(gtk_widget_get_toplevel(p_widget)),
					    GTK_DIALOG_DESTROY_WITH_PARENT,
					    "Add",
					    GTK_RESPONSE_OK,
					    "Cancel",
					    GTK_RESPONSE_CANCEL,
					    NULL);

	GtkWidget * l_content_area = gtk_dialog_get_content_area(GTK_DIALOG(l_dialog));
	GtkWidget *l_grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(l_content_area), l_grid);
	
	GtkWidget *l_label = gtk_label_new("Aninamation Name:");
	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 0, 1, 1);
	
	GtkWidget *l_entry = gtk_entry_new();
	gtk_entry_set_placeholder_text(GTK_ENTRY(l_entry),
				       "Enter Animation name...");
	gtk_grid_attach(GTK_GRID(l_grid), l_entry, 0, 1, 1, 1);
	gtk_widget_show_all(l_dialog);

	int l_result = gtk_dialog_run(GTK_DIALOG(l_dialog));
	if(l_result == GTK_RESPONSE_OK){
		g_animation_database = animation_add(
			      (char*)gtk_entry_get_text(GTK_ENTRY(l_entry)),
			      g_animation_database);
		p_widget = gtk_widget_get_ancestor(p_widget, GTK_TYPE_GRID);
		animation_select_container(p_widget);
	}
	gtk_widget_destroy(l_dialog);
}

void animation_delete_dialog_open(GtkWidget* p_widget, char* p_anim_id)
{
	GtkWidget *l_dialog =
		gtk_dialog_new_with_buttons("Delete Animation",
					    GTK_WINDOW(gtk_widget_get_toplevel(p_widget)),
					    GTK_DIALOG_DESTROY_WITH_PARENT,
					    "Delete",
					    GTK_RESPONSE_OK,
					    "Cancel",
					    GTK_RESPONSE_CANCEL,
					    NULL);

	GtkWidget * l_content_area = gtk_dialog_get_content_area(GTK_DIALOG(l_dialog));
	GtkWidget *l_grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(l_content_area), l_grid);
	
	GtkWidget *l_label = gtk_label_new("Are you sure you want to delete the animation?");
	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 0, 1, 1);
	gtk_widget_show_all(l_dialog);

	int l_result = gtk_dialog_run(GTK_DIALOG(l_dialog));
	if(l_result == GTK_RESPONSE_OK){
		g_animation_database = animation_delete(g_cur_animation->id,
							g_animation_database);
	        l_grid = gtk_widget_get_ancestor(p_widget, GTK_TYPE_GRID);
		animation_select_container(l_grid);
	}
	gtk_widget_destroy(l_dialog);
}

static void open_sprite_sheet_dialog(GtkWidget *p_widget, gpointer p_data)
{
	GtkWidget *l_dialog;
	GtkFileChooserAction l_action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint l_res;

	l_dialog = gtk_file_chooser_dialog_new ("Open File",
						GTK_WINDOW(gtk_widget_get_toplevel(p_data)),
						l_action,
						"Cancel",
						GTK_RESPONSE_CANCEL,
						"Open",
						GTK_RESPONSE_ACCEPT,
						NULL);

	l_res = gtk_dialog_run (GTK_DIALOG (l_dialog));
	if (l_res == GTK_RESPONSE_ACCEPT) {
		GtkFileChooser *l_chooser = GTK_FILE_CHOOSER (l_dialog);
		strncpy(g_cur_animation->file,
			gtk_file_chooser_get_filename (l_chooser),
			32);
		gtk_entry_set_text(GTK_ENTRY(p_data), g_cur_animation->file);
		//
	}

	gtk_widget_destroy (l_dialog);
}

static int load_sprite_sheet(sprite_sheet* p_sp_sheet)
{
	xmlDoc *l_db = NULL;
	g_print("loading sprite_sheet: %s\nname_spritesheet: %s\n",
		g_cur_animation->file, g_cur_animation->name);
	l_db = xmlReadFile(g_cur_animation->file, NULL, 0);

	//TODO make a error popup dialog appear when db is NULL

	if(l_db != NULL){
		xmlNodePtr l_rootElement = NULL;
		xmlNodePtr l_node = NULL;
		
		l_rootElement = xmlDocGetRootElement(l_db);
		l_node = l_rootElement->children;
		
		struct frame_sprite_sheet l_head;
		struct frame_sprite_sheet* l_current = &l_head;
		l_current->next = NULL;
		
		while(l_node != NULL) {
			if(!strcmp((char*)l_node->name, "Image")) {
				//TODO make that path to the image absolute with strchr and a extra function that goes through filepath and removes everything after the last ../ indicated in the image source
				strcpy(p_sp_sheet->imageSource, (char *)xmlGetProp(l_node,(xmlChar *)"source"));
			}
			else if(!strcmp((char*)l_node->name, "Frames")){
				l_node = l_node->children;
				continue;
			}
			else if(!strcmp((char*)l_node->name, "Frame")){
				struct frame_sprite_sheet* l_temp_frame = (struct frame_sprite_sheet*)
					malloc(sizeof(struct frame_sprite_sheet));
				strcpy(l_temp_frame->tag, (char *)xmlGetProp(l_node,(xmlChar *)"tag"));
				l_temp_frame->x = atoi((char *)xmlGetProp(l_node,(xmlChar *)"x"));
				l_temp_frame->y = atoi((char *)xmlGetProp(l_node,(xmlChar *)"y"));
				l_temp_frame->width = atoi((char *)xmlGetProp(l_node,(xmlChar *)"width"));
				l_temp_frame->height = atoi((char *)xmlGetProp(l_node,(xmlChar *)"height"));
				l_temp_frame->next = NULL;
				l_current->next = l_temp_frame;
				l_current = l_current->next;
				
			}
			l_node = l_node->next;
		}
		p_sp_sheet->frames = l_head.next;
		xmlFreeDoc(l_db);
		return 1;
	}
	return 0;
}

static void change_sheet_entry (GtkEntry *p_entry, gpointer p_user_data)
{
	strcpy(g_cur_animation->file,(char*)gtk_entry_get_text (p_entry));

	g_print ("\nHello %s!\n\n", g_cur_animation->file);
}

void get_frame_image(GtkWidget* p_dest, struct frame_sprite_sheet* p_frame)
{
	GdkPixbuf* l_pixbuf_area = gdk_pixbuf_new(GDK_COLORSPACE_RGB,
					          TRUE, 8, p_frame->width, p_frame->height);
	gdk_pixbuf_fill(l_pixbuf_area,0xffffffff);
	gdk_pixbuf_copy_area(g_animation_sprite_sheet.image,
			     p_frame->x, p_frame->y, p_frame->width,
			     p_frame->height, l_pixbuf_area,0,0);
	gtk_image_set_from_pixbuf (GTK_IMAGE(p_dest), l_pixbuf_area);
}

static void load_frame(GtkWidget *p_widget, gpointer p_data)
{

	GtkComboBox *l_combo_box = GTK_COMBO_BOX(p_widget);
	if(gtk_combo_box_get_active(l_combo_box)!= 0){
		gchar *l_choice = gtk_combo_box_text_get_active_text(
			GTK_COMBO_BOX_TEXT(l_combo_box));
		struct frame_sprite_sheet* l_frame = g_animation_sprite_sheet.frames;
		while(l_frame != NULL) {
			if(!strcmp(l_choice, l_frame->tag)) {
				g_print("node tag: %s\tx:%d\ty:%d\twidth:%d\theight:%d\n",  l_frame->tag,
					l_frame->x,
					l_frame->y,
					l_frame->width,
					l_frame->height);
				get_frame_image(p_data, l_frame);
				GdkPixbuf* l_pixbuf_area = gtk_image_get_pixbuf(p_data);
				l_pixbuf_area = gdk_pixbuf_scale_simple(l_pixbuf_area,
									200, 200,
									GDK_INTERP_NEAREST);
				gtk_image_set_from_pixbuf (p_data, l_pixbuf_area);
			}
			l_frame = l_frame->next;
		}
    
		g_free(l_choice);
	}
}

/* using dialog windows to insert new entries to the database  */
static void new_frame_window(GtkWidget* p_widget, gpointer p_data) { 
	GtkWidget *l_top_window, *l_window, *l_grid, *l_label, *l_time_button,
		*l_rotation_button, *l_pos_button_x, *l_pos_button_y,
		*l_content_area, *l_combo_box_sheet, *l_image;
	anim_keyframe* l_cur_keyframe;
	GtkAdjustment* l_spin_button_adj;
	l_top_window = gtk_widget_get_toplevel(p_widget);
	l_window = gtk_dialog_new_with_buttons("New Frame", GTK_WINDOW(l_top_window),
					       GTK_DIALOG_DESTROY_WITH_PARENT, 
					       "Add/Change Frame", 
					       GTK_RESPONSE_ACCEPT,
					       "Delete Frame",
					       GTK_RESPONSE_REJECT,
					       "Cancel",
					       GTK_RESPONSE_CANCEL, NULL);
	
	l_content_area = gtk_dialog_get_content_area(GTK_DIALOG(l_window));
	l_grid = gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(l_grid), 10);
	gtk_grid_set_column_homogeneous(GTK_GRID(l_grid), TRUE);
	// g_signal_connect_swapped (window,
	//                          "response",
	//                          G_CALLBACK (gtk_widget_destroy),
	//                          window);

	//sprite sheet!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	load_sprite_sheet(&g_animation_sprite_sheet);
	struct frame_sprite_sheet* l_frame = g_animation_sprite_sheet.frames;
	l_combo_box_sheet = gtk_combo_box_text_new();
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(l_combo_box_sheet), "Choose frame.");
	while(l_frame != NULL) {
		gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(l_combo_box_sheet),
					  l_frame->tag,
					  l_frame->tag);
		l_frame = l_frame->next;
	}
	
	g_animation_sprite_sheet.image = gdk_pixbuf_new_from_file(g_animation_sprite_sheet.imageSource, NULL);
	l_image = gtk_image_new_from_pixbuf(g_animation_sprite_sheet.image);

	gtk_combo_box_set_active(GTK_COMBO_BOX(l_combo_box_sheet),0);
	g_signal_connect(l_combo_box_sheet, "changed", G_CALLBACK(load_frame), l_image);
	gtk_grid_attach(GTK_GRID(l_grid), l_combo_box_sheet, 0,0,1,1);
	//end spritesheet
	//check if it is changing a frame or adding a frame
	if(!strcmp(gtk_button_get_label(GTK_BUTTON(p_widget)), "+")){
		g_print("Keyframe is null! %d\n", p_data);
		l_cur_keyframe = (anim_keyframe*)malloc(sizeof(anim_keyframe));
		l_cur_keyframe->next = NULL;
		strcpy(l_cur_keyframe->sourceTag, "");
		l_cur_keyframe->time = 0.0f;
		l_cur_keyframe->rotation = 0;
		l_cur_keyframe->position[0] = 0;
		l_cur_keyframe->position[1] = 0;
	}
	else{
		l_cur_keyframe = g_cur_animation->keyframes;
		for(int8_t i = 0; i < GPOINTER_TO_INT(p_data); i++,
			    l_cur_keyframe = l_cur_keyframe->next) {
			if((i+1) == GPOINTER_TO_INT(p_data)){
				p_data = NULL;
			}
		}
		g_print("Keyframe is not null! %s, %f\n", l_cur_keyframe->sourceTag, l_cur_keyframe->time);
		struct frame_sprite_sheet* i_frame = g_animation_sprite_sheet.frames;
		for(int8_t i = 0; i_frame != NULL;
		    i++, i_frame = i_frame->next){
			if(!strcmp(l_cur_keyframe->sourceTag, i_frame->tag)){
				gtk_combo_box_set_active(GTK_COMBO_BOX(l_combo_box_sheet),i+1);
				break;
			}
		}
	}
  
	l_label = gtk_label_new("Time (Proportional):");
	l_spin_button_adj = gtk_adjustment_new(l_cur_keyframe->time,
					       0.0f,
					       1.0f,
					       0.05f,
					       0.1f,
					       0.0f);
	l_time_button = gtk_spin_button_new(l_spin_button_adj,
					    0.05f,
					    2);
	/* gtk_spin_button_set_value (GTK_SPIN_BUTTON(l_time_button), l_cur_keyframe->time); */
 
	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 1, 1, 1);
	gtk_grid_attach(GTK_GRID(l_grid), l_time_button, 1, 1, 1, 1);

	l_label = gtk_label_new("Rotation (Degrees):");
	l_spin_button_adj = gtk_adjustment_new(l_cur_keyframe->rotation,
					       0,
					       360,
					       90,
					       1,
					       0);
	l_rotation_button = gtk_spin_button_new(l_spin_button_adj, 1.0, 0);
	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 2, 1, 1);
	gtk_grid_attach(GTK_GRID(l_grid), l_rotation_button, 1, 2, 1, 1);

	l_label = gtk_label_new("Position (x,y):");
	l_spin_button_adj = gtk_adjustment_new(l_cur_keyframe->position[0],
					       INT_MIN,
					       INT_MAX,
					       1,
					       1,
					       0);
	l_pos_button_x = gtk_spin_button_new(l_spin_button_adj, 1.0, 0);
	l_spin_button_adj = gtk_adjustment_new(l_cur_keyframe->position[1],
					       INT_MIN,
					       INT_MAX,
					       1,
					       1,
					       0);
	l_pos_button_y = gtk_spin_button_new(l_spin_button_adj, 1.0, 0);

	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 3, 1, 1);
	gtk_grid_attach(GTK_GRID(l_grid), l_pos_button_x, 1, 3, 1, 1);
	gtk_grid_attach(GTK_GRID(l_grid), l_pos_button_y, 2, 3, 1, 1);
	// g_signal_connect (GTK_ENTRY (entry), "activate", G_CALLBACK (on_activate), NULL);

	gtk_grid_attach(GTK_GRID(l_grid), l_image, 2, 1, 4,4);
	// gtk_container_add (GTK_CONTAINER (content_area), GTK_WIDGET (image));

	gtk_container_add(GTK_CONTAINER(l_content_area), l_grid);

	gtk_widget_show_all(l_window);
	int l_result = gtk_dialog_run(GTK_DIALOG(l_window));
	if(l_result == GTK_RESPONSE_ACCEPT) {
		g_print("Accept\n");
		struct frame_sprite_sheet* i_frame = g_animation_sprite_sheet.frames;
		for(int8_t i = 0; i_frame != NULL;
		    i++, i_frame = i_frame->next){
			if(i == (gtk_combo_box_get_active(GTK_COMBO_BOX(l_combo_box_sheet))-1)){
				strcpy(l_cur_keyframe->sourceTag, i_frame->tag);
			}
		}
		l_cur_keyframe->position[0] =
			gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(l_pos_button_x));
		l_cur_keyframe->position[1] =
			gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(l_pos_button_y));
		l_cur_keyframe->time =
			gtk_spin_button_get_value(GTK_SPIN_BUTTON(l_time_button));
		l_cur_keyframe->rotation =
			gtk_spin_button_get_value(GTK_SPIN_BUTTON(l_rotation_button));
		animation_cur_anim_add_frame(g_cur_animation,
					     l_cur_keyframe, p_data);

		if(!strcmp(gtk_button_get_label(GTK_BUTTON(p_widget)), "+")){
			animation_timeline_sub_window(p_data);
		}
	}else if(l_result == GTK_RESPONSE_REJECT){
		if(strcmp(gtk_button_get_label(GTK_BUTTON(p_widget)), "+")){
		        delete_keyframe(g_cur_animation, l_cur_keyframe);
			g_print("debug: frame got deleted!\n");
			GtkWidget *l_timeline_grid =
				get_widget_child_by_name(l_top_window,
						   "timeline_sub_window");
			if (l_timeline_grid != NULL){
				animation_timeline_sub_window(l_timeline_grid);
			}
			g_print("debug: l_timeline is NULL\n");
		}
	}else if(l_result == GTK_RESPONSE_CANCEL) {
		g_print("cancel\n");
		if(!strcmp(gtk_button_get_label(GTK_BUTTON(p_widget)), "+")){
			free(l_cur_keyframe);
		}
	}
	gtk_widget_destroy(l_window);
}

void animation_duration_button_callback(GtkWidget *p_widget,
					animation *p_animation)
{
	g_print("set the duration");
	p_animation->duration =
		gtk_spin_button_get_value(GTK_SPIN_BUTTON(p_widget));
}

void animation_timeline_sub_window(GtkWidget* p_grid)
{
	GtkWidget *l_label, *l_button, *l_window,
		*l_time_line_grid, *l_tot_time_button, *l_h_separator,
		*l_scroll_window, *l_frame_grid;
	
	l_window = gtk_widget_get_toplevel(p_grid);
	GtkWidget* l_an_grid = gtk_grid_get_child_at(GTK_GRID(p_grid), 0, 3);
	if(l_an_grid != NULL) {
		g_print("timeline grid isn't null\n");
		gtk_widget_destroy(l_an_grid);
	}
	//timeline bar
	gtk_widget_set_name(p_grid, "timeline_sub_window");
	l_time_line_grid = gtk_grid_new();
	gtk_grid_attach(GTK_GRID(p_grid), l_time_line_grid, 0, 3, 5, 3);
	gtk_widget_set_name(l_time_line_grid, "timeline");
	l_button = gtk_button_new_with_label("+");
	g_signal_connect(l_button, "clicked", G_CALLBACK(new_frame_window), GTK_GRID(p_grid));

	gtk_grid_attach(GTK_GRID(l_time_line_grid), l_button, 0, 0, 1, 1);

	l_label = gtk_label_new("Time:");
	gtk_grid_attach(GTK_GRID(l_time_line_grid), l_label, 1, 0, 1, 1);

	l_tot_time_button = gtk_spin_button_new_with_range(0.0f, 99.9f, 0.5f);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(l_tot_time_button),
				  g_cur_animation->duration);
	g_signal_connect(l_tot_time_button, "value-changed",
			 G_CALLBACK(animation_duration_button_callback),
			 g_cur_animation);
	gtk_grid_attach(GTK_GRID(l_time_line_grid), l_tot_time_button, 2, 0, 1, 1);

	char l_frame_label[16];
	snprintf(l_frame_label, 16, "Frames: %d", g_cur_animation->frames);
	l_label = gtk_label_new(l_frame_label);
	gtk_grid_attach(GTK_GRID(l_time_line_grid), l_label, 3, 0, 1, 1);

	l_h_separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_grid_attach(GTK_GRID(l_time_line_grid), l_h_separator, 0, 1, 5, 1);

	      
	l_h_separator = gtk_separator_new(GTK_ORIENTATION_VERTICAL);
	gtk_grid_attach(GTK_GRID(l_time_line_grid), l_h_separator, 0, 1, 5, 1);

	l_scroll_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(l_scroll_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_NEVER);
	l_frame_grid = gtk_grid_new();
	gtk_widget_set_name(l_frame_grid, "frame_grid");
	gtk_container_add(GTK_CONTAINER(l_scroll_window), l_frame_grid);
	      
	//Timeline content
	for(int8_t i = 0; i < g_cur_animation->frames; i++){
		g_print("timeline: %d\n", i);
		l_button = gtk_button_new_with_label("t");
		anim_keyframe* l_cur_keyframe = g_cur_animation->keyframes;
		for(int8_t b = 0; b <= i; b++,
		    l_cur_keyframe = l_cur_keyframe->next)
		{
			if(b == i)
			{
			      struct frame_sprite_sheet* l_frame = g_animation_sprite_sheet.frames;
			      while(l_frame != NULL)
			      {
			          if(!strcmp(l_cur_keyframe->sourceTag, l_frame->tag))
				  {
					  GtkWidget* l_image = gtk_image_new_from_pixbuf(g_animation_sprite_sheet.image);;
					  get_frame_image(l_image, l_frame);
					  gtk_button_set_always_show_image (GTK_BUTTON (l_button),
									    1);
					  gtk_button_set_image(GTK_BUTTON(l_button),
							       l_image);
					  break;
				  }
				  l_frame = l_frame->next;
			      }
			}
		}
		/* g_signal_connect(l_button, "clicked", G_CALLBACK(change_animation_speed), */
		/* GINT_TO_POINTER(i)); */
		g_signal_connect(l_button, "clicked", G_CALLBACK(new_frame_window),
				 GINT_TO_POINTER(i));
		gtk_grid_attach(GTK_GRID(l_frame_grid), l_button, i, 0, 1, 1);
	}

	gtk_grid_attach(GTK_GRID(l_time_line_grid), l_scroll_window, 0, 1, 10, 1);
	gtk_widget_show_all(p_grid);
}

void animation_sub_window(GtkWidget* p_grid) {
	GtkWidget *l_label, *l_button, *l_image, *l_animation_grid,
		  *l_sp_sheet_entry;
	GdkPixbuf* l_pixbuf, *l_pixbuf_small_part;
	GdkPixbufSimpleAnim* l_test_animation;

	GtkWidget* l_an_grid = gtk_grid_get_child_at(GTK_GRID(p_grid), 2, 2);
	if(l_an_grid != NULL) {
		g_print("anGrid isn't null\n");
		l_label = gtk_grid_get_child_at(GTK_GRID(p_grid),2,1);
		gtk_widget_destroy(l_label);
		gtk_widget_destroy(l_an_grid);
	}
	
	l_label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(l_label), g_cur_animation->name);
	gtk_grid_attach(GTK_GRID(p_grid), l_label, 2, 1, 1, 1);

	l_animation_grid = gtk_grid_new();
	gtk_grid_attach(GTK_GRID(p_grid), l_animation_grid, 2, 2, 3, 3);

	l_label = gtk_label_new("Sprite sheet:");
	gtk_grid_attach(GTK_GRID(l_animation_grid), l_label, 0, 0, 1, 1);
	l_sp_sheet_entry = gtk_entry_new();
	gtk_entry_set_placeholder_text(GTK_ENTRY(l_sp_sheet_entry), g_cur_animation->file);
	g_signal_connect (GTK_ENTRY (l_sp_sheet_entry), "activate",
			  G_CALLBACK (change_sheet_entry), NULL);
	gtk_grid_attach(GTK_GRID(l_animation_grid), l_sp_sheet_entry, 1, 0, 1, 1);
	l_button = gtk_button_new_from_icon_name("document-open", GTK_ICON_SIZE_BUTTON);
	g_signal_connect(l_button, "clicked", G_CALLBACK(open_sprite_sheet_dialog),
			 l_sp_sheet_entry);
	gtk_grid_attach(GTK_GRID(l_animation_grid), l_button, 2, 0, 1, 1);

	//Preview animation
	l_label = gtk_label_new("Preview");
	gtk_grid_attach(GTK_GRID(l_animation_grid), l_label, 0, 1, 1, 1);

	load_sprite_sheet(&g_animation_sprite_sheet); //TODO needs to be here?
	l_pixbuf = gdk_pixbuf_new_from_file(g_animation_sprite_sheet.imageSource, NULL);
	g_print("imagesource spritesheer: %s\n", g_animation_sprite_sheet.imageSource);

	l_test_animation = gdk_pixbuf_simple_anim_new(200,200,
						      (g_cur_animation->duration /
						       g_cur_animation->frames));
	anim_keyframe* l_current_frame = g_cur_animation->keyframes;

	while(l_current_frame != NULL) {
		struct frame_sprite_sheet* l_current_sp_frame = g_animation_sprite_sheet.frames;
		while(l_current_sp_frame != NULL) {
			if(!strcmp(l_current_frame->sourceTag, l_current_sp_frame->tag)) {
				l_pixbuf_small_part =
					gdk_pixbuf_new(GDK_COLORSPACE_RGB,
						       TRUE, 8,
						       l_current_sp_frame->width,
						       l_current_sp_frame->height);
				gdk_pixbuf_fill(l_pixbuf_small_part,0xffffffff);
				gdk_pixbuf_copy_area(l_pixbuf,
						     l_current_sp_frame->x,
						     l_current_sp_frame->y,
						     l_current_sp_frame->width,
						     l_current_sp_frame->height,
						     l_pixbuf_small_part,0,0);
				l_pixbuf_small_part =
					gdk_pixbuf_scale_simple(l_pixbuf_small_part,
								200, 200,
								GDK_INTERP_NEAREST);
				gdk_pixbuf_simple_anim_add_frame(l_test_animation,
								 l_pixbuf_small_part);
			}
			l_current_sp_frame = l_current_sp_frame->next;
		}
		l_current_frame = l_current_frame->next;
	}

	gdk_pixbuf_simple_anim_set_loop(l_test_animation, TRUE);
	/* l_image = gtk_image_new_from_pixbuf(l_pixbuf); */
	l_image = gtk_image_new_from_animation((GdkPixbufAnimation*)l_test_animation);
	gtk_grid_attach(GTK_GRID(l_animation_grid), l_image, 0, 2, 1, 1);
	
	animation_timeline_sub_window(l_animation_grid);
	
	gtk_widget_show_all(p_grid);
}

void select_animation(GtkWidget* p_widget, int p_ID)
{
	//change the current animation
	g_cur_animation = &g_animation_database->animations[p_ID];

	//Get the correct grid
	GtkWidget* l_scroll_window = gtk_widget_get_ancestor(p_widget, GTK_TYPE_SCROLLED_WINDOW);
	GtkWidget* l_top_animation_grid = gtk_widget_get_ancestor(l_scroll_window, GTK_TYPE_GRID);
  
	//redraw the sub window
	animation_sub_window(l_top_animation_grid);
}

void animation_select_container(GtkWidget* p_grid)
{
	GtkWidget* l_an_grid,*l_animation_scroll_select, *l_scroll_select_grid;
	l_an_grid = gtk_grid_get_child_at(GTK_GRID(p_grid), 0, 2);
	if(l_an_grid != NULL) {
		g_print("anGrid isn't null\n");
		gtk_widget_destroy(l_an_grid);
	}
	l_animation_scroll_select = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(l_animation_scroll_select),
				       GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	l_scroll_select_grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(l_animation_scroll_select), l_scroll_select_grid);

	for(int i = 0; i < g_animation_database->size; i++) {
		GtkWidget* l_button = gtk_button_new_with_label(g_animation_database->animations[i].name);
		gtk_grid_attach(GTK_GRID(l_scroll_select_grid), l_button, 0, i, 1, 1);
		g_signal_connect(l_button, "clicked", G_CALLBACK(select_animation), GINT_TO_POINTER(i));
	}
	
	gtk_grid_attach(GTK_GRID(p_grid), l_animation_scroll_select, 0, 2, 2, 6);
	gtk_widget_show_all(p_grid);
}

void animation_window(GtkWidget* p_widget, gpointer p_data)
{
	GtkWidget *l_grid, *l_label, *l_window, *l_button;

	l_grid = gtk_grid_get_child_at(p_data, 0, 1);
	gtk_widget_destroy(l_grid);
	l_grid = gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(l_grid), 20);
	gtk_grid_set_row_spacing(GTK_GRID(l_grid), 5);
	l_window = gtk_widget_get_toplevel(p_data);

	gtk_grid_attach(GTK_GRID(p_data), l_grid, 0, 1, 30, 10);
  
	//ANIMATIONS LIST
	l_label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(l_label), "<big><b>Animations:</b></big>");
	gtk_grid_attach(GTK_GRID(l_grid), l_label, 0, 1, 1, 1);
	g_animation_database = load_animation_database();
	if(g_animation_database == NULL){
		g_print("ERROR: animation database is null\n");
		gtk_widget_show_all(l_window);
		return;
	}
	g_cur_animation = &g_animation_database->animations[0];

	animation_select_container(l_grid);
	
	l_button = gtk_button_new_with_label("+");
	gtk_grid_attach(GTK_GRID(l_grid), l_button, 0, 8, 1, 1);
	g_signal_connect(l_button, "clicked", G_CALLBACK(animation_add_dialog_open),
			 NULL);
	l_button = gtk_button_new_with_label("-");
	gtk_grid_attach(GTK_GRID(l_grid), l_button, 1, 8, 1, 1);
	g_signal_connect(l_button, "clicked", G_CALLBACK(animation_delete_dialog_open),
			 NULL);
	l_button = gtk_button_new_from_icon_name("document-save",32);
	gtk_grid_attach(GTK_GRID(l_grid), l_button, 0, 9, 2, 2);
	g_signal_connect(l_button, "clicked", G_CALLBACK(animation_gui_save_dialog),
			 NULL);
	gtk_widget_show_all(l_window);
}

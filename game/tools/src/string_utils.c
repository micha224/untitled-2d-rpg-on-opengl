#include "string_utils.h"
#include <unistd.h>

void sub_string(char* p_dest, const char* p_input, int p_offset, int p_len)
{
	int l_input_length = strlen(p_input);

	if(p_offset + p_len > l_input_length)
		return;

	if(p_offset == 0 && p_len == 0)
		p_len = l_input_length;

	strncpy(p_dest, p_input + p_offset, p_len);
}

/* char* relative_path(char* p_directory, char* p_absolute_path); */
/* char* relative_path_to_cwd(char* p_absolute_path) */
/* { */
/* 	//getcwd() */
/* 	return relative_path("a", p_absolute_path); */
/* } */

/* char* relative_path_to_game(char* p_absolute_path) */
/* { */
/* 	return relative_path("a", p_absolute_path); */
/* } */

#include "editor.h"

const char *g_databases[] = {"Select database","Animations", "Items", "Characters", "SpriteSheet"};

static void change_window(GtkWidget *p_widget, gpointer p_data)
{

  GtkComboBox *l_combo_box = GTK_COMBO_BOX(p_widget);

  if(gtk_combo_box_get_active(l_combo_box)!= 0)
  {
    gchar *l_choice = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(l_combo_box));
    g_print("%s\n", l_choice);
    if(!strcmp(l_choice, g_databases[1]))
    {
      g_free(l_choice);
      animation_window(p_widget, p_data);
    }else if(!strcmp(l_choice, g_databases[4]))
    {
      g_free(l_choice);
      sprite_sheet_window(p_widget, p_data);
    }
    else
    {
      g_free(l_choice);
      GtkWidget *l_grid,*l_button, *l_entry, *l_window;
      l_grid = gtk_grid_get_child_at(p_data, 0, 1);
      gtk_widget_destroy(l_grid);
      l_grid = gtk_grid_new();
      l_window = gtk_widget_get_toplevel(p_data);

      gtk_grid_attach(GTK_GRID(p_data), l_grid, 0, 1, 30, 10);
      l_button = gtk_button_new_with_label("Click me");
      g_signal_connect(l_button, "clicked", G_CALLBACK(animation_window), p_data);

      gtk_grid_attach(GTK_GRID(l_grid), l_button, 1, 1, 1, 1);

      gtk_widget_show_all(l_window);
    }
  }


}

static void activate (GtkApplication* p_app, gpointer p_user_data)
{
  GtkWidget *l_window, *l_grid, *l_grid2, *l_button, *l_combo_box;
  
  l_window = gtk_application_window_new(p_app);
  gtk_window_set_title(GTK_WINDOW(l_window),"Window");
  gtk_window_set_default_size(GTK_WINDOW(l_window), 400, 400);

  l_grid2 = gtk_grid_new();
  l_grid = gtk_grid_new();
  l_combo_box = gtk_combo_box_text_new();
  

  for(int i = 0; i < G_N_ELEMENTS(g_databases); i++)
  {
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(l_combo_box), g_databases[i]);
  }

  gtk_combo_box_set_active(GTK_COMBO_BOX(l_combo_box),0);
  g_signal_connect(l_combo_box, "changed", G_CALLBACK(change_window), l_grid2);

  
  gtk_grid_attach(GTK_GRID(l_grid2), l_combo_box, 0, 0, 2, 1);
  gtk_container_add(GTK_CONTAINER(l_window), l_grid2);
  gtk_grid_attach(GTK_GRID(l_grid2), l_grid, 0, 1, 30, 10);
  
  gtk_widget_show_all(l_window);
}

int main(int argc, char **argv)
{
  GtkApplication *l_app;
  int l_status;

  l_app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(l_app, "activate", G_CALLBACK(activate), NULL);
  l_status = g_application_run(G_APPLICATION(l_app),argc,argv);
  g_object_unref(l_app);

  return l_status;
}

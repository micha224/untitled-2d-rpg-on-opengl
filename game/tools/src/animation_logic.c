#include "animation.h"

void swap_keyframes(anim_keyframe* p_keyframe_dest, anim_keyframe* p_keyframe_source)
{
	anim_keyframe l_temp         = *p_keyframe_dest;
	p_keyframe_dest->time        = p_keyframe_source->time;
	p_keyframe_dest->rotation    = p_keyframe_source->rotation;
	strcpy(p_keyframe_dest->sourceTag, p_keyframe_source->sourceTag);
	p_keyframe_dest->position[0] = p_keyframe_source->position[0];
	p_keyframe_dest->position[1] = p_keyframe_source->position[1];
	
	p_keyframe_source->time      = l_temp.time;
	p_keyframe_source->rotation  = l_temp.rotation;
	strcpy(p_keyframe_source->sourceTag, l_temp.sourceTag);
	p_keyframe_source->position[0]  = l_temp.position[0];
	p_keyframe_source->position[1]  = l_temp.position[1];
}

void sort_keyframes(anim_keyframe* p_keyframes)
{
	anim_keyframe* l_head = p_keyframes;
	for(;l_head->next != NULL; l_head = l_head->next)
	{
		anim_keyframe* l_min = l_head;
		anim_keyframe* l_walker = l_head->next;

		for(;l_walker != NULL; l_walker = l_walker->next)
		{
			if(l_min->time > l_walker->time)
				l_min = l_walker;
		}
		swap_keyframes(l_head, l_min);
	}
}

void delete_keyframe(animation* p_animation, anim_keyframe* p_del_frame)
{
	anim_keyframe* l_walker = p_animation->keyframes;
	if(l_walker == p_del_frame){
		p_animation->keyframes = p_del_frame->next;
		p_del_frame->next = NULL;
		p_animation->frames -= 1;
		free(p_del_frame);
		return;
	}

	while(l_walker->next != NULL) {
		if(l_walker->next == p_del_frame){
			l_walker->next = p_del_frame->next;
			p_del_frame->next = NULL;
			p_animation->frames -= 1;
			free(p_del_frame);
			break;
		}
		l_walker = l_walker->next;
	}
}

void parse_keyframe(xmlNodePtr p_node, anim_keyframe* p_keyframe)
{
	xmlNodePtr l_node = xmlFirstElementChild(p_node);
	while(l_node != NULL){		
		if(!strcmp(l_node->name, "sourceTag")){
			strcpy(p_keyframe->sourceTag, (char*)xmlNodeGetContent(l_node));
		}
		else if(!strcmp(l_node->name, "time")){
			p_keyframe->time = atof((char*)xmlNodeGetContent(l_node));
		}
		else if(!strcmp(l_node->name, "rotation")){
			p_keyframe->rotation = atof((char*)xmlNodeGetContent(l_node));
		}
		else if(!strcmp(l_node->name, "position")) {
			p_keyframe->position[0] = atoi((char*)xmlNodeGetContent(l_node->children));
			p_keyframe->position[1] = atoi((char*)
						       xmlNodeGetContent(l_node->children->next));
		}
		l_node = l_node->next;
	}
	p_keyframe->next = NULL;
}

void parse_keyframes(xmlNodePtr p_nodes, anim_keyframe* p_keyframes)
{
	xmlNodePtr l_frame_node = xmlFirstElementChild(p_nodes);
	anim_keyframe* l_current_keyframe = p_keyframes;
	parse_keyframe(l_frame_node, l_current_keyframe);
	l_frame_node = l_frame_node->next;
	while(l_frame_node != NULL) {
		l_current_keyframe->next = (anim_keyframe*) malloc(sizeof(anim_keyframe));
		parse_keyframe(l_frame_node, l_current_keyframe->next);
		l_current_keyframe = l_current_keyframe->next;
		l_frame_node = l_frame_node->next;
	}
	//add keyframes to the keyframe object
}

void parse_animation(xmlNodePtr p_animation_node, animation* p_anim_db,
		     int p_index)
{
	strcpy(p_anim_db[p_index].id, (char *)xmlGetProp(p_animation_node, (xmlChar *)"id"));
	g_print("Parse Animation id: %s\n", p_anim_db[p_index].id);
	xmlNodePtr l_child_node = xmlFirstElementChild(p_animation_node);
	while(l_child_node != NULL) {
		if(!strcmp(l_child_node->name, "sheetSource")){
			g_print("%s\n",(char *)l_child_node->name);
			strcpy(p_anim_db[p_index].file,
			       (char *)xmlNodeGetContent(l_child_node));
			g_print("%s\n", p_anim_db[p_index].file);
		} else if(!strcmp(l_child_node->name, "name")) {
			strcpy(p_anim_db[p_index].name,
			       (char *)xmlNodeGetContent(l_child_node));
			g_print("%s\n", p_anim_db[p_index].name);
		} else if(!strcmp(l_child_node->name, "duration")) {
			p_anim_db[p_index].duration =
				atof((char *)xmlNodeGetContent(l_child_node));
			g_print("%.2f\n", p_anim_db[p_index].duration);
		} else if(!strcmp(l_child_node->name, "loop")) {
			if(!strcmp("true",
				   (char *)xmlNodeGetContent(l_child_node))) {
				p_anim_db[p_index].loop = 1;
			}else{
				p_anim_db[p_index].loop = 0;
			}
			g_print("%d\n", p_anim_db[p_index].loop);
		}else if (!strcmp(l_child_node->name, "frames")) {
			p_anim_db[p_index].frames =
				xmlChildElementCount(l_child_node);
			p_anim_db[p_index].keyframes =
				(anim_keyframe*) malloc(sizeof(anim_keyframe));
			parse_keyframes(l_child_node,
					p_anim_db[p_index].keyframes);
		}
		l_child_node = l_child_node->next;
	}
}

animation_db* animation_load_db_from_doc(xmlDocPtr p_doc)
{
	xmlNodePtr l_node = xmlDocGetRootElement(p_doc);
        
	animation_db* l_anim_db_dest = (animation_db*)
		malloc(sizeof(animation_db));
	if(l_anim_db_dest == NULL){
		g_print("not enough memory available to load database!\n");
		return NULL;
	}
	l_anim_db_dest->size = xmlChildElementCount(l_node);
	l_anim_db_dest->animations = (animation*) malloc(l_anim_db_dest->size *
					    sizeof(animation));
	l_node = xmlFirstElementChild(l_node);
	for(int i =0; l_node != NULL; i++){
		g_print("got in node %d\n", i);
		parse_animation(l_node, l_anim_db_dest->animations, i);
		l_node = l_node->next;
	}
	xmlFreeDoc(p_doc);
	return l_anim_db_dest;
}

animation_db* load_animation_database()
{
	/* TODO  make it not use global variables, stands on it own */
	animation_db* l_result;
	xmlParserCtxtPtr l_ctxt;
	xmlDocPtr l_db = NULL;
	
	l_ctxt = xmlNewParserCtxt();
	if (l_ctxt == NULL) {
		g_print("Failed to allocate parser context\n");
		return NULL;
	}
	
	l_db = xmlCtxtReadFile(l_ctxt, ANIM_DB_FILE_PATH, NULL,  XML_PARSE_NOBLANKS+XML_PARSE_DTDVALID);
	if(l_db != NULL && l_ctxt->valid == 1){
		l_result = animation_load_db_from_doc(l_db);
	}else{
		g_print("Failed to load animation database\n");
		xmlFreeDoc(l_db);
	}
	xmlFreeParserCtxt(l_ctxt);
	return l_result;
}


xmlDocPtr animation_create_database_doc(animation_db* p_anim_db)
{
	xmlDocPtr l_doc;
	xmlTextWriterPtr l_writer;

	l_writer = xmlNewTextWriterDoc(&l_doc, 0);
	xmlTextWriterStartDocument(l_writer, NULL,
					      "UTF-8",
					      NULL);
	xmlTextWriterStartElement(l_writer, (xmlChar*) "AnimationDatabase");
	xmlTextWriterFullEndElement(l_writer);
	xmlFreeTextWriter(l_writer);
	return l_doc;
}

int8_t animation_database_save(animation_db* p_anim_db, int16_t p_db_size,
			       char* p_file_path)
{
	int8_t l_result;
	xmlTextWriterPtr l_writer;
      	xmlDocPtr l_doc;

	l_writer = xmlNewTextWriterDoc(&l_doc, 0);
	if (l_writer == NULL) {
		g_print("Animation Database save: Error creating the xml writer\n");
		return 0;
	}

	l_result = xmlTextWriterStartDocument(l_writer, NULL,
					     "UTF-8",
					      NULL);
	if (l_result < 0){
		g_print("Animation Database save: Error at xmlTextWriterStartDocument\n");
		return 0;
	}
	
	l_result = xmlTextWriterWriteDTD(l_writer,
					 (xmlChar*)"AnimationDatabase", NULL,
					 (xmlChar*)"AnimationDatabase.dtd",
					 NULL);
	if (l_result < 0){
		g_print("Animation Database save: Error at xmlTextWriterWriteDTD\n");
		return 0;
	}
	
	l_result = xmlTextWriterStartElement(l_writer, (xmlChar*) "AnimationDatabase");
	if (l_result < 0) {
		g_print("Animation Database save: Error at xmlTextWriterStartElement\n");
		return 0;
	}

	for (int16_t i = 0; i < p_db_size; i++){
		l_result = xmlTextWriterStartElement(l_writer, (xmlChar*) "animation");
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterStartElement 'animation'\n");
			return 0;
		}

		l_result = xmlTextWriterWriteFormatAttribute(l_writer,
						       (xmlChar*) "id",
						       "%s",       
						       p_anim_db->animations[i].id);
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterAttribute 'id'\n");
			return 0;
		}

		l_result = xmlTextWriterWriteFormatElement(l_writer,
							   (xmlChar*) "sheetSource",
							   "%s",
							   p_anim_db->animations[i].file);
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterWriteElement 'sheetSource'\n");
			return 0;
		}

		l_result = xmlTextWriterWriteFormatElement(l_writer,
							    (xmlChar*) "name",
							    "%s",
							    p_anim_db->animations[i].name);
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterWriteElement 'name'\n");
			return 0;
		}
		
		l_result = xmlTextWriterWriteFormatElement(l_writer,
							    (xmlChar*) "duration",
							    "%.2f",
							    p_anim_db->animations[i].duration);
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterWriteElement 'duration'\n");
			return 0;
		}

		l_result = xmlTextWriterWriteFormatElement(l_writer,
							   (xmlChar*) "loop",
							   "%d",
							   p_anim_db->animations[i].loop);
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterWriteElement 'loop'\n");
			return 0;
		}

		l_result = xmlTextWriterStartElement(l_writer, (xmlChar*) "frames");
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterStartElement 'frames'\n");
			return 0;
		}
		anim_keyframe* l_cur_frame = p_anim_db->animations[i].keyframes;
		for (int k = 0; k < p_anim_db->animations[i].frames;
		     k++, l_cur_frame = l_cur_frame->next){
			l_result = xmlTextWriterStartElement(l_writer,
							     (xmlChar*) "frame");
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterStartElement 'frame'\n");
				return 0;
			}

			l_result = xmlTextWriterWriteFormatElement(l_writer,
								   (xmlChar*) "sourceTag",
								   "%s",
								   l_cur_frame->sourceTag);
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterWriteElement 'sourceTag' %s\n", l_cur_frame->sourceTag);
				return 0;
			}
			l_result = xmlTextWriterWriteFormatElement(l_writer,
								   (xmlChar*) "time",
								   "%.2f",
								   l_cur_frame->time);
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterWriteElement 'time'\n");
				return 0;
			}
								   
			l_result = xmlTextWriterWriteFormatElement(l_writer,
								   (xmlChar*) "rotation",
								   "%.2f",
								   l_cur_frame->rotation);
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterWriteElement 'rotation'\n");
				return 0;
			}
	
			l_result = xmlTextWriterStartElement(l_writer,
							     (xmlChar*) "position");
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterStartElement 'position'\n");
				return 0;
			}

			l_result = xmlTextWriterWriteFormatElement(l_writer,
								   (xmlChar*) "x",
								   "%d",
								   l_cur_frame->position[0]);
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterWriteElement 'x'\n");
				return 0;
			}
			l_result = xmlTextWriterWriteFormatElement(l_writer,
								   (xmlChar*) "y",
								   "%d",
								   l_cur_frame->position[1]);
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterWriteElement 'y'\n");
				return 0;
			}
			/* close the position element */
			l_result = xmlTextWriterEndElement(l_writer);
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterEndElement 'position'\n");
				return 0;
			}
			/* close the frame element */
			l_result = xmlTextWriterEndElement(l_writer);
			if (l_result < 0) {
				g_print("Animation Database save: Error at xmlTextWriterEndElement 'frame'\n");
				return 0;
			}
		}

		/* close the frames element */
		l_result = xmlTextWriterEndElement(l_writer);
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterEndElement 'frames'\n");
			return 0;
		}
		/* close the animation element */
		l_result = xmlTextWriterEndElement(l_writer);
		if (l_result < 0) {
			g_print("Animation Database save: Error at xmlTextWriterEndElement 'animation'\n");
			return 0;
		}
		g_print("got to the end of the inner loop\n");
	}

	/* close the animationDatabase element || the last element */
	l_result = xmlTextWriterFullEndElement(l_writer);
	if (l_result < 0) {
		g_print("Animation Database save: Error at xmlTextWriterEndElement 'closedatabase'\n");
		return 0;
	}

	xmlFreeTextWriter(l_writer);

	xmlSaveFormatFileEnc(p_file_path, l_doc, "UTF-8", 1);

	xmlFreeDoc(l_doc);
	return 1;
}

animation_db* animation_add(char* p_animation_name, animation_db* p_anim_database)
{
	animation_db* l_tmp_db = p_anim_database;
	char l_id_tmp[6];
	sub_string(l_id_tmp, l_tmp_db->animations[l_tmp_db->size -1].id,
		   2, strlen(l_tmp_db->animations[l_tmp_db->size -1].id) - 2);
	int16_t last_id = atoi(l_id_tmp);
	int16_t l_size = l_tmp_db->size + 1;
	animation* l_new_anims = realloc(l_tmp_db->animations,
				       l_size * sizeof(animation));
	if(l_new_anims == NULL){
		g_print("Couldn't reallocate memory for the animation database!!\n");
		return p_anim_database;
	}
        l_tmp_db->size += 1;
	int16_t l_index = l_size - 1;
	snprintf(l_new_anims[l_index].id, 8, "i-%d", last_id+1);
	strcpy(l_new_anims[l_index].name, p_animation_name);
	strcpy(l_new_anims[l_index].file, "");
	l_new_anims[l_index].frames = 0;
	l_new_anims[l_index].duration = 0.0f;
        l_new_anims[l_index].loop = 1;
        l_new_anims[l_index].keyframes = NULL;
	l_tmp_db->animations = l_new_anims;
	return l_tmp_db;
}

animation_db* animation_delete(char* p_anim_id,
		      animation_db* p_anim_database)
{
	g_print("got here before delete\n");
	animation_db* l_db_tmp = (animation_db*) malloc(sizeof(animation_db));
	l_db_tmp->animations = (animation*) malloc(sizeof(animation) *
						  p_anim_database->size - 1);
	int8_t l_removed = 0;
	for(int8_t i,j = 0; i < p_anim_database->size; i++) {
		if(!strcmp(p_anim_database->animations[i].id, p_anim_id)) {
			l_removed = 1;
		        continue;
		}
		l_db_tmp->animations[j] = p_anim_database->animations[i];
		j++;
	}
	if(l_removed){
		l_db_tmp->size -= p_anim_database->size - 1;
		free(p_anim_database);		
		return l_db_tmp;
	}
	free(l_db_tmp);
	return p_anim_database;
}

void animation_cur_anim_add_frame(animation* p_animation,
					 anim_keyframe* p_keyframe,
					 gpointer p_index)
{
	if(p_index != NULL){
		anim_keyframe* l_cur_keyframe = p_animation->keyframes;
		if(l_cur_keyframe == NULL)
		{
			p_keyframe->next = NULL;
			p_animation->keyframes = p_keyframe;
		}else{
			while(l_cur_keyframe->next != NULL){
				l_cur_keyframe = l_cur_keyframe->next;
			}
			l_cur_keyframe->next = p_keyframe;
			l_cur_keyframe->next->next = NULL;
		}
		p_animation->frames += 1;
	}
	/* always sort the keyframes */
	sort_keyframes(p_animation->keyframes);
}

int8_t animation_keyframe_equals(anim_keyframe* p_anim_keyframe,
				 anim_keyframe* p_anim_keyframe_ref)
{
	return p_anim_keyframe != NULL && p_anim_keyframe_ref != NULL &&
		((int8_t) p_anim_keyframe->time * 100  ==
		 (int8_t) p_anim_keyframe_ref->time * 100 && 
		 p_anim_keyframe->rotation == p_anim_keyframe_ref->rotation && //check on 2 decimals
		!strcmp(p_anim_keyframe->sourceTag,
			p_anim_keyframe_ref->sourceTag) &&
		p_anim_keyframe->position[0] ==
		p_anim_keyframe_ref->position[0] &&
		p_anim_keyframe->position[1] ==
		p_anim_keyframe_ref->position[1]);
}

int8_t animation_equals(animation* p_anim, animation* p_anim_ref)
{
	int8_t l_equal = p_anim != NULL &&
		p_anim_ref != NULL &&
		(!strcmp(p_anim->id, p_anim_ref->id) &&
		 !strcmp(p_anim->name, p_anim_ref->name) &&
		 !strcmp(p_anim->file, p_anim_ref->file) &&
		 p_anim->frames == p_anim_ref->frames &&
		 p_anim->duration == p_anim_ref->duration &&
		 p_anim->loop == p_anim_ref->loop);
	if(!l_equal){
		return 0;
	}

	for(anim_keyframe* l_walker = p_anim->keyframes,
		    *l_walker_ref = p_anim_ref->keyframes;
	    l_walker != NULL; l_walker = l_walker->next,
		    l_walker_ref = l_walker_ref->next){
		if(!animation_keyframe_equals(l_walker, l_walker_ref)){
				return 0;
			}
	}
	return 1;
}

int8_t animation_db_equals(animation_db* p_db, animation_db* p_db_ref)
{
	if(p_db == NULL || p_db_ref == NULL || p_db->size != p_db_ref->size){
		return 0;
	}
	return 0;
}

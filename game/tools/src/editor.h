#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/xmlwriter.h>
#include "string_utils.h"
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "config.h"

struct frame_sprite_sheet {
	char tag[32];
	int x;
	int y;
	int width;
	int height;
	struct frame_sprite_sheet* next;
};

typedef struct sprite_sheet {
	char imageSource[64];
	GdkPixbuf *image;
	int amount;
	struct frame_sprite_sheet* frames;
} sprite_sheet;


void animation_window(GtkWidget*, gpointer);
void sprite_sheet_window(GtkWidget*, gpointer);

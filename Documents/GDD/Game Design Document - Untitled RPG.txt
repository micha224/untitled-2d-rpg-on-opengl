Game Design Document - Untitled RPG


Genre: RPG (Turn-Based combat)
Graphic: 2D (top-down view/sort of isometric, golden sun style)
General Description:
	TODO


1. Systems:

	1.1 Basic Controls:
		Arrow keys to move the playable character around the map (in 4 directions North, East, South and West) and navigate around menu's.
		Confirm key (default hotkey 'Z') to confirm option selected in menu's or to confirm an action (e.g. standing in front of a chest and pressing the confirm key will open the chest).
		Cancel key (default hotkey 'X') to cancel option selected in menu's or go back to previous menu.

	1.2 Map:
		The map (world) of the game is divided in different area's. The maps for the area's are grid based. There is no overworld, but there are different area's.

	1.3 Party:
		The party can have a maximum of 5 members (including the main character).

	1.4 Characters (For party):
		The characters have different elements:
		1.4.1 Graphics:
				Menu (icon),
				On maps,
				(in dialogue).
		1.4.2 Stats:
				Name: Name of the character.
				Health / Max Health: Hit points (how much damage you can still take) / How much damage you can take in total.
				Addiction (temp. name): The currency used for special powers/attacks.
				Attack: Influences how much damage is dealt by melee or ranged attacks with weapons (not for special powers/attacks).
				Accuracy: Determines if you hit or not.
				Speed: Used to determine the order of turns in combat.
				Defence: Negates a certain x% of damage from weapons (not from special powers).
				Special Defence: Negates a certain x% of damage from special attacks (not from weapons).

				special powers/attacks gain exp. when used and when leveling up gain new features (can choose between at least two at certain levels). Stats increase/decrease on lvl up and with equipment.
				All stats range from 1-100 except health, max health has a limit of 99999. Max health increases by leveling up, which based on a base value + percentage of health taken from hits in that level. Health can also be increased by training in the dojo (stamina training).
				Levels have no limit, so if they grind enough every stat can be 100. However, enemies don't scale with levels so at some point monsters don't give alot of exp for the level anymore.
				A character has a equipment slot for weapon, armour and a accessory.

				1.4.2.1 Leveling up:
					A character levels up if it reaches the max exp for that level. Max health gets increased by a base value x. The stats are increased based on their distribution, max points a stat can be increased by is 3 points. The highest stat gets full points, the next two stats gain 2/3 of the full points. The remaining stats have 50% of gainin 1/3 of the full points.
					Exp calculations:
	   					Exp from level 0 to 10 it is a logistic function, 10000/(1+exp(-0.6*(i-5))). From level 10 to 100 it is linear, 250*(i-10) + levels[10]. From level 100 to infinity it is sort of exponential, the curve resets every 33 levels:
	   					#level 100+
						c <- levels[100];
						a <-1;
						d<- 100;
						while(a <= 34)
						{
						  if(((a %% 33 ) == 0)&& d <200)
						  {
						    c <- levels[(a+d-1)];
						    a <-1;
						    d <- d +32;
						  }
						  levels[(a+d)] <- c + (1000000)/(1+exp(-0.17*(a-33)));
						  a <- a+1;
						}. 								

	1.5 Enemies:
		1.5.1 Graphics:
			graphics for battle.
		1.5.2 Stats:
			Name: Name of the enemy.
			Health / Max Health:  Hit points (how much damage you can still take) / How much damage you can take in total.
			Addiction (temp. name): The currency used for special powers/attacks.
			Attack: Influences how much damage is dealt by melee or ranged attacks with weapons (not for special powers/attacks).
			Accuracy: Determines if you hit or not.
			Speed: Used to determine the order of turns in combat.
			Defence: Negates a certain x% of damage from weapons (not from special powers).
			Special Defence: Negates a certain x% of damage from special attacks (not from weapons).


	1.6 Combat:
		1.6.1 Turn-Based:
			The combat is based on turns, the turn order is based on the speed of the characters from the party and enemies. In every turn that entity can use a attack, special power, item or flee.
		1.6.2 Status:
			Stun: Can't do anything in the turn(s).
			Fasten: Speed is increased, turn order is changed.
			Slowed: Speed is decreased, turn order is changed.
			Crazy: Random action performed in turn.
		1.6.3 End of combat:
			End of combat has two states, Victory or Defeat.
			Victory:
				Enemies are defeated. Party members gain experience (EXP) and money. Bosses may drop gear.
			Defeat:
				Party members have lost all their health. After battle wake up at a nearby 'cave/temple' with their max health reduced.
		1.6.4 Combat Calculations:
			Hitting:
				Hit: Accuracy(attacker) > Defence + speed (Attacked), P=1. Accuracy(attacker) < Defence + speed (Attacked), P=0.8. Accuracy (attacker) <= 0.5(Defence + speed) (Attacked), p=0.35. This is for weapons, special attacks/powers always hit.
			Damage:
				Amount of damage on attacked = (dmgWeapon * attack) (attacker) * (1 - (Defence * 0.1)) (Attacked). For Weapons.
				Amount of damage on attacked = ((dmgSPower) * level) (attacker) * (1 - (Special Defence *0.1)) (Attacked). For Special Attacks.
		1.6.5 Areas:
			Types of area's: safe area's, or unsafe. In unsafe area's random encounters can happen, whereas in safe area's that is not the case. The random encounters have a limit, once limit is reached (will be indicated to the player) there will be no random encounters anymore unless the player uses a attractant. 

	1.7 Buildings:
		1.7.1 Weapons and armour shop:
			Can buy weapons and armour from it (in later towns the gear has better stats).
		1.7.2 Inn:
			Here the party can rest, this puts health to max health. Can buy general items, like food or drinks.
		1.7.3 Temple:
			The place where party members can be revived. If revived the party members max health will be drecreased by z. Furthermore, party members are able to fill up their addiction by x amount, for every x amount one of the stats will be randomly decreased by y amount.
		1.7.4 Dojo:
			A place to have the party members train certain stats, like health, defence.
		

2. Engine:
	2.1 Tiled:
		Importer for Tiled maps. Each tile layer is rendered on top of each other (new plane) in openGL. Create one texture (maybe texture array?) object that contains each layer (based on coordinates in the tmx file), instead of having each tile a quad/square in geometry.
	
	2.2 Renderer:
		OpenGL
		UI system.

	2.3 Audio:
		OpenAL

	2.4 Engine:
		RenderManager, PhysicsManager (if needed), AnimationManager, TextureManager, VideoManager, MemoryManager, FileSystemManager, sceneManager, input/ControllerManager AudioManager (in code)
		Example layout:
		class RenderManager
		{
			public:
				RenderManager()
				{
				// do nothing
				}

				~RenderManager()
				{
				//do nothing
				}

				void startUp()
				{
					// start up the manager
				}

				void shutDown()
				{
					// shut down the manager
				}
		}
		

3. Story
	3.1 Main plot:
		General:
		The story takes place right after the defeat of the unknown (after 5 neulebs). And it involves in the struggles of the now freely used magic. Different stories over chapters in the end seem to be involved with the "Energy". multiptle ends if the Energy is destroyed the world is destroyed, if let free then world goes on like before and if they trap the energy, there is peace for the moment.
	3.2 Main Character(s):
		-Rua, elf, young adult  (main character)
			General description:
				Indifferent and quiet person.
				Personality trait(s):
				Ideal(s):
				Bond(s):
				Flaw(s):
			Combat/stats preferences:
				General, player defines these stats at beginning of game.

		-Za, elf, young adult (childhood friend of Rua)
			General description:
				Personality trait(s):
				Ideal(s):
				Bond(s):
				Flaw(s):
			Combat/stats preferences:
				Is very fast, and is somewhat good in special powers.

		-Gimgen, dwarf, adult 53 years, (member of the brotherhood of order)
			General description:
				Personality trait(s):
				Ideal(s):
				Bond(s):
				Flaw(s):
			Combat/stats preferences:
				Is very good in special powers, and somewhat good in defence.
				
		-Ping, human, adult 36 years,  (member of the true blood)
			General description:
				Personality trait(s):
				Ideal(s):
				Bond(s):
				Flaw(s):
			Combat/stats preferences:
				Is good in special defence and normal defence.

	3.3 Prologue:
		General:
		The fight with the unknown and some time before that.
		3.3.1 Characters involved:
			-Zailiena (first human) and the fire red swords.
				General description:
					Personality trait(s):
					Ideal(s):
					Bond(s):
					Flaw(s):
				Combat/stats preferences:
					Is good in special defence and normal defence.

			-Aika (mother of Rua) part of the fire red swords.
			-Takeo (father of Rua) part of the fire red swords.
			-Pelton, Varis, Stam, Rakar, Drake & Gaya, are described with their story about how they wanted to search for the truth about 'magic' stones. 

		3.3.2 In-Depth description:



	3.4 Chapter 1:
		General:
		Mayor (human) of the town, Gamor in Reikei, (from main character) is now power hungry because of the freely available magic. At the end of the chapter, Zailiena comes to town and tells the mother of Za that the parents of Rua did not survive.
		3.4.1 Characters involved:
		3.4.2 In-Depth description:

	3.5 Chapter 2:
		General:
		Main character has flashbacks to its parents, and visits the places from the flashbacks, in this deserty place (where now tree's grow because climate change) it gets tangled up in the mess between the true blood and the brotherhood of order (meets Gimgen). 
		3.5.1 Characters involved:
		3.5.2 In-Depth description:

4. Items
	4.1 General items
		4.1.1 Attractant
			

	4.2 Weapons

	4.3 Armour and shields

	4.4 Accessories

5. Special powers
	5.1 Physical

	5.2 "Magical"

6. Enemies

7. World
	7.1 World Map

	7.2 Towns and cities
		7.2.1 Gamor











* Systems
** Controls                                                          :system:
*** Basic Controls
Arrow keys to move the playable character around the map (in 4 directions
North, East, South and West) and navigate around menu's.
Confirm key (default hotkey 'Z') to confirm option selected in menu's or
to confirm an action (e.g. standing in front of a chest and pressing the
confirm key will open the chest).
Cancel key (default hotkey 'X') to cancel option selected in menu's or
go back to previous menu.
*** Input handling
    Using the command pattern for coding might be useful.
** Map                                                               :system:
   The map (world) of the game is divided in different area's. The maps for
   the area's are grid based. There is no overworld, but there are different
   area's.(look at the legend of heroes series, e.g., moonlight witch prophecy)
** Party                                                             :system:
   The party can have a maximum of 5 members (including the main character).
*** Characters                                                       :system:
The characters have different elements:
**** Graphics
Menu (icon),
On maps,
(in dialogue).
**** Stats
Name: Name of the character.
Health / Max Health: Hit points (how much damage you can still take) /
How much damage you can take in total.
Addiction (temp. name): The currency used for special powers/attacks.
Attack: Influences how much damage is dealt by melee or ranged attacks with
weapons (not for special powers/attacks).
Accuracy: Determines if you hit or not.
Speed: Used to determine the order of turns in combat.
Defence: Negates a certain x% of damage from weapons (not from special powers).
Special Defence: Negates a certain x% of damage from special attacks (not from weapons).
Attraction: How much monsters/enemies are attracted to the player party.
(Only the current leader of the party is taking into account for attracting enemies)

special/improved powers/attacks gain exp. when used and when leveling up gain
new features (can choose between at least two at certain levels).
Improved attacks are based on weapons used and replaces the default attack
(The different tiers add up on each other).
Stats increase/decrease on lvl up and with equipment.

All stats range from 1-100 except health, max health has a limit of 99999.
Max health increases by leveling up, which based on a base value + percentage
of health taken from hits in that level. Health can also be increased by
training in the dojo (stamina training).

Levels have no limit, so if they grind enough every stat can be 100. However,
enemies don't scale with levels so at some point monsters don't give alot of
exp for the level anymore.

A character has a equipment slot for weapon, armour and a accessory.

Leveling up:
A character levels up if it reaches the max exp for that level. Max health
gets increased by a base value x. The stats are increased based on their
distribution, max points a stat can be increased by is 3 points. The highest
stat gets full points, the next two stats gain 2/3 of the full points.
The remaining stats have 50% of gainin 1/3 of the full points.
Exp calculations:
Exp from level 0 to 10 it is a logistic function, 10000/(1+exp(-0.6(i-5))).
From level 10 to 100 it is linear, 250(i-10) + levels[10].
From level 100 to infinity it is sort of exponential, the curve resets every
33 levels:
#+BEGIN_SRC 
           #level 100+
        c <- levels[100];
        a <-1;
        d<- 100;
        while(a <= 34)
        {
          if(((a %% 33 ) == 0)&& d <200)
          {
            c <- levels[(a+d-1)];
            a <-1;
            d <- d +32;
          }
          levels[(a+d)] <- c + (1000000)/(1+exp(-0.17*(a-33)));
          a <- a+1;
        }.           
#+END_SRC
** Enemies                                                           :system:
*** Graphics
graphics for battle.
*** Stats
Name: Name of the enemy.
Health / Max Health: Hit points (how much damage you can still take) / How much damage you can take in total.
Addiction (temp. name): The currency used for special powers/attacks.
Attack: Influences how much damage is dealt by melee or ranged attacks with weapons (not for special powers/attacks).
Accuracy: Determines if you hit or not.
Speed: Used to determine the order of turns in combat.
Defence: Negates a certain x% of damage from weapons (not from special powers).
Special Defence: Negates a certain x% of damage from special attacks (not from weapons).

maybe type-object pattern?
** Combat                                                            :system:
*** Turn-Based
The combat is based on turns, the turn order is based on the speed of the characters from the party and enemies. In every turn that entity can use a attack, special power, item/re-equip or flee.
*** Status
Stun: Can't do anything in the turn(s).
Fasten: Speed is increased, turn order is changed.
Slowed: Speed is decreased, turn order is changed.
Crazy: Random action performed in turn.
*** End of combat
End of combat has two states, Victory or Defeat.
**** Victory:
Enemies are defeated. Party members gain experience (EXP) and money.
Bosses may drop gear.
**** Defeat
Party members have lost all their health. After battle wake up at a nearby
'cave/temple' with their max health reduced.
*** Combat Calculations
**** Hitting
Hit: Accuracy(attacker) > Defence + speed (Attacked),
P=1. Accuracy(attacker) < Defence + speed (Attacked),
P=0.8. Accuracy (attacker) <= 0.5(Defence + speed) (Attacked),
p=0.35. This is for weapons, special attacks/powers always hit.
**** Damage
***** Weapons
      Amount of damage on attacked = (dmgWeapon attack) (attacker) (1 - (Defence 0.1)) (Attacked).
***** Special attacks
      Amount of damage on attacked = ((dmgSPower) level) (attacker) (1 - (Special Defence 0.1)) (Attacked).
*** Areas
Types of area's: safe area's, or unsafe.
In unsafe area's random encounters can happen, whereas in safe area's that is
not the case. The random encounters have a limit, once limit is reached (will
be indicated to the player) there will be no random encounters anymore unless
the player uses a attractant.
** Buildings                                                         :system:
*** Weapons and armour shop                                          :system:
    Can buy weapons and armour from it (in later towns the gear has better stats).
*** Inn                                                              :system:
    Here the party can rest, this puts health to max health.
    Can buy general items, like food or drinks.
*** Temple                                                           :system:
    The place where party members can be revived. If revived the party members
    max health will be drecreased by z. Furthermore, party members are able to
    fill up their addiction by x amount, for every x amount one of the stats
    will be randomly decreased by y amount.
*** Dojo                                                             :system:
    A place to have the party members train certain stats, like health, defence.
** GUI menus                                                         :system:
   List of GUI for the menus.
   Diff menus for gba and pc version or make one that is compatible with both?
*** Main Menu
    mock-up
*** Pause Menu
    inspiration from trails in the sky pc version
*** Combat Menu
    mock up/ inspiration from trails in the sky
* Engine
** Tiled                                                             :system:
   Importer for Tiled maps. Each tile layer is rendered on top of each other
   (new plane) in openGL. Create one texture (maybe texture array?) object
   that contains each layer (based on coordinates in the tmx file), instead of
   having each tile a quad/square in geometry.
   uses tmx library
** Renderer                                                          :system:
   Uses glfw and opengl via raylib. Maybe own layer for gba port.
*** Cutscene system                                                  :system:
    ability to play cutscenes. Initial design?
    Using events that are played after each other in sequence.
    Events can be a lookup table to certain functions?
    (could make it read from xml for example but could also just be a c file
    with the event functions, is easier for me I think)
    Ability to trigger it in the map, or certain parts of main story
** Audio                                                             :system:
   audio from raylib or openal.
** Implementation                                                    :system:
libxml2 for import of xml data.
Dear ImGUI is useful for debugging/writing tools (for example tools for databases; github)

Look at unity's game loop for inspiration.
RenderManager, PhysicsManager (if needed), AnimationManager, TextureManager, VideoManager, MemoryManager, FileSystemManager, sceneManager, input/ControllerManager AudioManager (in code)
Example layout:
#+BEGIN_SRC
class RenderManager
{
public:
RenderManager()
{
// do nothing
}

    ~RenderManager()
    {
    //do nothing
    }

    void startUp()
    {
      // start up the manager
    }

    void shutDown()
    {
      // shut down the manager
    }
}
#+END_SRC
game loop:
Initilization (only once):

Loop (per frame):
(physics)
input
game mechanics
render on scene

** coding conventions                                                :system:
| Code element                         | Convention      | Example                                  |
| -------------                        | :-------------: | -----:                                   |
| Macros                               | ALL_CAPS        | #define MAX(a,b) ((a) > (b) ? (a) : (b)) |
| Defines                              | ALL_CAPS        | #define ANIM_DB_FILE_PATH                |
| Enum members                         | ALL_CAPS        | {MAIN_MENU, MENU, COMBAT, MAP}           |
| functions, variables, enums, structs | snake_case      | screen_width                             |
| Operators                            | value1 * value2 | int product = value + 2                  |
| Pointers                             | type *pointer   | game_object *my_object                   |
| float,double                         | x.xf, x.xd      | float x = 1.0f, double y = 1.0d          |
| functions                            | { on newline    | void my_function()                       |
|                                      |                 | {                                        |
| if, routines, structs                | { on same line  | if (a==b) {                              |

use tabs, (size 8)
* Story
** Characters
   List of the acting characters
*** Main party
**** Rua                                                          :character:
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      Rua, elf, young adult (main character)
      General description:
      Indifferent and quiet person. Plays a music instrument
      Personality trait(s):
      Ideal(s):
      Bond(s):
      Flaw(s):
      Combat/stats preferences:
      General, player defines these stats at beginning of game.
**** Za
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      Za, elf, young adult (childhood friend of Rua)
      General description:
      Personality trait(s):
      Ideal(s):
      Bond(s):
      Flaw(s):
      Combat/stats preferences:
      Is very fast, and is somewhat good in special powers.
**** Gimgen
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      Gimgen, dwarf, adult 53 years, (member of the brotherhood of order)
      General description:
      Personality trait(s):
      Ideal(s):
      Bond(s):
      Flaw(s):
      Combat/stats preferences:
      Is very good in special powers, and somewhat good in defence.
**** Ping
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      Ping, human, adult 36 years, (member of the true blood)
      General description:
      Personality trait(s):
      Ideal(s):
      Bond(s):
      Flaw(s):
      Combat/stats preferences:
      Is good in special defence and normal defence.
**** Zailiena
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      Zailiena (first human) and the fire red swords.
      General description:
      Personality trait(s):
      Ideal(s):
      Bond(s):
      Flaw(s):
      Combat/stats preferences:
      Is good in special defence and normal defence.
*** Side Characters
**** Aika                                                         :character:
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      Aika (mother of Rua) part of the fire red swords
**** Takeo                                                        :character:
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      Takeo (father of Rua). A member of the Fire Red Swords
**** Pelton                                                       :character:
**** Varis                                                        :character:
**** Stam                                                         :character:
**** Rakar                                                        :character:
**** Drake                                                        :character:
**** Gaya                                                         :character:
*** NPC
**** Fire Red Swords Member                                       :character:
     #+CAPTION: graphic
     #+NAME: ingame graphic
     [[samuria_1]]
***** description
      A member of the Fire Red Swords
** Main plot                                                        :chapter:
The story takes place right after the defeat of the unknown (after 5 neulebs).
And it involves in the struggles of the now freely used magic.
Different stories over chapters in the end seem to be involved with the
"Energy".
multiptle ends if the Energy is destroyed the world is destroyed,
if let free then world goes on like before and if they trap the energy,
there is peace for the moment.
** Prologue                                                         :chapter:
*** General
The fight with the unknown and some time before that.
*** Characters involved:
    Character: [[Zailiena]]
    Character: [[Aika]]
    Character: [[Takeo]]
    Character: [[Pelton]]
    Character: [[Varis]]
    Character: [[Stam]]
    Character: [[Rakar]]
    Character: [[Drake]]
    Character: [[Gaya]]
*** Description
Starts with opening cut-scene ( Cutscene: [[Opening scene]] ) and explanation of
the unknown struggle with the first human ( Character: Zailiena ). The player
is playing a random [[Fire Red Swords Member]]. The player can walk
around location [[Fire Red Swords encampment]] and talk to members and [[Zailiena]],
[[Aika]], [[Takeo]]. The random NPC members will have dialogue about the illusion of
the encampment and about the dragon (casino owner) that went into rampage a
little while ago. [[Zailiena]], [[Aika]], [[Takeo]] will talk about attack on the Unknown
and about [[Pelton]], [[Varis]], [[Stam]], [[Rakar]], [[Drake]] and [[Gaya]]. When the player goes
down to the Location: [[Cave of Unkown (fight)]], he sees that there are many
members preparing the attack on the unknown. Player can talk with some of the
NPC's and they will talk about what kind of spells they are preparing with
their magic stones. When too close to the unknown, cut-scene ( Cutscene:
[[Before Fight with Unknown]]) happens and boss fight with the unknown happens.
Pelton, Varis, Stam, Rakar, Drake & Gaya, are described with their story
about how they wanted to search for the truth about 'magic' stones.
*** Opening scene                                                  :cutscene:
    Text on screen (subtitles):
    [[./Prologue/Prologue_dialogue_lines.org]]
*** Before Fight with Unknown                                      :cutscene:
** Chapter 1                                                        :chapter:
*** General
Mayor (human) of the town, Gamor in Reikei, (from main character) is now
power hungry because of the freely available magic. At the end of the chapter,
Zailiena comes to town and tells the mother of Za that the parents of Rua did
not survive.
*** Characters involved
    Character: [[Rua]]
    Character: [[Za]]
*** Description
[[Rua]], [[Za]] play around, being amazed by the fact that there is green foilage
growing around them. While playing around they see the "adult" people being
very eager with their new energy. Especially the mayor is crazy power hungry
with his new ability to use "magic". [[Rua]], [[Za]], see this and try to dumb down
the mayor to stop him from rampaging the town.
*** cutscene/dialogue lines
[[./Chapter 1/Chapter1_dialogue_lines.org]]
** Chapter 2                                                        :chapter:
*** General
Main character has flashbacks to its parents, and visits the places from the
flashbacks, in this deserty place (where now tree's grow because climate
change) it gets tangled up in the mess between the true blood and the
brotherhood of order (meets [[Gimgen]]). In the end infiltrate true blood camp
(true blood are trying to erridicate the elves in reikei). End boss is [[Ping]].
*** Characters involved
*** Description
** Chapter 3                                                        :chapter:
*** General
[[Gimgen]] asks the main character if he want to join and go to
surrounding cities to help with the problems there. Finds out that even the
brotherhood order wants to use energy for their own self gain and power.
*** Characters involved
*** Description
** Chapter 4                                                        :chapter:
*** General
Goes to the lake of insight. finds [[Zailiena]], and [[Ping]] fighting each other
(but fight in the party against boss). [[Zailiena]] says that using all these
special powers gives the unknown energy to resurrect (Because you give a part
of your life/soul with it). Goes back to [[Gamor]] at the end of chapter.
*** Characters involved
*** Description
** Final                                                            :chapter:
*** General
The last dungeon (which is in the mountains and final boss is at the same
place as the prologue), with in the end fight with the Unknown. multiple
choices at the end of game.
*** Characters involved
*** Description
* Items
  Inventory of all the items in the game.
** General Items
*** Attractant                                                         :item:
    #+CAPTION: ICON
    #+NAME: Attractant
    [[]]
**** description
     Consumable potion that contains pheromones for monsters, however, for
     ordinary people it stinks like hell.
     It attracts enemies in the area you are currently in.
**** shop stats
     | variables  | values |
     |------------+--------|
     | Price      |     12 |
     | Sell value |      1 |
     | Consumable | true   |
** Weapons
   General Weapon Classes:
   - Swords
   - Bows
   - Staves
   - Bandages
*** Tier 1     
** Armour
   General Armour classes (for stats):
   - Defence
   - Special Defence
*** Tier 1     
** Shields
   General classes for shields:
   - Defence
*** Tier 1     
** Accessories
*** Tier 1   
* Enemies
  Monster manual of enemies.
** Generic enemy template                                             :enemy:
* Attacks
** Default Attacks                                                 :mechanic:
   The default attack that is based on the damage calculations just from the stats (Attack stat). It only targets a single enemy (or party member).
*** Improved Bow
**** Tier 1
***** Arrow shower
      Default attack now hits multiple targets.
***** Strong shot
      You pull the string more towards the back and release the arrow with more power, resulting in more damage from the arrow on the target.
**** Tier 2
***** attack 1
***** attack 2
**** Tier 3
***** attack 1
***** attack 2
*** Improved Sword
**** Tier 1
***** Extra Attack
      The attack now does an extra attack on a random enemy.
***** Crazieness
      You abilities with the sword have the chance to make the target go crazy.
**** Tier 2
***** attack 1
***** attack 2     
**** Tier 3
***** attack 1
***** attack 2     
*** Improved Staff
**** Tier 1
***** Random spell
      An attack has now a chance to launch a random special power.
***** Random status effect
      The attack has the chance to put on the target a random status effect.
**** Tier 2
***** attack 1
***** attack 2     
**** Tier 3
***** attack 1
***** attack 2     
*** Improved Bandages/Hand
**** Tier 1
***** Self heal
      An attack has now a chance to self heal.
***** Stun blow
      Attack has now the chance to stun the target.
**** Tier 2
***** attack 1
***** attack 2     
**** Tier 3    
***** attack 1
***** attack 2
** Special Powers                                                  :mechanic:
*** Elemental power
    wip: elemental attacks just as fire,water,etc.
*** Tier 1
**** Infuse Energy
     Infuses energy (addiction) in the weapon of the character (even fists/bandages), to increase the power of the attack. At first it takes a certain amount of addiction but after leveling up the special power, the player can choose how much addiction it wants to use to power up the attack.
**** Cure
     Heals part of the HP of a single target. On later levels it heals more.
*** Tier 2
**** Conjure stone
     Can conjure stone to attack a target or defend for one attack. At higher levels more defence turns or more attacks on single target.
**** Drain blood
     Drains blood of a target and can use the blood to attack another target. At higher levels the blood can attack multiple targets.
**** Summon creature
     Summon a creature. First a small creature (does just damage). On later levels you can upgrade your creature (or changes to other creature) that adds other bonuses (like buff for the party or debuff enemy, more dmg, etc.)
*** Tier 3 (end-game)
**** Meteorite
     High level (endgame) power. Summon a meteorite that kills every opponent. But ends the game with destroying the world (cutscene).
* World
  List of zones, areas, that are not dungeons or towns.
** World map                                                          :world:
   #+CAPTION: world map of Gilja
   #+NAME: world map of Gilja
   [[./images/gilja_2nd_campaign.jpg]]

** Towns & Cities                                                     :world:
   List of towns, cities and locations like that.
*** Gamor                                                          :location:
    starting town
** Dungeons                                                           :world:
*** Fire Red Swords encampment                                     :location:
    Encampment located under the mountains and has an illusion spell on it for
    non-members of the Fire Red Swords (Looks like village).
    Has a drop-down to deeper in the mountain where the unknown is.
**** Cave of Unkown (fight)                                        :location:
    Located deep in the mountain, the unkown was amassing energy to regain
    power.
** Zones/Areas    
